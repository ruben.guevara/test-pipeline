const { withSuperjson } = require("next-superjson");
const nextBuildId = require("next-build-id");

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  output: "standalone",
  basePath: "/v4",
  generateBuildId: () => nextBuildId({ dir: __dirname }),
  compiler: {
    removeConsole: false,
  },
  images: {
    dangerouslyAllowSVG: true,
    contentDispositionType: 'attachment',
    contentSecurityPolicy: "default-src 'self'; script-src 'none'; sandbox;"
  },
  webpack: {
    webpackDevMiddleware: (config) => {
      config.watchOptions = {
        poll: 1000,
        aggregateTimeout: 300,
      };

      return config;
    },
  },
};

module.exports = withSuperjson()(nextConfig);
