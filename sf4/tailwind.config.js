/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      boxShadow: {
        "3xl": "-2px 4px 10px rgba(0, 0, 0, 0.2)",
        box: "0px 1px 10px #cccccc",
        input: "0px 1px 4px #cccccc",
        highlight: "0px 1px 20px #63AFFA",
        "highlight-danger": "0px 1px 12px #ED1A26",
      },
      colors: {
        sfblue: {
          50: "#F1F6FC",
          100: "#D4E9FF",
          200: "#B7DBFE",
          300: "#9BCCFC",
          400: "#7FBDFB",
          500: "#63AFFA",
          600: "#308FE3",
          700: "#187CCB",
          800: "#056BB3",
          900: "#005E9D",
        },
        sfgray: {
          300: "#cccccc",
          400: "#9e9e9e",
          500: "#676767",
          600: "#424242",
          700: "#323232",
          800: "#212121",
        },
        sfred: {
          800: "#ED1A26",
        },
      },
      keyframes: {
        toast: {
          "0%": { opacity: 0, width: "64px" },
          "1%": { opacity: 1, width: "64px" },
          "6%": { width: "278px" },
          "97%": { width: "278px" },
          "99%": { opacity: 1 },
          "100%": { opacity: 0, width: "64px" },
        },
        "highlight-question": {
          "0%": { opacity: 1 },
          "40%": { opacity: 0.9 },
          "100%": { opacity: 0 },
        },
        "tooltip-fadeout": {
          "0%": { opacity: 1 },
          "100%": { opacity: 0 },
        },
      },
      animation: {
        toast: "toast 2s ease-in-out forwards",
        "highlight-question": "highlight-question 1.6s linear",
        "tooltip-fadeout":
          "tooltip-fadeout 270ms cubic-bezier(0.16, 1, 0.3, 1)",
      },
    },
  },
  plugins: [],
};
