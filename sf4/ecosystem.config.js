module.exports = {
  apps: [
    {
      name: "studyforge",
      script: "npm",
      args: "run start",
      env: {
        NODE_ENV: "production",
      },
    },
  ],
};