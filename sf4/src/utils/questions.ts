import { IPool, IQuestion } from "../context/quiz/types";

function findDuplicate(
  list: Array<IQuestion | IPool>,
  reference: string
): boolean {
  const hasDuplicate = list.reduce((prev, current) => {
    if (current.type === "pool") {
      return findDuplicate(current.questions, reference) || prev;
    }

    return current.id === reference || prev;
  }, false);

  return hasDuplicate;
}

export { findDuplicate };
