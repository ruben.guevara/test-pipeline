import ErrorBoundary from "$components/shared/ErrorBoundary";
import "$styles/globals.css";
import * as Tooltip from "@radix-ui/react-tooltip";
import { AppType } from "next/app";
import Head from "next/head";
import { AbortProps } from "../../types/Errors";
import { ModalContextProvider } from "../context/modal/ModalContext";
import { api } from "../lib/api";
import Error from "./_error";

const App: AppType<{ error?: AbortProps }> = ({ Component, pageProps }) => {
  if (pageProps.error) {
    return <Error {...pageProps.error} />;
  }
  return (
    <>
      <Head>
        <title>StudyForge</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {/* <GlobalStyles /> */}
      <main className="flex min-h-screen flex-col items-center bg-white">
        <Tooltip.Provider delayDuration={0}>
          <ModalContextProvider>
            <ErrorBoundary>
              <Component {...pageProps} />
            </ErrorBoundary>
          </ModalContextProvider>
        </Tooltip.Provider>
      </main>
    </>
  );
};

export default api.withTRPC(App);
