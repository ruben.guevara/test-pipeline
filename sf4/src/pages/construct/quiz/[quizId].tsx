import { QuizContainer } from "$components/quiz/QuizContainer";
import { LearnosityVersion } from "$lib/learnosity";
import { createServerSideAppRouter } from "$server/api/root";
import { getUser } from "$server/auth/auth";
import { prisma } from "$server/db/prisma";
import { type GetServerSideProps, InferGetServerSidePropsType } from "next";
import Head from "next/head";
import Script from "next/script";
import { useState } from "react";
import { QuizContextProvider } from "../../../context/quiz/QuizContext";
import { listInitialQuizItems } from "../../../context/quiz/reducerFunctions";
import { IQuiz } from "../../../context/quiz/types";

export const getServerSideProps: GetServerSideProps = async ({
  req,
  params,
}) => {
  const [user, error] = await getUser(req, ["assessment_edit"], "root");
  if (error) return { props: { error } };

  const router = await createServerSideAppRouter({ user });

  const id = Number(params?.quizId);
  const quiz = id
    ? await prisma.assessment.findUnique({
        where: { id },
        include: {
          assessment_questions: {
            include: {
              pool: true,
            },
          },
        },
      })
    : null;

  if (!quiz) {
    return {
      notFound: true,
    };
  }

  const references = quiz.assessment_questions.map(
    (q) => q.item_reference as string
  );
  const itemsData = quiz.assessment_questions.length
    ? await router.question.findSelected.fetch(references)
    : [];

  const items =
    listInitialQuizItems(quiz.assessment_questions, itemsData) || [];

  const chapter = quiz.chapter_id
    ? await prisma.chapter.findUnique({
        where: { id: quiz.chapter_id as number },
      })
    : null;

  const course = await prisma.course.findUnique({
    where: {
      id: chapter ? (chapter.course_id as number) : (quiz.course_id as number),
    },
  });

  const region = await prisma.region.findUnique({
    where: { id: course?.region_id as number },
  });

  return {
    props: {
      learnosity: {
        version: LearnosityVersion,
      },
      id: quiz.id,
      name: quiz.name,
      link_active: quiz.link_active,
      parent_id: quiz.parent_id,
      meta: {
        chapter: chapter
          ? {
              id: chapter?.id,
              name: chapter?.name,
            }
          : null,
        course: {
          id: course?.id,
          name: course?.name,
        },
        region: {
          id: region?.id,
          name: region?.name,
        },
      },
      items: items,
    },
  };
};

export default function QuizBuilder({
  learnosity,
  ...props
}: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const [authorScriptLoaded, setAuthorScriptLoaded] = useState(false);

  return (
    <>
      <Script src={`https://items.learnosity.com/?${learnosity?.version}`} />
      <Script
        src={`https://authorapi.learnosity.com/?${learnosity?.version}`}
        onLoad={() => setAuthorScriptLoaded(true)}
      />
      <Head>
        <title>Quiz Builder</title>
      </Head>
      <QuizContextProvider initialData={props as IQuiz}>
        <div className="w-full min-w-[48rem] mx-auto flex flex-col items-center justify-center px-4">
          <QuizContainer authorScriptLoaded={authorScriptLoaded} />
        </div>
      </QuizContextProvider>
    </>
  );
}
