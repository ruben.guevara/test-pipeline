import { Button } from "$components/shared/Button";
import type { AbortProps, ErrorStatusCode } from "$customTypes/Errors";
import type { NextPageContext } from "next";

const statusCodeTitles: Record<ErrorStatusCode, string> = {
  400: "Bad Request",
  401: "Unauthorized",
  402: "Payment Required",
  403: "Forbidden",
  404: "Not Found",
  405: "Method Not Allowed",
  406: "Not Acceptable",
  407: "Proxy Authentication Required",
  408: "Request Timeout",
  409: "Conflict",
  410: "Gone",
  411: "Length Required",
  412: "Precondition Failed",
  413: "Payload Too Large",
  414: "URI Too Long",
  415: "Unsupported Media Type",
  416: "Range Not Satisfiable",
  417: "Expectation Failed",
  418: "I'm a teapot",
  421: "Misdirected Request",
  422: "Unprocessable Entity",
  423: "Locked",
  424: "Failed Dependency",
  425: "Too Early",
  426: "Upgrade Required",
  428: "Precondition Required",
  429: "Too Many Requests",
  431: "Request Header Fields Too Large",
  451: "Unavailable For Legal Reasons",
  500: "Internal Server Error",
  501: "Not Implemented",
  502: "Bad Gateway",
  503: "Service Unavailable",
  504: "Gateway Timeout",
  505: "HTTP Version Not Supported",
  506: "Variant Also Negotiates",
  507: "Insufficient Storage",
  508: "Loop Detected",
  510: "Not Extended",
  511: "Network Authentication Required",
};

const statusCodeMessages: Partial<Record<ErrorStatusCode, string>> = {
  404: "The page you’re looking for does not exist.",
  401: "You need to be logged in to view this page.",
  403: "You do not have permission to view this page.",
  500: "An unknown error occurred on the server. Please let us know if this keeps happening.",
};

function Error({ statusCode, message, title }: AbortProps) {
  return (
    <div className="flex h-screen w-screen items-center justify-center bg-gradient-to-br from-white to-sfblue-50">
      <div className="rounded-md bg-white px-40 py-20 shadow-xl">
        <div className="flex flex-col items-center gap-6">
          <h1 className="text-9xl font-bold text-sfblue-500">{statusCode}</h1>

          <h6 className="text-center text-2xl font-bold text-gray-800 md:text-3xl">
            <span className="text-red-500">Oops!</span>{" "}
            {title ?? statusCodeTitles[statusCode] ?? "Something went wrong"}
          </h6>
          <p className="text-center text-gray-500 md:text-lg">
            {message ??
              statusCodeMessages[statusCode] ??
              statusCodeMessages[500]}
          </p>
          <Button href="/">Go home</Button>
        </div>
      </div>
    </div>
  );
}

Error.getInitialProps = ({ res, err }: NextPageContext) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

export default Error;
