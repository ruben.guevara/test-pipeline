import { RegionTable } from "$components/features/tables/RegionTable";
import { Button } from "$components/shared/Button";
import { Loader as Spinner } from "$components/shared/Spinner";
import { GetServerSideProps } from "$customTypes/Errors";
import { api } from "$lib/api";
import { createServerSideAppRouter } from "$server/api/root";
import { PermissionName } from "$server/auth/accessControl";
import { getUser } from "$server/auth/auth";
import cn from "classnames";
import Head from "next/head";

const permissions: PermissionName[] = [
  "region_edit",
  "region_create",
  "region_delete",
];

export const getServerSideProps: GetServerSideProps<{}> = async ({ req }) => {
  const [user, error] = await getUser(req, permissions, "root");
  if (error) return { props: { error } };

  const router = await createServerSideAppRouter({ user });
  await router.region.list.prefetch();
  await router.auth.checkpoints.prefetch({ permissions });
  return {
    props: {
      trpcState: router.dehydrate(),
    },
  };
};

export default function ManageRegions() {
  const { data: checkpoints } = api.auth.checkpoints.useQuery(
    { permissions },
    { refetchOnMount: false, staleTime: 1000 * 60 * 60 }
  );

  return (
    <>
      <Head>
        <title>Manage Regions</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="container flex flex-col items-center justify-center gap-12 px-4 py-16">
        <h1 className="text-4xl text-[3rem] font-semibold">Manage Regions</h1>
        {checkpoints?.region_create && (
          <Button href="/manage/regions/new">Create New Region</Button>
        )}

        <RegionTable />
      </div>
    </>
  );
}

type RegionListItem = {
  _count: {
    authors: number;
    courses: number;
    institutions: number;
    partners: number;
  };
};

type DeleteLinkProps<T> = {
  item: T;
  loading: boolean;
  onClick: (region: T) => void;
};

function DeleteLink<T extends RegionListItem>({
  item: region,
  loading,
  onClick,
}: DeleteLinkProps<T>) {
  const canDelete =
    region._count.authors === 0 &&
    region._count.courses === 0 &&
    region._count.institutions === 0 &&
    region._count.partners === 0;
  if (!canDelete) return null;
  const className = cn(
    "italic underline",
    loading && "pointer-events-none text-gray-400"
  );
  return (
    <a
      href="#"
      className={className}
      onClick={(e) => {
        e.preventDefault();
        onClick(region);
      }}
    >
      {loading && <Spinner />}
      Delete
    </a>
  );
}
