import { RegionForm } from "$components/features/forms/RegionForm";
import { getUser } from "$server/auth/auth";
import { GetServerSideProps } from "next";
import Head from "next/head";

const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const [, error] = await getUser(req, ["region_create"], "root");
  if (error) return { props: { error } };

  return {
    props: {},
  };
};

export default function CreateNewRegion() {
  return (
    <>
      <Head>
        <title>Create New Region</title>
      </Head>
      <div className="container flex flex-col items-center justify-center gap-12 px-4 py-16">
        <h1 className="text-4xl font-semibold text-[2rem]">
          Create New Region
        </h1>
        <RegionForm />
      </div>
    </>
  );
}
