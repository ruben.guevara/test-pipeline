import { RegionForm } from "$components/features/forms/RegionForm";
import { getUser } from "$server/auth/auth";
import { prisma } from "$server/db/prisma";
import { Region } from "@prisma/client";
import { type GetServerSideProps } from "next";
import Head from "next/head";

export const getServerSideProps: GetServerSideProps = async ({
  req,
  params,
}) => {
  const [, error] = await getUser(req, ["region_edit"], "root");
  if (error) return { props: { error } };

  const id = Number(params?.id);
  const region = id ? await prisma.region.findUnique({ where: { id } }) : null;
  if (!region) {
    return {
      notFound: true,
    };
  } else {
    return {
      props: { region },
    };
  }
};

export default function EditRegion({ region }: { region: Region }) {
  return (
    <>
      <Head>
        <title>{`Edit Region: ${region.name}`}</title>
      </Head>
      <div className="container flex flex-col items-center justify-center gap-12 px-4 py-16">
        <h1 className="text-4xl text-[2rem] font-semibold">
          Edit Region: {region.name}
        </h1>
        <RegionForm region={region} />
      </div>
    </>
  );
}
