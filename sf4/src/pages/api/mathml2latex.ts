import {NextApiRequest, NextApiResponse} from "next";
import M2L from "mathml-to-latex";
import z from "zod";

export default function mathml2latex(request: NextApiRequest, response: NextApiResponse) {
    const input = z.object({mathml: z.array(z.string())});
    const parsed = input.safeParse(request.body);
    if(!parsed.success) {
        response.status(400).json({error: parsed.error});

        return;
    }

    const {mathml} = parsed.data;
    const latex = [];
    for(let m of mathml) {
        latex.push(M2L.convert(m));
    }
    response.status(200).json({
        latex
    });
}