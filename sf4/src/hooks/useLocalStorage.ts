import { Dispatch, SetStateAction, useEffect, useState } from "react";

const STORAGE_PREFIX = "@studyforge:";

function useLocalStorage<T>(
  key: string,
  initialValue?: T
): [value: T, setValue: Dispatch<SetStateAction<T>>] {
  const prefixedKey = STORAGE_PREFIX + key;

  const [value, setValue] = useState<T>(() => {
    if (typeof window !== "undefined") {
      const json = localStorage.getItem(prefixedKey);

      if (json !== null) {
        return JSON.parse(json);
      }
    }

    if (typeof initialValue === "function") {
      return initialValue();
    }

    return initialValue;
  });

  useEffect(() => {
    localStorage.setItem(prefixedKey, JSON.stringify(value));
  }, [value, prefixedKey]);

  return [value, setValue];
}

export { useLocalStorage };
