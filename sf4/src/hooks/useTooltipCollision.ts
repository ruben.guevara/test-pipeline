import { TooltipContentProps } from "@radix-ui/react-tooltip";
import { useCallback, useState } from "react";

/**
 * Provides an easy way to set up the collision boundary for the tooltip content.
 * Returns a ref function to set to the wanted collision element
 */
function useTooltipContentCollision(
  collisionPadding?: TooltipContentProps["collisionPadding"]
): {
  collision: Pick<
    TooltipContentProps,
    "collisionBoundary" | "collisionPadding"
  >;
  ref: (node: HTMLElement | null) => void;
} {
  const [tooltipCollision, setTooltipCollition] = useState<HTMLElement | null>(
    null
  );

  const ref = useCallback((node: HTMLElement | null) => {
    if (node) {
      setTooltipCollition(node);
    }
  }, []);

  return {
    collision: {
      collisionBoundary: tooltipCollision,
      collisionPadding,
    },
    ref,
  };
}

export { useTooltipContentCollision };
