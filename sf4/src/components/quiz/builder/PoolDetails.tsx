import { QuizItem } from "$components/quiz/builder/QuizItem";
import { Button } from "$components/shared/Button";
import { Tooltip } from "$components/shared/Tooltip";
import { mdiClose } from "@mdi/js";
import Icon from "@mdi/react";
import cn from "classnames";
import Image from "next/image";
import React, { useEffect, useMemo, useRef, useState } from "react";
import { useModal } from "../../../context/modal/ModalContext";
import { useQuiz } from "../../../context/quiz/QuizContext";
import { IPool } from "../../../context/quiz/types";
import { sortPoolQuestions } from "../../../context/quiz/utils";
import redoIcon from "../images/redo.svg";
import removeAllIcon from "../images/remove-all.svg";
import undoIcon from "../images/undo.svg";

function PoolDetails() {
  const {
    data,
    dispatch,
    questionsToHighlight,
    poolPickToHighlight,
    poolDetailsOpenedId,
    closePoolDetails,
    canUndo,
    canRedo,
    undo,
    redo,
  } = useQuiz();
  const { dialog } = useModal();
  const questionsContainerRef = useRef<HTMLDivElement>(null);

  const pool = useMemo(() => {
    return data.items.find(
      (item) => item.id === poolDetailsOpenedId && item.type === "pool"
    ) as IPool;
  }, [data.items, poolDetailsOpenedId]);
  const { pickStr, showError, handleSanitizeValue, handleUpdatePoolPick } =
    usePoolPick(pool);

  /**
   * Questions sorted alphabetically
   */
  const sortedQuestions = useMemo(() => {
    return sortPoolQuestions(pool?.questions);
  }, [pool]);

  // If there are highlithed questions, scroll them into view
  useEffect(() => {
    if (
      !!pool?.questions &&
      !!questionsContainerRef.current &&
      questionsToHighlight.length > 0
    ) {
      const index = pool.questions.findIndex(
        (item) => item.id === questionsToHighlight[0]
      );

      if (index !== -1) {
        questionsContainerRef.current.children[index].scrollIntoView({
          block: "center",
          inline: "start",
          behavior: "smooth",
        });
      }
    }
  }, [questionsToHighlight, pool]);

  const handleCloseMovingAllQuestionsOutOfPool = () => {
    if (!poolDetailsOpenedId) {
      return;
    }

    dispatch({
      type: "move_all_questions_out_of_pool",
      payload: poolDetailsOpenedId,
    });
    closePoolDetails();
  };

  const handleMoveQuestionOutOfPool = (questionIndex: number) => {
    if (!poolDetailsOpenedId) {
      return;
    }

    dispatch({
      type: "move_question_out_of_pool",
      payload: {
        poolId: poolDetailsOpenedId,
        questionIndex,
      },
    });
  };

  const handleDeleteQuestionFromPool = (questionIndex: number) => {
    if (!poolDetailsOpenedId) {
      return;
    }

    dispatch({
      type: "delete_question_from_pool",
      payload: {
        poolId: poolDetailsOpenedId,
        questionIndex,
      },
    });
  };

  const openConfirmRemoveAllDialog = () => {
    dialog({
      title: "Remove All?",
      content: "Are you sure you want to remove all questions from the pool?",
      onConfirmLabel: "Remove",
      onDismissLabel: "Cancel",
      onConfirm: handleCloseMovingAllQuestionsOutOfPool,
    });
  };

  const handleClosePoolDetails = () => {
    if (pool.questions.length === 1) {
      dialog({
        title: "Last Question",
        content:
          "You only have one question left on the pool. If you close it, it will return the question as a single question back to the quiz builder.",
        onConfirmLabel: "Ok",
        onDismissLabel: "Cancel",
        onConfirm: closePoolDetails,
      });
    } else {
      closePoolDetails();
    }
  };

  if (typeof poolDetailsOpenedId !== "string" || !pool) {
    return null;
  }

  return (
    <>
      <div className="w-screen h-screen fixed inset-0 z-50 bg-slate-900 opacity-80" />

      <div className="fixed inset-1/2 bg-white flex flex-col -translate-x-1/2 -translate-y-1/2 px-6 rounded w-full z-50 min-h-min max-w-5xl h-full max-h-[80vh]">
        <div className="py-8 flex shrink-0 justify-center">
          <h4 className="text-2xl font-semibold text-gray-700 text-center">
            Edit Pool
          </h4>
        </div>
        <div
          className="absolute top-3 right-3 w-8 h-8 cursor-pointer"
          onClick={handleClosePoolDetails}
        >
          <Icon path={mdiClose} />
        </div>
        <div className="flex flex-1">
          <div className="flex flex-col w-full px-4">
            <div className="flex justify-between items-center mb-4">
              <div className="flex items-center gap-x-3">
                <span className="text-lg text-sfgray-700 opacity-80">
                  Choose
                </span>
                <Tooltip
                  open={showError}
                  animateClose
                  title={`Value must be between 1 and ${pool.questions.length}`}
                >
                  <form
                    key={poolPickToHighlight.inputKeyValue}
                    className={cn("quiz-item", {
                      "quiz-item-animate":
                        poolPickToHighlight.poolId === pool.id,
                    })}
                    onSubmit={(e) => {
                      e.preventDefault();
                      handleUpdatePoolPick();
                    }}
                    noValidate
                  >
                    <input
                      className="w-20 h-10 bg-white pl-4 pr-2 shadow-3xl text-slate-400"
                      value={pickStr}
                      onChange={handleSanitizeValue}
                      onBlur={handleUpdatePoolPick}
                      type="number"
                      name="pick"
                      min={1}
                      max={pool.questions.length}
                      step={1}
                    />
                  </form>
                </Tooltip>
                <span className="text-lg text-sfgray-700 opacity-80">
                  questions from the{" "}
                  <span className="font-semibold">{pool.questions.length}</span>{" "}
                  in the pool
                </span>
              </div>
              <Button variant="secondary" onClick={openConfirmRemoveAllDialog}>
                <Image src={removeAllIcon} alt="" />
                <span>Remove All from Pool</span>
              </Button>
            </div>
            <div className="flex flex-1 relative">
              <div
                ref={questionsContainerRef}
                className="flex flex-col overflow-y-auto absolute inset-0 px-2.5"
              >
                {sortedQuestions.map((question, index) => (
                  <QuizItem
                    key={question.id}
                    item={question}
                    index={index}
                    poolItem
                    highlight={questionsToHighlight.includes(question.id)}
                  />
                ))}
              </div>
            </div>
            <div className="flex justify-center items-center gap-x-4 py-8">
              <button
                className="h-[2.875rem] px-6 rounded-sm border cursor-pointer border-sfblue-500 disabled:bg-white disabled:border-gray-300 disabled:text-gray-400 disabled:pointer-events-none disabled:shadow-none bg-white text-sfblue-500 hover:bg-sfblue-50 active:bg-sfblue-100"
                disabled={!canUndo}
                onClick={undo}
              >
                <Image
                  src={undoIcon}
                  alt=""
                  className={`${!canUndo ? "opacity-50" : ""}`}
                />
              </button>
              <button
                className="h-[2.875rem] px-6 rounded-sm border cursor-pointer border-sfblue-500 disabled:bg-white disabled:border-gray-300 disabled:text-gray-400 disabled:pointer-events-none disabled:shadow-none bg-white text-sfblue-500 hover:bg-sfblue-50 active:bg-sfblue-100"
                disabled={!canRedo}
                onClick={redo}
              >
                <Image
                  src={redoIcon}
                  alt=""
                  className={`${!canRedo ? "opacity-50" : ""}`}
                />
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const usePoolPick = (pool: IPool) => {
  const { dispatch } = useQuiz();
  const [pickStr, setPickStr] = useState(String(pool?.pick));
  const [showError, setShowError] = useState(false);
  const timeoutRef = useRef<NodeJS.Timeout | null>(null);

  // Update the pick value if the pool is updated
  useEffect(() => {
    if (pool?.pick) {
      setPickStr(String(pool.pick));
    }
  }, [pool]);

  // Remove the error
  useEffect(() => {
    if (showError) {
      timeoutRef.current = setTimeout(() => {
        setShowError(false);
      }, 1500);
    }

    return () => {
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current);
      }
    };
  }, [showError]);

  const handleSanitizeValue = (event: React.ChangeEvent<HTMLInputElement>) => {
    const inputValue = event.target.value;
    // Remove any non-digit characters except for a single dot
    const sanitizedValue = inputValue.replace(/[^0-9.]/g, "");
    // Parse float values
    const dotIndex = sanitizedValue.indexOf(".");
    const finalValue =
      dotIndex !== -1 ? sanitizedValue.substring(0, dotIndex) : sanitizedValue;

    setPickStr(finalValue);
  };

  const handleUpdatePoolPick = () => {
    let value = parseInt(pickStr);
    let error = false;

    if (isNaN(value)) {
      setPickStr(String(pool?.pick));
      return;
    }

    if (value < 1) {
      error = true;
      value = 1;
    } else if (value > pool.questions.length) {
      error = true;
      value = pool.questions.length;
    }

    setPickStr(String(value));
    if (error && pool.questions.length > 0) {
      setShowError(true);
    }

    if (value !== pool.pick) {
      dispatch({
        type: "change_pool_pick",
        payload: {
          poolId: pool.id,
          pick: value,
        },
      });
    }
  };

  return {
    pickStr,
    showError,
    handleSanitizeValue,
    handleUpdatePoolPick,
  };
};

export { PoolDetails };
