import { CustomDroppable } from "$components/shared/Droppable";
import { api } from "$lib/api";
import { useCallback, useEffect, useMemo, useState } from "react";
import { AiOutlineSearch } from "react-icons/ai";
import { RxCaretDown, RxCaretUp } from "react-icons/rx";
import { useQuiz } from "../../../context/quiz/QuizContext";
import { findDuplicate } from "../../../utils/questions";
import { SearchQuestionItem } from "./SearchBarQuestionItem";

type LernosityItems = ReturnType<LernosityAuthorApp["getItemList"]>;

interface SearchBarProps {
  authorScriptLoaded: boolean;
}

function SearchBar({ authorScriptLoaded }: SearchBarProps) {
  const [expanded, setExpanded] = useState<boolean>(true);

  const toggleExpanded = useCallback(() => {
    setExpanded((current) => !current);
  }, []);

  if (!authorScriptLoaded) {
    return (
      <div
        className={`border-b-2 border-solid border-sfgray-400 flex flex-col relative ${
          expanded ? "w-[443px] px-6 pt-10 border-x-2" : "w-0.5 border-x"
        }`}
      >
        <button
          onClick={toggleExpanded}
          className="h-10 absolute -left-[86px] rotate-90 top-1/2 -translate-y-1/2 flex items-center px-3 border-x-2 border-b-2 border-solid border-sfgray-400 rounded-b-2xl bg-white gap-x-2"
        >
          <AiOutlineSearch size={20} color="#9e9e9e" />
          <span className="text-sm text-sfgray-400">Search</span>
          {expanded ? (
            <RxCaretUp size={24} color="#9e9e9e" />
          ) : (
            <RxCaretDown size={24} color="#9e9e9e" />
          )}
        </button>
      </div>
    );
  }

  return (
    <div className="relative border-r-2 border-solid border-sfgray-400">
      <div
        className={`overflow-hidden h-full border-l-2 border-solid border-sfgray-400 transition-all duration-300 ease-in-out ${
          expanded ? "w-[443px]" : "w-0"
        }`}
      >
        <div
          className={`min-h-full w-[443px] flex flex-col relative transition-all duration-300 ease-in-out px-6 pt-10 ${
            expanded ? "" : "translate-x-[443px]"
          }`}
        >
          <div className={"quiz-item-adv-search-container w-full relative"}>
            <div id="learnosity-author"></div>
          </div>
          <div className={"relative flex grow flex-1"}>
            <CustomDroppable droppableId="search-bar">
              {({ droppableProps, innerRef, placeholder }) => (
                <div
                  ref={innerRef}
                  {...droppableProps}
                  className="absolute inset-0 overflow-x-hidden overflow-y-auto"
                >
                  <ResultsList />
                  {placeholder}
                </div>
              )}
            </CustomDroppable>
          </div>
        </div>
      </div>
      <button
        onClick={toggleExpanded}
        className={`h-10 absolute rotate-90 top-1/2 -translate-y-1/2 flex items-center px-3 border-x-2 border-b-2 border-solid border-sfgray-400 rounded-b-3xl bg-white gap-x-2 ${
          expanded ? "-left-[84.5px]" : "-left-[86px]"
        }`}
      >
        <AiOutlineSearch size={20} color="#9e9e9e" />
        <span className="text-sm text-sfgray-400">Search</span>
        {expanded ? (
          <RxCaretUp size={24} color="#9e9e9e" />
        ) : (
          <RxCaretDown size={24} color="#9e9e9e" />
        )}
      </button>
    </div>
  );
}

const ResultsList = () => {
  const { data } = useQuiz();
  const {
    searchedItems,
    recentQuestions,
    authorAppRendered,
    loading,
    emptyResults,
  } = useSearchResults();
  const showRecent = searchedItems.length === 0;

  const items = useMemo(() => {
    if (showRecent) {
      return recentQuestions;
    }

    return searchedItems;
  }, [recentQuestions, searchedItems, showRecent]);

  if (!authorAppRendered) {
    return null;
  }

  if (loading) {
    return (
      <div className="w-full py-6">
        <h4 className="text-lg text-gray-600 font-semibold text-center italic">
          Loading...
        </h4>
      </div>
    );
  }

  if (emptyResults) {
    return (
      <div className="w-full py-6">
        <h4 className="text-lg text-gray-600 font-semibold text-center">
          No results found
        </h4>
      </div>
    );
  }

  return (
    <>
      {showRecent && (
        <div className="w-full py-6">
          <h4 className="text-lg text-gray-600 font-semibold text-center">
            {items.length === 0
              ? "No history dound"
              : "My recently edited questions"}
          </h4>
        </div>
      )}
      {items.map((item, index) => (
        <SearchQuestionItem
          key={item.reference}
          {...item}
          id={item.reference}
          type="question"
          index={index}
          isDuplicate={findDuplicate(data.items, item.reference)}
          weight={1}
        />
      ))}
    </>
  );
};

function useSearchResults() {
  const RESULTS_LIMIT = 20;
  const [authorApp, setAuthorApp] = useState<LernosityAuthorApp | null>(null);
  const [authorAppRendered, setAuthorAppRendered] = useState(false);
  const [loading, setLoading] = useState(false);
  const [baseItems, setBaseItems] = useState<LernosityItems>([]);
  const [emptySearch, setEmptySearch] = useState(true);

  const itemsToShowReferences = useMemo(() => {
    if (emptySearch) {
      return [];
    }

    return baseItems.slice(0, RESULTS_LIMIT).map((item) => item.reference);
  }, [baseItems, emptySearch]);

  const { data: items } = api.question.findSelected.useQuery(
    itemsToShowReferences,
    {
      enabled: !emptySearch,
      onSettled: () => setLoading(false),
    }
  );
  const { data: recentQuestions } = api.question.findLatest.useQuery();

  const { data: init } = api.quiz.authorInit.useQuery(undefined, {
    retry: false,
    refetchOnMount: false,
    cacheTime: 0,
    refetchOnWindowFocus: false,
  });

  useEffect(() => {
    if (typeof authorApp?.on === "function") {
      /**
       * Setting up author app listeners
       */

      authorApp.on("navigate", (args) => {
        const empty = args.data.location === "items";

        setEmptySearch(empty);
        if (!empty) {
          setLoading(true);
        }
      });

      authorApp.on("render:itemlist", async () => {
        setAuthorAppRendered(true);
        const itemList = authorApp.getItemList();
        if (Array.isArray(itemList)) {
          setBaseItems(itemList);
        }
      });
    }
  }, [authorApp]);

  useEffect(() => {
    if (init && !authorApp && window.LearnosityAuthor) {
      setAuthorApp(window.LearnosityAuthor.init(init));
    }
  }, [init, authorApp]);

  useEffect(() => {
    return () => {
      // TODO: Fix crash on hot reload, probably related to destroying the author app instance
      authorApp?.destroy();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    searchedItems: items || [],
    recentQuestions: recentQuestions || [],
    authorAppRendered,
    emptyResults: !emptySearch && !loading && baseItems.length === 0,
    loading,
  };
}

export { SearchBar };
