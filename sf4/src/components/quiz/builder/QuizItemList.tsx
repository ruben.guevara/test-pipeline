import { QuizItem } from "$components/quiz/builder/QuizItem";
import { CustomDroppable } from "$components/shared/Droppable";
import { useEffect, useRef, useState } from "react";
import { Draggable } from "react-beautiful-dnd";
import { useQuiz } from "../../../context/quiz/QuizContext";
import { PoolDetails } from "./PoolDetails";
import { QuizItemSkeleton } from "./QuizItem/QuizItemSkeleton";

function QuizItemList() {
  const [scrollIndex, setScrollIndex] = useState<
    number | "first" | "last" | null
  >(null);
  const { data, questionsToHighlight, poolDetailsOpenedId, loadingItems } =
    useQuiz();
  const ref = useRef<HTMLDivElement>(null);

  /**
   * Scroll list to a highlighted question
   */
  useEffect(() => {
    if (questionsToHighlight.length > 0) {
      // Get the index of the last removed question, scroll to that question
      const lastRemovedQuestionId =
        questionsToHighlight[questionsToHighlight.length - 1];
      const index = data.items.findIndex(
        (item) => item.id === lastRemovedQuestionId
      );

      if (index !== -1) {
        setScrollIndex(index);
      }
    }
  }, [data.items, questionsToHighlight]);

  const scrollToSelectedIndex = (index: number) => {
    ref.current?.children[0]?.children[index]?.scrollIntoView({
      block: "center",
      inline: "start",
      behavior: "smooth",
    });

    setScrollIndex(null);
  };

  useEffect(() => {
    if (scrollIndex !== null) {
      switch (scrollIndex) {
        case "first":
          scrollToSelectedIndex(0);
          break;
        case "last":
          scrollToSelectedIndex(data.items.length - 1);
          break;
        default:
          scrollToSelectedIndex(scrollIndex);
          break;
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [scrollIndex]);

  return (
    <div className="flex grow relative w-full" ref={ref}>
      <CustomDroppable
        droppableId="quiz-list"
        isDropDisabled={data.link_active}
        isCombineEnabled
      >
        {({ innerRef, droppableProps, placeholder }, { isDraggingOver }) => (
          <div
            ref={innerRef}
            {...droppableProps}
            className={`absolute inset-x-0 bottom-0 top-0 flex-col grow pt-5 overflow-y-auto ${
              isDraggingOver ? "bg-slate-100 rounded-lg" : "bg-white"
            }`}
          >
            {loadingItems ? (
              <>
                {[...Array(4)].map((_, index) => (
                  <QuizItemSkeleton key={index} />
                ))}
              </>
            ) : (
              <>
                {data.items?.map((item, index) => {
                  return (
                    <Draggable
                      key={item.id}
                      draggableId={item.id}
                      index={index}
                      isDragDisabled={
                        item.type === "question" && item.deactivated
                      }
                    >
                      {(provided, snapshot) => (
                        <QuizItem
                          key={item.id}
                          item={item}
                          index={index}
                          scrollIntoView={setScrollIndex}
                          highlight={
                            !poolDetailsOpenedId &&
                            questionsToHighlight.includes(item.id)
                          }
                          draggableProps={{ provided, snapshot }}
                        />
                      )}
                    </Draggable>
                  );
                })}
              </>
            )}

            {placeholder}
            {data.items.length === 0 && !isDraggingOver && (
              <div className="flex flex-col justify-center items-center mt-8">
                <h3 className="text-2xl font-bold text-sfgray-600 mb-3">
                  No Questions Yet
                </h3>
                <p className="text-base text-sfgray-600 opacity-50 italic">
                  You can add questions from the buttom bellow or <br />
                  find some to add from the search bar to the right
                </p>
              </div>
            )}
          </div>
        )}
      </CustomDroppable>

      <PoolDetails key={poolDetailsOpenedId} />
    </div>
  );
}

export { QuizItemList };
