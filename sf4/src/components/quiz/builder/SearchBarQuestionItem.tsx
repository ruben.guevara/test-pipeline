import { useCallback } from "react";
import { Draggable } from "react-beautiful-dnd";
import { useModal } from "../../../context/modal/ModalContext";
import { useQuiz } from "../../../context/quiz/QuizContext";
import { IQuestion } from "../../../context/quiz/types";
import { QuestionTypeIcon } from "./QuestionTypeIcon";

type QuestionProps = IQuestion & { isDuplicate: boolean; index: number };

export function SearchQuestionItem({
  isDuplicate,
  index,
  ...question
}: QuestionProps) {
  const { data, dispatch } = useQuiz();
  const { flyout } = useModal();
  const parser = new DOMParser();

  const onAddQuestionToQuizClick = useCallback(() => {
    if (!isDuplicate) {
      dispatch({
        type: "add_item_to_quiz",
        payload: { ...question, weight: 1 },
      });
    } else {
      flyout({
        title: "Duplicate",
        content:
          "You already have this question in your quiz! Pick another one!",
        onDismissLabel: "Back",
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isDuplicate]);

  return (
    <Draggable
      draggableId={JSON.stringify(question)}
      index={index}
      isDragDisabled={isDuplicate || data.link_active}
    >
      {({ draggableProps, dragHandleProps, innerRef }, { isDragging }) => (
        <div
          ref={innerRef}
          {...draggableProps}
          {...dragHandleProps}
          onClick={onAddQuestionToQuizClick}
          data-testid="question-container"
          className={`w-full overflow-hidden bg-white border-b border-solid border-neutral-400 ${
            isDragging ? "border h-20" : "border-b"
          } group${isDuplicate ? " opacity-50" : " cursor-pointer"}${
            data.link_active ? "pointer-events-none" : ""
          }`}
        >
          <div className="w-full overflow-hidden flex py-2.5">
            <div className="flex items-center justify-center grow-0 shrink-0 w-20">
              <div className="w-11 ">
                <QuestionTypeIcon
                  isDuplicate={isDuplicate}
                  type={question.questions.data[0].type}
                />
              </div>
            </div>
            <div className="flex flex-col flex-grow overflow-hidden">
              <p
                className="text-sm italic text-zinc-500 w-full overflow-hidden text-ellipsis whitespace-nowrap"
                data-testid="question-reference"
              >
                Ref: {question.id}
              </p>
              {!!question.title && (
                <h4
                  className="text-lg text-zinc-700 font-semibold w-full overflow-hidden text-ellipsis whitespace-nowrap"
                  data-testid="question-title"
                >
                  {question.title}
                </h4>
              )}
            </div>
          </div>

          <div className="hidden flex-col gap-y-6 group-hover:flex pb-4">
            <div className="text-base font-normal text-zinc-500 relative h-14 overflow-hidden">
              {parser
                .parseFromString(
                  question.questions.data.map((q) => q.data.stimulus).join(""),
                  "text/html"
                )
                .body.innerText.substring(0, 400)}
              <div className="w-full h-7 absolute inset-x-0 bottom-0 bg-gradient-to-t from-white to-transparent"></div>
            </div>
          </div>
        </div>
      )}
    </Draggable>
  );
}
