import { Checkbox } from "$components/shared/Checkbox";
import { KeyboardEventHandler, useEffect, useState } from "react";
import { DragDropContext, DropResult } from "react-beautiful-dnd";
import { useQuiz } from "../../../context/quiz/QuizContext";
import { IPool, IQuestion } from "../../../context/quiz/types";
import { QuizItemList } from "./QuizItemList";
import { QuizListActionBar } from "./QuizListActionBar";
import { QuizSelectionToolBar } from "./QuizSelectionToolbar";
import { SearchBar } from "./SearchBar";

type QuizContainerProps = {
  /**
   * Wait for the script to finish loading to render the search
   * TODO: Have a nicer UI to handle the script loading state, instead of just not rendering anything
   */
  authorScriptLoaded: boolean;
};

export function BuilderContainer({ authorScriptLoaded }: QuizContainerProps) {
  const [inBrowser, setInBrowser] = useState<boolean>(false);
  const {
    allItemsSelected,
    canRedo,
    canUndo,
    data,
    dispatch,
    loadingItems,
    redo,
    undo,
    onSelectAll,
    selected,
  } = useQuiz();

  const handleDragEnd = ({
    combine,
    destination,
    draggableId,
    source,
  }: DropResult) => {
    // if it's being dropped somewhere on the quiz list droppable or its content
    if (destination?.droppableId === "quiz-list") {
      // if it comes from the quiz
      if (source.droppableId === "quiz-list") {
        // sorting items
        const itemsList = [...data.items];
        const item = itemsList.splice(source.index, 1)[0];
        if (item) {
          itemsList.splice(destination.index, 0, item);
          dispatch({ type: "drop_item_on_quiz", payload: itemsList });
        }
        return;
      }

      // if it comes from the search bar
      if (source.droppableId === "search-bar") {
        // parse the data from the stringified item field
        const item = JSON.parse(draggableId) as IQuestion;
        const itemExists = !!data.items.find((i) => i.id == item.id);

        // if the items it not in the quiz already add it at specified index
        if (!itemExists) {
          dispatch({
            type: "add_item_to_quiz_at_index",
            payload: { item: { ...item, weight: 1 }, index: destination.index },
          });
        }
      }
    }

    // if it's being combined
    if (combine?.draggableId) {
      // it the items being dropped on is in the quiz list
      if (combine.droppableId === "quiz-list") {
        // if the item being dragged comes from the search bar
        const draggedOverItem =
          source.droppableId === "search-bar"
            ? (JSON.parse(draggableId) as IQuestion | IPool)
            : data.items.find((item) => item.id === draggableId);

        if (!draggedOverItem) return;
        const droppedOverItem = data.items.find(
          (item) => item.id === combine.draggableId
        );

        if (droppedOverItem) {
          // if the items being dropped on is a question
          if (
            droppedOverItem.type === "question" &&
            !droppedOverItem.deactivated
          ) {
            // if the item being dragged is a question
            if (draggedOverItem.type === "question") {
              const index = data.items.findIndex(
                (item) => item.id === droppedOverItem.id
              );

              dispatch({
                type: "merge_questions",
                payload: {
                  questions: [draggedOverItem, droppedOverItem],
                  index: index,
                },
              });
            }
          }

          // if the item being droped on is a pool
          if (droppedOverItem.type === "pool") {
            // if the item being dragged over is a question
            if (draggedOverItem.type === "question") {
              dispatch({
                type: "add_question_to_pool",
                payload: {
                  pool_id: droppedOverItem.id,
                  question: draggedOverItem,
                },
              });
            }

            if (draggedOverItem.type === "pool") {
              const index: number = data.items.findIndex(
                (item) => item.id === droppedOverItem.id
              );
              dispatch({
                type: "merge_pools",
                payload: {
                  pools: [droppedOverItem, draggedOverItem],
                  insertionIndex: index,
                },
              });
            }
          }
        }
      }
    }
  };

  const onKeyDown: KeyboardEventHandler<HTMLDivElement> = (e) => {
    if ((e.ctrlKey || e.metaKey) && e.shiftKey && e.key === "z") {
      if (data.link_active) return;
      if (canRedo) {
        redo();
      }
    } else if ((e.ctrlKey || e.metaKey) && e.key === "z") {
      if (data.link_active) return;
      if (canUndo) {
        undo();
      }
    }
  };

  useEffect(() => {
    if (typeof window !== "undefined") {
      setInBrowser(true);
    }
  }, []);

  if (!inBrowser) {
    return null;
  }

  return (
    <DragDropContext onDragEnd={handleDragEnd}>
      <div
        tabIndex={0}
        className="w-full flex flex-1 min-h-full"
        onKeyDown={onKeyDown}
      >
        <div className="px-12 flex flex-col flex-1">
          <div className="flex flex-1 flex-col grow">
            <div className="flex items-center justify-start mt-4 grow-0 gap-x-4 h-[50px]">
              <Checkbox
                label="Select all"
                checked={allItemsSelected}
                onChange={onSelectAll}
                disabled={data.link_active || loadingItems}
                name="select-all"
              />
              {!!selected.length && <QuizSelectionToolBar />}
            </div>
            <QuizItemList />
            <QuizListActionBar />
          </div>
        </div>
        <SearchBar authorScriptLoaded={authorScriptLoaded} />
      </div>
    </DragDropContext>
  );
}
