import { api } from "$lib/api";
import Image from "next/image";
import { useRouter } from "next/router";
import { useCallback } from "react";
import { useModal } from "../../../context/modal/ModalContext";
import { useQuiz } from "../../../context/quiz/QuizContext";
import redoIcon from "../images/redo.svg";
import undoIcon from "../images/undo.svg";

function QuizListActionBar() {
  const router = useRouter();
  const { data, canUndo, canRedo, redo, undo } = useQuiz();
  const { dialog } = useModal();

  const { mutate: unlink, isLoading: isUnlinking } =
    api.assessment.unlink.useMutation();

  const handleUnlinkQuiz = useCallback(() => {
    dialog({
      title: "Quiz linked",
      content: `This quiz is linked to '${data.parent_id}', you can't edit it unless you unlink it.`,
      onConfirmLabel: "Unlink",
      onDismissLabel: "Cancel",
      onConfirm: () => {
        unlink(data.id, {
          onSuccess: () => {
            router.reload();
          },
        });
      },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, unlink]);

  return (
    <div className="w-full flex justify-center items-center py-5 gap-x-4 relative">
      <button
        className="h-[2.875rem] px-6 rounded-sm border cursor-pointer border-sfblue-500 disabled:bg-white disabled:border-gray-300 disabled:text-gray-400 disabled:pointer-events-none disabled:shadow-none bg-white text-sfblue-500 hover:bg-sfblue-50 active:bg-sfblue-100"
        disabled={!canUndo}
        onClick={() => (data.link_active ? handleUnlinkQuiz() : undo())}
      >
        <Image
          src={undoIcon}
          alt=""
          className={`${!canUndo ? "opacity-50" : ""}`}
        />
      </button>
      <button
        className="h-[2.875rem] px-6 rounded-sm border cursor-pointer border-sfblue-500 disabled:bg-white disabled:border-gray-300 disabled:text-gray-400 disabled:pointer-events-none disabled:shadow-none bg-white text-sfblue-500 hover:bg-sfblue-50 active:bg-sfblue-100"
        disabled={!canRedo}
        onClick={() => (data.link_active ? handleUnlinkQuiz() : redo())}
      >
        <Image
          src={redoIcon}
          alt=""
          className={`${!canRedo ? "opacity-50" : ""}`}
        />
      </button>
    </div>
  );
}

export { QuizListActionBar };
