import { api } from "$lib/api";
import { useEffect } from "react";

export function QuizItemPreview(props: { id: string }) {
  const { data, isLoading } = api.question.findAll.useQuery(props.id, {
    retry: false,
    refetchOnMount: false,
  });
  const { data: init, isLoading: isLoadingInit } =
    api.quiz.previewInit.useQuery(
      { reference: props.id },
      {
        retry: false,
        refetchOnMount: false,
        cacheTime: 0,
        refetchOnWindowFocus: false,
      }
    );

  useEffect(() => {
    const itemsApp = (window as any).LearnosityItems.init(init);
    return () => itemsApp?.reset?.();
  }, [init]);

  if (isLoading || isLoadingInit) return null;
  if (!data?.data) {
    return <p>Question with reference &quot;{props.id}&quot; not found.</p>;
  }

  const question = data.data[0];
  return (
    <div className="w-full flex flex-col items-center">
      <h2 className="text-xl font-semibold mb-3">{question.title}</h2>
      <h3 className="text-lg italic mb-3">Ref: {question.reference}</h3>
      <div className="learnosity-item" data-reference={props.id}></div>
    </div>
  );
}
