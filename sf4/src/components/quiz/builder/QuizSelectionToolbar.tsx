import { Button } from "$components/shared/Button";
import Image from "next/image";
import React from "react";
import { useQuiz } from "../../../context/quiz/QuizContext";
import deleteIcon from "../images/delete.svg";
import poolIcon from "../images/pool-blue.svg";

function QuizSelectionToolBar() {
  const { data, onRemoveItems, selected, mergeSelected } = useQuiz();

  function cannotMerge(): boolean {
    return selected.length < 2;
  }

  return (
    <div className="flex items-center gap-x-3">
      {selected.length > 1 && (
        <Button type="button" disabled={cannotMerge()} onClick={mergeSelected}>
          <Image src={poolIcon} alt="" width={14} height={16} />
          <span>Merge into a Pool</span>
        </Button>
      )}

      <Button
        type="button"
        variant="danger"
        onClick={() => onRemoveItems(selected)}
      >
        <Image src={deleteIcon} alt="" width={16} height={16} />
        <span>Delete from quiz</span>
      </Button>
    </div>
  );
}

export { QuizSelectionToolBar };
