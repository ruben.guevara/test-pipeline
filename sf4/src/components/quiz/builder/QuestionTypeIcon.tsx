import Image from "next/image";
import classify from "../images/classify.svg";
import cloze from "../images/cloze.svg";
import plaintext from "../images/ctx.svg";
import lockIcon from "../images/lock.svg";
import mcq from "../images/mcq.svg";
import other from "../images/other.svg";

interface Props {
  type: string;
  isDuplicate?: boolean;
}

function QuestionTypeIcon(props: Props) {
  if (props.isDuplicate) {
    return <Image src={lockIcon} alt="" width={22} height={32} />;
  }

  switch (props.type) {
    case "mcq":
    case "choicematrix":
      return <Image src={mcq} alt="" width={74} height={65} />;
    case "plaintext":
      return <Image src={plaintext} alt="" width={68} height={77} />;
    case "clozetext":
      return <Image src={cloze} alt="" />;
    case "association":
    case "orderlist":
    case "rating":
      return <Image src={classify} alt="" />;
    default:
      return <Image src={other} alt="" />;
  }
}

export { QuestionTypeIcon };
