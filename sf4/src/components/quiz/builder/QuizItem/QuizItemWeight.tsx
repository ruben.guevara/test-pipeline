import { NumberField } from "$components/shared/NumberField";
import { useQuiz } from "../../../../context/quiz/QuizContext";
import { useQuizItemContext } from "./QuizItemContext";

const QuizItemWeight = ({ isDragging }: { isDragging: boolean }) => {
  const { item, poolItem } = useQuizItemContext();
  const { dispatch } = useQuiz();

  if (poolItem) {
    return null;
  }

  if (isDragging) {
    return <div className="w-36" />;
  }

  return (
    <div className="flex flex-col p-6 items-start justify-center">
      <div className="w-24">
        <NumberField
          label="Weight"
          name={item.id + "__weight"}
          value={item.weight as number}
          onChange={(value) =>
            dispatch({
              type: "change_item_weight",
              payload: {
                itemId: item.id,
                weight: Number(value),
              },
            })
          }
        />
      </div>
      <div className="w-24">
        <span className="text-base font-semibold opacity-80">Percentage</span>
        <div className="w-full h-9 shadow-3xl px-4 italic bg-white flex items-center text-slate-400">
          {new Intl.NumberFormat("en-US", {
            maximumFractionDigits: 2,
          }).format(item.percentage as number)}
          %
        </div>
      </div>
    </div>
  );
};

export { QuizItemWeight };
