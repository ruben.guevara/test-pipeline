import { Tooltip } from "$components/shared/Tooltip";
import cn from "classnames";
import Image from "next/image";
import React, { LegacyRef, useMemo } from "react";
import { useModal } from "../../../../context/modal/ModalContext";
import { useQuiz } from "../../../../context/quiz/QuizContext";
import { useTooltipContentCollision } from "../../../../hooks/useTooltipCollision";
import deleteIcon from "../../images/delete.svg";
import eyeIcon from "../../images/eye.svg";
import pencilIcon from "../../images/pencil.svg";
import removeQuestionIcon from "../../images/remove-question.svg";
import toAfterIcon from "../../images/toAfter.svg";
import toBeforeIcon from "../../images/toBefore.svg";
import toFirstIcon from "../../images/toFirst.svg";
import toLastIcon from "../../images/toLast.svg";
import { QuizItemPreview } from "../QuizItemPreview";
import { useQuizItemContext } from "./QuizItemContext";

type QuizItemToolbarProps = {
  tooltipContentProps: ReturnType<
    typeof useTooltipContentCollision
  >["collision"];
  scrollIntoView?: (index: number | "last") => void;
};

const QuizItemToolbar = (props: QuizItemToolbarProps) => {
  const { item, poolItem } = useQuizItemContext();

  return (
    <div
      className={cn("group-hover:flex absolute -top-2.5 -right-2.5", {
        hidden: !poolItem,
      })}
    >
      <div className="flex items-center h-10 rounded border-2 border-solid border-slate-300 bg-white px-1.5">
        <div className="flex items-center h-7 bg-gray-300 gap-x-px">
          <ActionButtons {...props} />
        </div>
      </div>
    </div>
  );
};

const ActionButtons = (props: QuizItemToolbarProps) => {
  const { item, poolItem, index } = useQuizItemContext();
  const {
    data: quiz,
    poolDetailsOpenedId,
    dispatch,
    openPoolDetails,
    onRemoveItems,
  } = useQuiz();
  const { popup } = useModal();

  const { isFirst, isLast } = useMemo(() => {
    return {
      isFirst: index === 0,
      isLast: index === quiz.items.length - 1,
    };
  }, [index, quiz]);

  const handleMoveQuestionOutOfPool = (questionIndex: number) => {
    if (!poolDetailsOpenedId) {
      return;
    }

    dispatch({
      type: "move_question_out_of_pool",
      payload: {
        poolId: poolDetailsOpenedId,
        questionIndex,
      },
    });
  };

  const handleDeleteQuestionFromPool = (questionIndex: number) => {
    if (!poolDetailsOpenedId) {
      return;
    }

    dispatch({
      type: "delete_question_from_pool",
      payload: {
        poolId: poolDetailsOpenedId,
        questionIndex,
      },
    });
  };

  const handleOpenQuestionPreview = () => {
    popup({
      title: "Preview Question",
      children: <QuizItemPreview id={item.id} />,
    });
  };

  if (item.type === "question" && Boolean(item.deactivated)) {
    return (
      <Tooltip
        title={"Remove this question"}
        contentProps={props.tooltipContentProps}
      >
        <ActionButton onClick={() => onRemoveItems([item.id])}>
          <Image src={deleteIcon} alt="" width={23.61} height={26} />
        </ActionButton>
      </Tooltip>
    );
  }

  if (poolItem) {
    return (
      <>
        <Tooltip
          title="Remove from this pool"
          contentProps={props.tooltipContentProps}
        >
          <ActionButton onClick={() => handleMoveQuestionOutOfPool(index)}>
            <Image src={removeQuestionIcon} alt="" width={26} height={21} />
          </ActionButton>
        </Tooltip>
        <Tooltip
          title="Edit this question"
          contentProps={props.tooltipContentProps}
        >
          <ActionButton>
            <Image src={pencilIcon} alt="" width={23} height={23} />
          </ActionButton>
        </Tooltip>
        <Tooltip
          title="Preview this question"
          contentProps={props.tooltipContentProps}
        >
          <ActionButton onClick={handleOpenQuestionPreview}>
            <Image src={eyeIcon} alt="" width={23} height={16} />
          </ActionButton>
        </Tooltip>
        <Tooltip
          title="Delete from this quiz"
          contentProps={props.tooltipContentProps}
        >
          <ActionButton onClick={() => handleDeleteQuestionFromPool(index)}>
            <Image src={deleteIcon} alt="" width={23.61} height={26} />
          </ActionButton>
        </Tooltip>
      </>
    );
  }

  return (
    <>
      <Tooltip
        title={
          item.type === "question" ? "Edit this question" : "Manage this pool"
        }
        contentProps={props.tooltipContentProps}
      >
        <ActionButton
          onClick={() => {
            if (item.type === "pool") {
              openPoolDetails(item.id);
            }
          }}
        >
          <Image src={pencilIcon} alt="" width={23} height={23} />
        </ActionButton>
      </Tooltip>

      {item.type === "question" && (
        <Tooltip
          title="Preview this question"
          contentProps={props.tooltipContentProps}
        >
          <ActionButton onClick={handleOpenQuestionPreview}>
            <Image src={eyeIcon} alt="" width={23} height={16} />
          </ActionButton>
        </Tooltip>
      )}

      <ActionButton
        disabled={isFirst}
        onClick={() => {
          dispatch({
            type: "send_item_to_first_position",
            payload: item.id,
          });
          props.scrollIntoView?.(0);
        }}
      >
        <Image
          src={toFirstIcon}
          alt=""
          width={19}
          height={17}
          className={`${isFirst ? "opacity-50" : ""}`}
        />
      </ActionButton>
      <ActionButton
        disabled={isFirst}
        onClick={() => {
          dispatch({ type: "send_item_up", payload: item.id });
          props.scrollIntoView?.(index - 1);
        }}
      >
        <Image
          src={toBeforeIcon}
          alt=""
          width={19}
          height={11}
          className={`${isFirst ? "opacity-50" : ""}`}
        />
      </ActionButton>
      <ActionButton
        disabled={isLast}
        onClick={() => {
          dispatch({ type: "send_item_down", payload: item.id });
          props.scrollIntoView?.(index + 1);
        }}
      >
        <Image
          src={toAfterIcon}
          alt=""
          width={19}
          height={11}
          className={`${isLast ? "opacity-50" : ""}`}
        />
      </ActionButton>
      <ActionButton
        disabled={isLast}
        onClick={() => {
          dispatch({
            type: "send_item_to_last_position",
            payload: item.id,
          });
          props.scrollIntoView?.("last");
        }}
      >
        <Image
          src={toLastIcon}
          alt=""
          width={19}
          height={17}
          className={`${isLast ? "opacity-50" : ""}`}
        />
      </ActionButton>
      <Tooltip
        title={
          item.type === "pool"
            ? "Remove this pool and all of its questions from this quiz"
            : "Remove this question"
        }
        contentProps={props.tooltipContentProps}
      >
        <ActionButton onClick={() => onRemoveItems([item.id])}>
          <Image src={deleteIcon} alt="" width={23.61} height={26} />
        </ActionButton>
      </Tooltip>
    </>
  );
};

const ActionButton = React.forwardRef(function ActionButton(
  props: {
    children: React.ReactNode;
  } & React.ComponentPropsWithoutRef<"button">,
  ref: LegacyRef<HTMLButtonElement>
) {
  const { children, ...buttonProps } = props;

  return (
    <button
      ref={ref}
      type="button"
      className="w-8 h-7 flex items-center justify-center bg-white"
      {...buttonProps}
    >
      {children}
    </button>
  );
});

export { QuizItemToolbar };
