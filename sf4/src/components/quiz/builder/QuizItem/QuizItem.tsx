import cn from "classnames";
import { useMemo } from "react";
import { DraggableProvided, DraggableStateSnapshot } from "react-beautiful-dnd";
import { useQuiz } from "../../../../context/quiz/QuizContext";
import { IPool, IQuestion } from "../../../../context/quiz/types";
import { useTooltipContentCollision } from "../../../../hooks/useTooltipCollision";
import { QuizItemCheckbox } from "./QuizItemCheckbox";
import { QuizItemContent } from "./QuizItemContent";
import QuizItemContext from "./QuizItemContext";
import { QuizItemToolbar } from "./QuizItemToolbar";
import { QuizItemWeight } from "./QuizItemWeight";

export type QuizItemProps = {
  /**
   * Quiz item
   */
  item: IQuestion | IPool;
  /**
   * Item index on the list
   */
  index: number;
  /**
   * Boolean indicating if it is a pool item or not
   */
  poolItem?: boolean;
  /**
   * Trigger a scroll into view
   */
  scrollIntoView?: (index: number | "last") => void;
  /**
   * Boolean indicating if the question should be highlighted
   */
  highlight?: boolean;
  /**
   * If the item is a draggable item, provided the arguments
   */
  draggableProps?: {
    provided: DraggableProvided;
    snapshot: DraggableStateSnapshot;
  };
};

const QuizItem = (props: QuizItemProps) => {
  const { item, index, poolItem, highlight, scrollIntoView, draggableProps } =
    props;
  const { data: quiz } = useQuiz();
  const { collision, ref } = useTooltipContentCollision(-12);

  // If it is a question, check if it is deactivated
  // If it is a pool, check if it has any deactivated question(s)
  const deactivatedItem = useMemo(() => {
    if (item.type === "question") {
      return Boolean(item.deactivated);
    }

    return item.questions.findIndex((item) => Boolean(item.deactivated)) !== -1;
  }, [item]);

  function canCombine(draggableId: string | null | undefined): boolean {
    if (!draggableId) return false;

    const comesFromSearchBar = draggableId.indexOf("reference") !== -1;
    if (comesFromSearchBar) {
      const draggedItem = JSON.parse(draggableId) as IQuestion | IPool;
      if (draggedItem.type === "question") {
        return true;
      }
      return false;
    }

    const item = quiz.items.find((q) => q.id === draggableId);
    if (!item) return false;

    if (item.type === "question" && !item.deactivated) {
      return true;
    }

    return false;
  }

  return (
    <QuizItemContext.Provider value={{ poolItem, item, index }}>
      <div
        ref={draggableProps?.provided.innerRef}
        {...(draggableProps?.provided.draggableProps || {})}
        {...(draggableProps?.provided.dragHandleProps || {})}
        className={`w-full flex items-stretch group bg-transparent relative py-2${
          quiz.link_active ? " pointer-events-none" : ""
        }`}
        data-testid="question-container"
      >
        <QuizItemCheckbox isDragging={!!draggableProps?.snapshot.isDragging} />
        <div
          ref={ref}
          className={cn(
            "flex flex-1 relative p-6 bg-white border-solid rounded shadow-box quiz-item",
            canCombine(draggableProps?.snapshot.combineTargetFor)
              ? "border-2 border-sfblue-400"
              : "shadow-box border-sfgray-300 border",
            {
              "quiz-item-animate": highlight,
              "py-3 shadow-highlight-danger": deactivatedItem,
            }
          )}
        >
          <QuizItemContent />
          <QuizItemToolbar
            tooltipContentProps={collision}
            scrollIntoView={scrollIntoView}
          />
        </div>
        <QuizItemWeight isDragging={!!draggableProps?.snapshot.isDragging} />
      </div>
    </QuizItemContext.Provider>
  );
};

export { QuizItem };
