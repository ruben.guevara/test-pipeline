/**
 * Skeleton component for the quiz item
 */
const QuizItemSkeleton = () => {
  return (
    <div className="flex h-[168px] animate-pulse py-2">
      <div className="w-10 flex items-center justify-center">
        <div className="h-6 w-6 bg-gray-200 rounded" />
      </div>

      <div className="flex flex-1 bg-gray-200 rounded"></div>

      <div className="w-36 grid grid-rows-2 items-center p-6">
        <div className="bg-gray-200 rounded w-full h-8" />
        <div className="bg-gray-200 rounded w-full h-8" />
      </div>
    </div>
  );
};

export { QuizItemSkeleton };
