import { createContext, useContext } from "react";
import { IPool, IQuestion } from "../../../../context/quiz/types";

const QuizItemContext = createContext<{
  item: IQuestion | IPool;
  index: number;
  poolItem?: boolean;
} | null>(null);

export function useQuizItemContext() {
  const context = useContext(QuizItemContext);
  if (!context) {
    throw new Error(
      "QuizItem.* component must be rendered as child of QuizItem component"
    );
  }
  return context;
}

export default QuizItemContext;
