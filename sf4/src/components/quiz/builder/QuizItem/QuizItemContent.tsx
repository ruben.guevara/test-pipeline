import { Button } from "$components/shared/Button";
import { api } from "$lib/api";
import Image from "next/image";
import { MdOutlineBlock } from "react-icons/md";
import { useQuiz } from "../../../../context/quiz/QuizContext";
import { sortPoolQuestions } from "../../../../context/quiz/utils";
import poolIcon from "../../images/pool.svg";
import { QuestionTypeIcon } from "../QuestionTypeIcon";
import { useQuizItemContext } from "./QuizItemContext";

const QuizItemContent = () => {
  const { item, poolItem, index } = useQuizItemContext();
  const { data, isLoading } = api.quiz.maxScore.useQuery([item.id]);
  const { poolDetailsOpenedId, dispatch, onRemoveItems } = useQuiz();
  const parser = new DOMParser();

  const handleDeleteQuestion = () => {
    if (poolItem && poolDetailsOpenedId) {
      dispatch({
        type: "delete_question_from_pool",
        payload: {
          poolId: poolDetailsOpenedId,
          questionIndex: index,
        },
      });
    } else {
      onRemoveItems([item.id]);
    }
  };

  if (item.type === "question" && item.deactivated) {
    return (
      <>
        <div className="flex flex-col flex-1 text-center justify-center items-center">
          <div>
            <MdOutlineBlock className="text-sfred-800" size={36} />
          </div>
          <div className="p-2">
            <p className="text-zinc-500">
              We’re sorry, question{" "}
              <span className="italic">
                <span className="font-bold">Ref:</span> ‘{item.id}’
              </span>{" "}
              has been disabled from our database,
              <br />
              can’t be used anymore and will be deleted from this quiz. Please
              try one of the following options
            </p>
          </div>
          <div className="grid grid-cols-1 gap-3">
            <Button size="sm" variant="danger" onClick={handleDeleteQuestion}>
              Delete question
            </Button>
          </div>
        </div>
        <div className="flex items-center justify-center w-10 h-10 rounded border-2 border-solid border-sfgray-300 absolute -top-2.5 -left-2.5 bg-white">
          <span className="font-semibold">
            <MdOutlineBlock className="text-sfred-800" size={22} />
          </span>
        </div>
      </>
    );
  }

  return (
    <>
      <div className="w-14 shrink-0 grow-0 flex items-center justify-start">
        <div className="w-7 h-7 flex items-center justify-center">
          {item.type === "pool" ? (
            <Image src={poolIcon} alt="" />
          ) : (
            <QuestionTypeIcon type={item.questions.data[0].type} />
          )}
        </div>
      </div>
      <div className="flex flex-col flex-1">
        {item.type === "question" ? (
          <>
            <h4 className="text-base italic text-sfgray-500 mb-4 gap-x-2 flex items-center">
              <span className="font-semibold">Ref:</span> {item.id}
              <span className="not-italic font-normal">|</span>
              {!isLoading && data && (
                <>
                  <span className="font-semibold">Max Score:</span>{" "}
                  {data && data[item.id]}
                </>
              )}
            </h4>
            <h3
              className="text-lg font-semibold text-gray-700"
              data-testid="question-title"
            >
              {item.title}
            </h3>
            <div className="text-base font-normal text-zinc-500 relative h-12 overflow-hidden">
              {parser
                .parseFromString(
                  item.questions.data.map((q) => q.data.stimulus).join(""),
                  "text/html"
                )
                .body.innerText.substring(0, 400)}
              <div className="w-full h-6 absolute inset-x-0 bottom-0 bg-gradient-to-t from-white to-transparent"></div>
            </div>
          </>
        ) : (
          <>
            <h4 className="text-base italic text-sfgray-500 mb-4 gap-x-2 flex items-center">
              <span>
                <span className="font-semibold">Ref:</span>{" "}
                {item.id.indexOf("new-pool") !== -1 ? "New Pool" : item.id}
              </span>
              <span className="not-italic font-normal">|</span>
              <span>
                <span className="font-semibold">{item.pick}</span>{" "}
                {item.pick > 1 ? "questions " : "question "}chosen from the{" "}
                <span className="font-semibold">{item.questions.length}</span>{" "}
                in the pool
              </span>
            </h4>
            <h3
              className="text-lg text-sfgray-500 italic"
              data-testid="pool-title"
            >
              Pool Content
            </h3>
            <div className="h-28 w-full overflow-y-auto">
              <div className="border-t-2 border-x-2 border-solid border-slate-300 w-fit">
                <div className="border-b-2 border-solid border-slate-300 flex h-8">
                  <div className="border-r-2 border-solid border-slate-300 h-full flex items-center justify-center w-14">
                    <span className="text-sm font-semibold">Type</span>
                  </div>
                  <div className="h-full flex items-center px-2.5">
                    <span className="text-sm font-semibold">Title</span>
                  </div>
                </div>
                {sortPoolQuestions(item.questions).map((question) => (
                  <div
                    key={question.id}
                    className="border-b-2 border-solid border-slate-300 h-9 flex"
                  >
                    <div className="border-r-2 border-solid border-slate-300 h-full flex items-center justify-center w-14">
                      <div className="w-5 h-5">
                        {question.deactivated ? (
                          <MdOutlineBlock
                            className="text-sfred-800"
                            size={22}
                          />
                        ) : (
                          <QuestionTypeIcon
                            type={question.questions.data[0].type}
                          />
                        )}
                      </div>
                    </div>
                    <div className="flex h-full items-center px-2.5">
                      <span className="text-sm font-semibold">
                        {question.deactivated
                          ? `Ref: ${question.id}`
                          : question.title}
                      </span>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </>
        )}
      </div>
      <div className="flex items-center justify-center w-10 h-10 rounded border-2 border-solid border-sfgray-300 absolute -top-2.5 -left-2.5 bg-white">
        <span className="font-semibold">{index + 1}</span>
      </div>
    </>
  );
};

export { QuizItemContent };
