import { Checkbox } from "$components/shared/Checkbox";
import { useQuiz } from "../../../../context/quiz/QuizContext";
import { useQuizItemContext } from "./QuizItemContext";

const QuizItemCheckbox = ({ isDragging }: { isDragging: boolean }) => {
  const { item, poolItem } = useQuizItemContext();
  const { selected, onSelectItem } = useQuiz();

  if (poolItem) {
    return null;
  }

  if (isDragging || (item.type === "question" && item.deactivated)) {
    return <div className="w-10 shrink-0" />;
  }

  return (
    <div className="w-10 flex items-center justify-start shrink-0">
      <Checkbox
        name={item.id}
        checked={selected.includes(item.id)}
        onChange={() => onSelectItem(item.id)}
      />
    </div>
  );
};

export { QuizItemCheckbox };
