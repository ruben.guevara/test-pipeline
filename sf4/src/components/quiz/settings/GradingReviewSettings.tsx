import { Checkbox } from "$components/shared/Checkbox";
import { Tooltip } from "$components/shared/Tooltip";
import Image from "next/image";
import { useContext } from "react";
import { useFormContext } from "react-hook-form";
import questionMarkIcon from "../images/question-mark.svg";
import { Section } from "./Section";
import { TooltipCollisionContext } from "./Seetings";
import { ConfigurationFormData } from "./schemas";

function GradingReviewSettings() {
  const { register, setValue, watch } = useFormContext<ConfigurationFormData>();
  const collision = useContext(TooltipCollisionContext);

  return (
    <Section title="Grading and Review">
      <div className="flex flex-col px-12 py-4 border-r border-solid border-sfgray-300">
        <div className="flex flex-col gap-y-5">
          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">
              Show Solutions
            </span>
            <Tooltip
              contentProps={collision}
              title="Specify whether or not detailed solutions are displayed when reviewing or grading an attempt"
            >
              <Image src={questionMarkIcon} width={22} height={22} alt="" />
            </Tooltip>
          </h3>
          <div className="flex items-center gap-x-4">
            <label
              className="text-sm text-black opacity-50"
              htmlFor="default.showSolutions"
            >
              Use default (No)
            </label>
            <Checkbox
              name="default.showSolutions"
              size="sm"
              checked={watch("default.showSolutions")}
              onChange={(checked) => {
                setValue("default.showSolutions", !checked, {
                  shouldTouch: true,
                  shouldValidate: true,
                  shouldDirty: true,
                });
              }}
            />
          </div>
          <input
            type="string"
            disabled={watch("default.showSolutions")}
            placeholder="Default (No)"
            className="w-full h-10 bg-white shadow-3xl px-2.5 italic"
            {...register("showSolutions")}
          />
          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">
              Show Grade Percentages
            </span>
            <Tooltip
              contentProps={collision}
              title="Specify whether or not the overall grades (as a percentage) are shown to students on the attempts summary table and when reviewing individual attempts."
            >
              <Image src={questionMarkIcon} width={22} height={22} alt="" />
            </Tooltip>
          </h3>
          <div className="flex items-center gap-x-4">
            <label
              className="text-sm text-black opacity-50"
              htmlFor="default.showGradePercentages"
            >
              Use default (Yes)
            </label>
            <Checkbox
              name="default.showGradePercentages"
              size="sm"
              checked={watch("default.showGradePercentages")}
              onChange={(checked) => {
                setValue("default.showGradePercentages", !checked, {
                  shouldTouch: true,
                  shouldValidate: true,
                  shouldDirty: true,
                });
              }}
            />
          </div>
          <input
            type="string"
            disabled={watch("default.showGradePercentages")}
            placeholder="Default (Yes)"
            className="w-full h-10 bg-white shadow-3xl px-2.5 italic"
            {...register("showGradePercentages")}
          />
          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">
              Show Scores
            </span>
            <Tooltip
              contentProps={collision}
              title="Specify whether or not the overall scores (as a fraction) are shown to students on the attempts summary table and when reviewing individual attempts."
            >
              <Image src={questionMarkIcon} width={22} height={22} alt="" />
            </Tooltip>
          </h3>
          <div className="flex items-center gap-x-4">
            <label
              className="text-sm text-black opacity-50"
              htmlFor="default.showScores"
            >
              Use default (Yes)
            </label>
            <Checkbox
              name="default.showScores"
              size="sm"
              checked={watch("default.showScores")}
              onChange={(checked) => {
                setValue("default.showScores", !checked, {
                  shouldTouch: true,
                  shouldValidate: true,
                  shouldDirty: true,
                });
              }}
            />
          </div>
          <input
            type="string"
            disabled={watch("default.showScores")}
            placeholder="Default (Yes)"
            className="w-full h-10 bg-white shadow-3xl px-2.5 italic"
            {...register("showScores")}
          />
        </div>
      </div>
      <div className="flex flex-col px-12 py-4 border-l border-solid border-sfgray-300">
        <div className="flex flex-col gap-y-5">
          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">
              Lock Reviews
            </span>
            <Tooltip
              contentProps={collision}
              title="Specify whether or not users can review submitted attempts."
            >
              <Image src={questionMarkIcon} width={22} height={22} alt="" />
            </Tooltip>
          </h3>
          <div className="flex items-center gap-x-4">
            <label
              className="text-sm text-black opacity-50"
              htmlFor="default.lockReviews"
            >
              Use default (No)
            </label>
            <Checkbox
              name="default.lockReviews"
              size="sm"
              checked={watch("default.lockReviews")}
              onChange={(checked) => {
                setValue("default.lockReviews", !checked, {
                  shouldTouch: true,
                  shouldValidate: true,
                  shouldDirty: true,
                });
              }}
            />
          </div>
          <input
            type="string"
            disabled={watch("default.lockReviews")}
            placeholder="Default (No)"
            className="w-full h-10 bg-white shadow-3xl px-2.5 italic"
            {...register("lockReviews")}
          />
          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">
              Grade Aggregation
            </span>
            <Tooltip
              contentProps={collision}
              title="Specify how grades are calculated for submitted attempts."
            >
              <Image src={questionMarkIcon} width={22} height={22} alt="" />
            </Tooltip>
          </h3>
          <div className="flex items-center gap-x-4">
            <label
              className="text-sm text-black opacity-50"
              htmlFor="default.gradeAggregation"
            >
              Use default (Highest Grade)
            </label>
            <Checkbox
              name="default.gradeAggregation"
              size="sm"
              checked={watch("default.gradeAggregation")}
              onChange={(checked) => {
                setValue("default.gradeAggregation", !checked, {
                  shouldTouch: true,
                  shouldValidate: true,
                  shouldDirty: true,
                });
              }}
            />
          </div>
          <input
            type="string"
            disabled={watch("default.gradeAggregation")}
            placeholder="Default (Highest Grade)"
            className="w-full h-10 bg-white shadow-3xl px-2.5 italic"
            {...register("gradeAggregation")}
          />
          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">
              Percentage Complete Required to Submit
            </span>
            <Tooltip
              contentProps={collision}
              title="Specify the percentage of questions that must be completed before an activity can be submitted. Do you want to keep students from leaving sections of the assessment blank?"
            >
              <Image src={questionMarkIcon} width={22} height={22} alt="" />
            </Tooltip>
          </h3>
          <div className="flex items-center gap-x-4">
            <label
              className="text-sm text-black opacity-50"
              htmlFor="default.percentageCompleteRequiredToSubmit"
            >
              Use default (No Criteria)
            </label>
            <Checkbox
              name="default.percentageCompleteRequiredToSubmit"
              size="sm"
              checked={watch("default.percentageCompleteRequiredToSubmit")}
              onChange={(checked) => {
                setValue(
                  "default.percentageCompleteRequiredToSubmit",
                  !checked,
                  {
                    shouldTouch: true,
                    shouldValidate: true,
                    shouldDirty: true,
                  }
                );
              }}
            />
          </div>
          <input
            type="number"
            disabled={watch("default.percentageCompleteRequiredToSubmit")}
            placeholder="Default (No Criteria)"
            className="w-full h-10 bg-white shadow-3xl px-2.5 italic"
            {...register("percentageCompleteRequiredToSubmit")}
          />
        </div>
      </div>
    </Section>
  );
}

export { GradingReviewSettings };
