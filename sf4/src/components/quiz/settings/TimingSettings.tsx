import { Checkbox } from "$components/shared/Checkbox";
import { Tooltip } from "$components/shared/Tooltip";
import Image from "next/image";
import { useContext } from "react";
import { useFormContext } from "react-hook-form";
import questionMarkIcon from "../images/question-mark.svg";
import { Section } from "./Section";
import { TooltipCollisionContext } from "./Seetings";
import { ConfigurationFormData } from "./schemas";

function TimingSettings() {
  const { register, setValue, watch } = useFormContext<ConfigurationFormData>();
  const collision = useContext(TooltipCollisionContext);

  return (
    <Section title="Timing">
      <div className="flex flex-col px-12 py-4 border-r border-solid border-sfgray-300">
        <div className="flex flex-col gap-y-5">
          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">
              Time limit
            </span>
            <Tooltip
              contentProps={collision}
              title="Specify the time allowed to complete the activity in minutes. If blank, students will have unlimited time. Do you want the activity to automatically submit after a certain amount of time? If so how long?"
            >
              <Image src={questionMarkIcon} width={22} height={22} alt="" />
            </Tooltip>
          </h3>
          <div className="flex items-center gap-x-4">
            <label
              className="text-sm text-black opacity-50"
              htmlFor="custom:max_attempts"
            >
              Use default (No limit)
            </label>
            <Checkbox
              name="default.timeLimit"
              size="sm"
              checked={watch("default.timeLimit")}
              onChange={(checked) => {
                setValue("default.timeLimit", !checked, {
                  shouldTouch: true,
                  shouldValidate: true,
                  shouldDirty: true,
                });
              }}
            />
          </div>
          <input
            type="string"
            disabled={watch("default.timeLimit")}
            placeholder="Default (No Limit)"
            className="w-full h-10 bg-white shadow-3xl px-2.5 italic"
            {...register("timeLimit")}
          />
          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">
              Hide time spent
            </span>
            <Tooltip
              contentProps={collision}
              title="Specify whether or not to display the 'time spent' timer in the assessment window. This setting is only toggleable when there is no 'time limit'."
            >
              <Image src={questionMarkIcon} width={22} height={22} alt="" />
            </Tooltip>
          </h3>
          <div className="flex items-center gap-x-4">
            <label
              className="text-sm text-black opacity-50"
              htmlFor="custom:max_attempts"
            >
              Use default (No)
            </label>
            <Checkbox
              name="deafult.hideTimeSpent"
              size="sm"
              checked={watch("default.hideTimeSpent")}
              onChange={(checked) => {
                setValue("default.hideTimeSpent", !checked, {
                  shouldTouch: true,
                  shouldValidate: true,
                  shouldDirty: true,
                });
              }}
            />
          </div>
          <input
            type="string"
            disabled={watch("default.hideTimeSpent")}
            placeholder="Default (No)"
            className="w-full h-10 bg-white shadow-3xl px-2.5 italic"
            {...register("hideTimeSpent")}
          />
        </div>
      </div>
      <div className="flex flex-col px-12 py-4 border-l border-solid border-sfgray-300">
        <div className="flex flex-col gap-y-5">
          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">
              Time limit warning
            </span>
            <Tooltip
              contentProps={collision}
              title="Specify when students should be warned how much time is left. Requires a Time Limit has been set. Do you want students to have an obvious warning that time is getting low?"
            >
              <Image src={questionMarkIcon} width={22} height={22} alt="" />
            </Tooltip>
          </h3>
          <div className="flex items-center gap-x-4">
            <label
              className="text-sm text-black opacity-50"
              htmlFor="custom:max_attempts"
            >
              Use default (No Warning)
            </label>
            <Checkbox
              name="default.timeLimitWarning"
              size="sm"
              checked={watch("default.timeLimitWarning")}
              onChange={(checked) => {
                setValue("default.timeLimitWarning", !checked, {
                  shouldTouch: true,
                  shouldValidate: true,
                  shouldDirty: true,
                });
              }}
            />
          </div>
          <input
            type="string"
            disabled={watch("default.timeLimitWarning")}
            placeholder="Default (No Warning)"
            className="w-full h-10 bg-white shadow-3xl px-2.5 italic"
            {...register("timeLimitWarning")}
          />
        </div>
      </div>
    </Section>
  );
}

export { TimingSettings };
