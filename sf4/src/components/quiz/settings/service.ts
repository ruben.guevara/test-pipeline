import { ConfigurationFormData, SettingConfig } from "./schemas";

function getInitialSettingsData(data: SettingConfig): ConfigurationFormData {
  return {
    default: {
      maximumNumberOfAttempts:
        data.maximumNumberOfAttempts === undefined ||
        data.maximumNumberOfAttempts === null,
      password: data.password === undefined || data.password === null,
      instructions:
        data.instructions === undefined || data.instructions === null,
      layout: data.layout === undefined || data.layout === null,
      timeLimit: data.timeLimit === undefined || data.timeLimit === null,
      hideTimeSpent:
        data.hideTimeSpent === undefined || data.hideTimeSpent === null,
      timeLimitWarning:
        data.timeLimitWarning === undefined || data.timeLimitWarning === null,
      previewTime: data.previewTime === undefined || data.previewTime === null,
      previewTimeWarning:
        data.previewTimeWarning === undefined ||
        data.previewTimeWarning === null,
      allowCheckingAnswersDuringAttempt:
        data.allowCheckingAnswersDuringAttempt === undefined ||
        data.allowCheckingAnswersDuringAttempt === null,
      showSolutions:
        data.showSolutions === undefined || data.showSolutions === null,
      showGradePercentages:
        data.showGradePercentages === undefined ||
        data.showGradePercentages === null,
      showScores: data.showScores === undefined || data.showScores === null,
      lockReviews: data.lockReviews === undefined || data.lockReviews === null,
      gradeAggregation:
        data.gradeAggregation === undefined || data.gradeAggregation === null,
      percentageCompleteRequiredToSubmit:
        data.percentageCompleteRequiredToSubmit === undefined ||
        data.percentageCompleteRequiredToSubmit === null,
    },
    ...data,
  };
}

function parseSettingsData(data: ConfigurationFormData): SettingConfig {
  const payload: SettingConfig = {
    ...(!data.default.maximumNumberOfAttempts
      ? { maximumNumberOfAttempts: data.maximumNumberOfAttempts }
      : {}),
    ...(!data.default.password ? { password: data.password } : {}),
    ...(!data.default.instructions ? { instructions: data.instructions } : {}),
    ...(!data.default.layout ? { layout: data.layout } : {}),
    ...(!data.default.timeLimit ? { timeLimit: data.timeLimit } : {}),
    ...(!data.default.hideTimeSpent
      ? { hideTimeSpent: data.hideTimeSpent }
      : {}),
    ...(!data.default.timeLimitWarning
      ? { timeLimitWarning: data.timeLimitWarning }
      : {}),
    ...(!data.default.previewTime ? { previewTime: data.previewTime } : {}),
    ...(!data.default.previewTimeWarning
      ? { previewTimeWarning: data.previewTimeWarning }
      : {}),
    ...(!data.default.allowCheckingAnswersDuringAttempt
      ? {
          allowCheckingAnswersDuringAttempt:
            data.allowCheckingAnswersDuringAttempt,
        }
      : {}),
    ...(!data.default.showSolutions
      ? { showSolutions: data.showSolutions }
      : {}),
    ...(!data.default.showGradePercentages
      ? { showGradePercentages: data.showGradePercentages }
      : {}),
    ...(!data.default.showScores ? { showScores: data.showScores } : {}),
    ...(!data.default.lockReviews ? { lockReviews: data.lockReviews } : {}),
    ...(!data.default.gradeAggregation
      ? { gradeAggregation: data.gradeAggregation }
      : {}),
    ...(!data.default.percentageCompleteRequiredToSubmit
      ? {
          percentageCompleteRequiredToSubmit:
            data.percentageCompleteRequiredToSubmit,
        }
      : {}),
  };
  console.log("payload no payload", payload);
  return payload;
}

export { getInitialSettingsData, parseSettingsData };
