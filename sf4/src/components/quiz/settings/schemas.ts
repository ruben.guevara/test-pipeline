import { z } from "zod";

const AssessmentConfigurationSchema = z.object({
  maximumNumberOfAttempts: z.number().optional().nullable(),
  password: z.string().optional().nullable(),
  instructions: z.string().optional().nullable(),
  layout: z.enum(["Standard", "Vertical"]).optional().nullable(),
  timeLimit: z.string().optional().nullable(),
  hideTimeSpent: z.enum(["Yes", "No"]).optional().nullable(),
  timeLimitWarning: z.string().optional().nullable(),
  previewTime: z.string().optional().nullable().nullable(),
  previewTimeWarning: z.string().optional().nullable(),
  allowCheckingAnswersDuringAttempt: z
    .enum(["Yes", "No"])
    .optional()
    .nullable(),
  showSolutions: z.enum(["Yes", "No"]).optional().nullable(),
  showGradePercentages: z.enum(["Yes", "No"]).optional().nullable(),
  showScores: z.enum(["Yes", "No"]).optional().nullable(),
  lockReviews: z.enum(["Yes", "No"]).optional().nullable(),
  gradeAggregation: z
    .enum(["Highest Grade", "Latest Grade", "Average Grade"])
    .optional()
    .nullable(),
  percentageCompleteRequiredToSubmit: z.number().optional().nullable(),
});

const ConfigFormSchema = AssessmentConfigurationSchema.extend({
  default: z.object({
    maximumNumberOfAttempts: z.boolean(),
    password: z.boolean(),
    instructions: z.boolean(),
    layout: z.boolean(),
    timeLimit: z.boolean(),
    hideTimeSpent: z.boolean(),
    timeLimitWarning: z.boolean(),
    previewTime: z.boolean(),
    previewTimeWarning: z.boolean(),
    allowCheckingAnswersDuringAttempt: z.boolean(),
    showSolutions: z.boolean(),
    showGradePercentages: z.boolean(),
    showScores: z.boolean(),
    lockReviews: z.boolean(),
    gradeAggregation: z.boolean(),
    percentageCompleteRequiredToSubmit: z.boolean(),
  }),
});

const INITIAL_SETTINGS = {
  default: {
    maximumNumberOfAttempts: true,
    password: true,
    instructions: true,
    layout: true,
    timeLimit: true,
    hideTimeSpent: true,
    timeLimitWarning: true,
    previewTime: true,
    previewTimeWarning: true,
    allowCheckingAnswersDuringAttempt: true,
    showSolutions: true,
    showGradePercentages: true,
    showScores: true,
    lockReviews: true,
    gradeAggregation: true,
    percentageCompleteRequiredToSubmit: true,
  },
};

type ConfigurationFormData = z.infer<typeof ConfigFormSchema>;
type SettingConfig = z.infer<typeof AssessmentConfigurationSchema>;

export { AssessmentConfigurationSchema, ConfigFormSchema, INITIAL_SETTINGS };
export { type ConfigurationFormData, type SettingConfig };
