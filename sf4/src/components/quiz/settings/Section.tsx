import Image from "next/image";
import { ReactNode, useCallback, useState } from "react";
import caretDownIcon from "../images/caret-down.svg";

interface SectionProps {
  title: string;
  children: ReactNode;
}

function Section({ title, children }: SectionProps) {
  const [expanded, setExpanded] = useState<boolean>(false);

  const handleToggleExpanded = useCallback(() => {
    setExpanded((current) => !current);
  }, []);

  return (
    <div className="flex flex-col">
      <div className="flex h-10 items-center">
        <h4 className="text-2xl font-semibold text-gray-700 pr-4">{title}</h4>
        <div className="flex flex-1 border-b-2 border-solid border-sfgray-400 relative">
          <button
            type="button"
            onClick={handleToggleExpanded}
            className="absolute right-0 -top-5 h-10 w-10 rounded-full border-2 border-solid border-sfgray-400 bg-white flex items-center justify-center"
          >
            <Image
              src={caretDownIcon}
              alt=""
              className={`${expanded ? "rotate-180" : ""}`}
            />
          </button>
        </div>
      </div>
      <div
        className={`${
          !expanded ? "h-0 overflow-hidden" : "h-auto"
        } grid grid-cols-2 my-3 transition-all`}
      >
        {children}
      </div>
    </div>
  );
}

export { Section };
