import { Button } from "$components/shared/Button";
import { api } from "$lib/api";
import { zodResolver } from "@hookform/resolvers/zod";
import Image from "next/image";
import { createContext, useEffect } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { useModal } from "../../../context/modal/ModalContext";
import { useQuiz } from "../../../context/quiz/QuizContext";
import { useTooltipContentCollision } from "../../../hooks/useTooltipCollision";
import checkIcon from "../images/check.svg";
import { GeneralSettings } from "./GeneralSettings";
import { GradingReviewSettings } from "./GradingReviewSettings";
import { PreviewSettings } from "./PreviewSettings";
import { TimingSettings } from "./TimingSettings";
import {
  AssessmentConfigurationSchema,
  ConfigFormSchema,
  ConfigurationFormData,
  INITIAL_SETTINGS,
} from "./schemas";
import { getInitialSettingsData, parseSettingsData } from "./service";

export const TooltipCollisionContext = createContext<
  ReturnType<typeof useTooltipContentCollision>["collision"]
>({});

interface SettingsProps {
  assessment_id: number;
  modal_id?: string;
}

function Settings() {
  const { data } = useQuiz();
  const { collision, ref } = useTooltipContentCollision(20);

  const { data: settings, isLoading } = api.assessment.fetchConfig.useQuery(
    data.id
  );

  const { mutate: saveChanges, isSuccess } =
    api.assessment.saveConfig.useMutation();

  const methods = useForm<ConfigurationFormData>({
    resolver: zodResolver(ConfigFormSchema),
    defaultValues: INITIAL_SETTINGS,
  });

  function handleSaveConfig(formData: ConfigurationFormData) {
    const payload = parseSettingsData(formData);
    saveChanges({ assessment_id: data.id, config: payload });
  }

  useEffect(() => {
    if (!!settings && !isLoading) {
      const parsed = AssessmentConfigurationSchema.parse(settings);
      const initialValues = getInitialSettingsData(parsed);

      methods.reset(initialValues);
    }
  }, [settings, isLoading, methods]);

  if (isLoading) {
    return (
      <div className="w-full py-6">
        <h4 className="text-lg text-gray-600 font-semibold text-center italic">
          Loading...
        </h4>
      </div>
    );
  }

  return (
    <FormProvider {...methods}>
      <form
        ref={ref}
        onSubmit={methods.handleSubmit(handleSaveConfig)}
        className="flex flex-1 flex-col"
      >
        <div className="flex flex-1 relative">
          <div className="absolute inset-0 overflow-y-auto py-6">
            <TooltipCollisionContext.Provider value={collision}>
              <GeneralSettings />
              <TimingSettings />
              <PreviewSettings />
              <GradingReviewSettings />
            </TooltipCollisionContext.Provider>
          </div>
        </div>
        <footer className="flex items-center justify-center py-6 gap-x-12 relative">
          <Button variant="primary" type="submit">
            Save
          </Button>
          {isSuccess && (
            <div className="w-auto flex h-16 absolute bg-sfblue-500 overflow-hidden rounded-full -top-20 left-1/2 -translate-x-1/2 animate-toast transition-all">
              <div className="w-full overflow-hidden relative">
                <div className="shrink grow overflow-hidden flex items-center justify-center h-16 px-24">
                  <span className="text-white whitespace-nowrap">
                    Changes saved!
                  </span>
                </div>
                <div className="w-16 h-16 rounded-full shrink-0 grow-0 flex items-center justify-center bg-white border-2 border-solid border-sfblue-500 absolute top-0 right-0">
                  <Image src={checkIcon} alt="" />
                </div>
              </div>
            </div>
          )}
        </footer>
      </form>
    </FormProvider>
  );
}

export { Settings };
