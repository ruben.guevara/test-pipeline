import { Checkbox } from "$components/shared/Checkbox";
import { Tooltip } from "$components/shared/Tooltip";
import Image from "next/image";
import { useContext } from "react";
import { useFormContext } from "react-hook-form";
import questionMarkIcon from "../images/question-mark.svg";
import { Section } from "./Section";
import { TooltipCollisionContext } from "./Seetings";
import { ConfigurationFormData } from "./schemas";

function GeneralSettings() {
  const { register, setValue, watch } = useFormContext<ConfigurationFormData>();
  const collision = useContext(TooltipCollisionContext);

  return (
    <Section title="General">
      <div className="flex flex-col px-12 py-4 border-r border-solid border-sfgray-300">
        <div className="flex flex-col gap-y-5">
          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">
              Maximum Number of Attempts
            </span>
            <Tooltip
              contentProps={collision}
              title="Specify the number of attempts a student is allowed on the assessment. Leave blank or set to 0 for unlimited attempts. Do you want the student to have 1 attempt, many attempts, or unlimited attempts?"
            >
              <Image src={questionMarkIcon} width={22} height={22} alt="" />
            </Tooltip>
          </h3>
          <div className="flex items-center gap-x-4">
            <label
              className="text-sm text-black opacity-50"
              htmlFor="default.maximumNumberOfAttempts"
            >
              Use default (No limit)
            </label>
            <Checkbox
              name="default.maximumNumberOfAttempts"
              size="sm"
              checked={watch("default.maximumNumberOfAttempts")}
              onChange={(checked) => {
                setValue("default.maximumNumberOfAttempts", !checked, {
                  shouldTouch: true,
                  shouldValidate: true,
                  shouldDirty: true,
                });
              }}
            />
          </div>
          <input
            type="number"
            disabled={watch("default.maximumNumberOfAttempts")}
            placeholder="Default (No Limit)"
            className="w-full h-10 bg-white shadow-3xl px-2.5 italic"
            {...register("maximumNumberOfAttempts", {
              valueAsNumber: true,
            })}
          />
          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">
              Quiz page css
            </span>
          </h3>
          <div className="flex items-center gap-x-4">
            <textarea className="w-full h-40 bg-sfgray-800" />
          </div>
        </div>
      </div>
      <div className="flex flex-col px-12 py-4 border-l border-solid border-sfgray-300">
        <div className="flex flex-col gap-y-5">
          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">
              Password
            </span>
            <Tooltip
              contentProps={collision}
              title="Specify a password that must be entered by the student before starting new attempts. Do you want to limit students' access by requiring a password?"
            >
              <Image src={questionMarkIcon} width={22} height={22} alt="" />
            </Tooltip>
          </h3>
          <div className="flex items-center gap-x-4">
            <label
              className="text-sm text-black opacity-50"
              htmlFor="default.password"
            >
              Use default (No Password)
            </label>
            <Checkbox
              name="default.password"
              size="sm"
              checked={watch("default.password")}
              onChange={(checked) => {
                setValue("default.password", !checked, {
                  shouldTouch: true,
                  shouldValidate: true,
                  shouldDirty: true,
                });
              }}
            />
          </div>
          <input
            type="string"
            disabled={watch("default.password")}
            placeholder="Default (No Password)"
            className="w-full h-10 bg-white shadow-3xl px-2.5 italic"
            {...register("password")}
          />

          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">
              Instructions
            </span>
            <Tooltip
              contentProps={collision}
              title="Specify a message to be displayed before the assessment."
            >
              <Image src={questionMarkIcon} width={22} height={22} alt="" />
            </Tooltip>
          </h3>
          <div className="flex items-center gap-x-4">
            <label
              className="text-sm text-black opacity-50"
              htmlFor="default.instructions"
            >
              Use default (No Instructions)
            </label>
            <Checkbox
              name="default.instructions"
              size="sm"
              checked={watch("default.instructions")}
              onChange={(checked) => {
                setValue("default.instructions", !checked, {
                  shouldTouch: true,
                  shouldValidate: true,
                  shouldDirty: true,
                });
              }}
            />
          </div>
          <input
            type="number"
            disabled={watch("default.instructions")}
            placeholder="Default (No Instructions)"
            className="w-full h-10 bg-white shadow-3xl px-2.5 italic"
            {...register("instructions")}
          />
          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">Layout</span>
            <Tooltip
              contentProps={collision}
              title="Specify 'standard' to render individual questions with number navigation. Specify 'vertical' to render all questions in a single page with a scrollbar."
            >
              <Image src={questionMarkIcon} width={22} height={22} alt="" />
            </Tooltip>
          </h3>
          <div className="flex items-center gap-x-4">
            <label
              className="text-sm text-black opacity-50"
              htmlFor="default.layout"
            >
              Use default (Standard)
            </label>
            <Checkbox
              name="default.layout"
              size="sm"
              checked={watch("default.layout")}
              onChange={(checked) => {
                setValue("default.layout", !checked, {
                  shouldTouch: true,
                  shouldValidate: true,
                  shouldDirty: true,
                });
              }}
            />
          </div>
          <input
            type="string"
            disabled={watch("default.layout")}
            placeholder="Default (Standard)"
            className="w-full h-10 bg-white shadow-3xl px-2.5 italic"
            {...register("layout")}
          />
        </div>
      </div>
    </Section>
  );
}

export { GeneralSettings };
