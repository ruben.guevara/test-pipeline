import { Checkbox } from "$components/shared/Checkbox";
import { Tooltip } from "$components/shared/Tooltip";
import Image from "next/image";
import { useContext } from "react";
import { useFormContext } from "react-hook-form";
import questionMarkIcon from "../images/question-mark.svg";
import { Section } from "./Section";
import { TooltipCollisionContext } from "./Seetings";
import { ConfigurationFormData } from "./schemas";

function PreviewSettings() {
  const { register, setValue, watch } = useFormContext<ConfigurationFormData>();
  const collision = useContext(TooltipCollisionContext);

  return (
    <Section title="Preview">
      <div className="flex flex-col px-12 py-4 border-r border-solid border-sfgray-300">
        <div className="flex flex-col gap-y-5">
          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">
              Preview Time
            </span>
            <Tooltip
              contentProps={collision}
              title="Specify the time a student has to preview the activity. During the Preview Time students are unable to answer questions or submit the activity. Do you want to force students to take some time to review the questions before they can start answering them?"
            >
              <Image src={questionMarkIcon} width={22} height={22} alt="" />
            </Tooltip>
          </h3>
          <div className="flex items-center gap-x-4">
            <label
              className="text-sm text-black opacity-50"
              htmlFor="default.previewTime"
            >
              Use default (No Preview)
            </label>
            <Checkbox
              name="default.previewTime"
              size="sm"
              checked={watch("default.previewTime")}
              onChange={(checked) => {
                setValue("default.previewTime", !checked, {
                  shouldTouch: true,
                  shouldValidate: true,
                  shouldDirty: true,
                });
              }}
            />
          </div>
          <input
            type="number"
            disabled={watch("default.previewTime")}
            placeholder="Default (No Preview)"
            className="w-full h-10 bg-white shadow-3xl px-2.5 italic"
            {...register("previewTime")}
          />
          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">
              Preview Time Warning
            </span>
            <Tooltip
              contentProps={collision}
              title="Specify when students are warned that the Preview Time will end. Do students need to know the assessment is about to start? How much warning do they need?"
            >
              <Image src={questionMarkIcon} width={22} height={22} alt="" />
            </Tooltip>
          </h3>
          <div className="flex items-center gap-x-4">
            <label
              className="text-sm text-black opacity-50"
              htmlFor="default.previewTimeWarning"
            >
              Use default (No Warning)
            </label>
            <Checkbox
              name="default.previewTimeWarning"
              size="sm"
              checked={watch("default.previewTimeWarning")}
              onChange={(checked) => {
                setValue("default.previewTimeWarning", !checked, {
                  shouldTouch: true,
                  shouldValidate: true,
                  shouldDirty: true,
                });
              }}
            />
          </div>
          <input
            type="string"
            disabled={watch("default.previewTimeWarning")}
            placeholder="Default (No Warning)"
            className="w-full h-10 bg-white shadow-3xl px-2.5 italic"
            {...register("previewTimeWarning")}
          />
        </div>
      </div>
      <div className="flex flex-col px-12 py-4 border-l border-solid border-sfgray-300">
        <div className="flex flex-col gap-y-5">
          <h3 className="flex items-center gap-x-2">
            <span className="text-xl text-gray-700 font-semibold">
              Allow Checking Answers During Attempt
            </span>
            <Tooltip
              contentProps={collision}
              title="Specify whether or not users can see their answers as they progress through the assessment. Usually only used for non-graded activities. Do you want students to be able to check their answers as they go? Is this a practice test?"
            >
              <Image src={questionMarkIcon} width={22} height={22} alt="" />
            </Tooltip>
          </h3>
          <div className="flex items-center gap-x-4">
            <label
              className="text-sm text-black opacity-50"
              htmlFor="default.allowCheckingAnswersDuringAttempt"
            >
              Use default (No)
            </label>
            <Checkbox
              name="default.allowCheckingAnswersDuringAttempt"
              size="sm"
              checked={watch("default.allowCheckingAnswersDuringAttempt")}
              onChange={(checked) => {
                setValue(
                  "default.allowCheckingAnswersDuringAttempt",
                  !checked,
                  {
                    shouldTouch: true,
                    shouldValidate: true,
                    shouldDirty: true,
                  }
                );
              }}
            />
          </div>
          <input
            type="string"
            disabled={watch("default.allowCheckingAnswersDuringAttempt")}
            placeholder="Default (No)"
            className="w-full h-10 bg-white shadow-3xl px-2.5 italic"
            {...register("allowCheckingAnswersDuringAttempt")}
          />
        </div>
      </div>
    </Section>
  );
}

export { PreviewSettings };
