import { Button } from "$components/shared/Button";
import { api } from "$lib/api";
import Image from "next/image";
import { useRouter } from "next/router";
import { useCallback } from "react";
import { useModal } from "../../context/modal/ModalContext";
import { useQuiz } from "../../context/quiz/QuizContext";
import { getChangesFromState } from "../../context/quiz/reducerFunctions";
import { BreadCrumb } from "./BreadCrumb";
import checkIcon from "./images/check.svg";
import relinkIcon from "./images/relink.svg";
import unlinkIcon from "./images/unlink.svg";

function QuizHeader() {
  const router = useRouter();
  const { status, mutate, isSuccess } = api.quiz.save.useMutation();

  const { data } = useQuiz();
  const { dialog } = useModal();
  const { mutate: unlink, isLoading: isUnlinking } =
    api.assessment.unlink.useMutation();
  const { mutate: relink, isLoading: isRelinking } =
    api.assessment.relink.useMutation();

  const handleSaveChanges = useCallback(() => {
    const payload = getChangesFromState(data);
    mutate(payload);
  }, [data, mutate]);

  const handleUnlinkQuiz = useCallback(() => {
    dialog({
      title: "Quiz linked",
      content: `This quiz is linked to '${data.parent_id}', you can't edit it unless you unlink it.`,
      onConfirmLabel: "Unlink",
      onDismissLabel: "Cancel",
      onConfirm: () => {
        unlink(data.id, {
          onSuccess: () => {
            router.reload();
          },
        });
      },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, unlink]);

  const handleRelinkQuiz = useCallback(() => {
    dialog({
      title: "Quiz unlinked",
      content: `Would you like to relink this quiz to '${data.parent_id}'? Remember, once you relink, all your changes will be lost and the quiz will return to the link's state.`,
      onConfirmLabel: "Relink",
      onDismissLabel: "Cancel",
      onConfirm: () => {
        relink(data.id, {
          onSuccess: () => {
            router.reload();
          },
        });
      },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, relink]);

  return (
    <>
      <div className="flex flex-col">
        <div className="flex justify-between items-center mt-8">
          <div className="flex items-center">
            <BreadCrumb />
          </div>
          <div className="flex gap-x-4">
            {!!data.parent_id && (
              <div className="flex items-center self-end">
                <span className="text-base italic text-slate-500 mr-6">
                  Parent Quiz: {data.parent_id}
                </span>
                {data.link_active && (
                  <Button
                    type="button"
                    onClick={handleUnlinkQuiz}
                    disabled={isUnlinking}
                    size="sm"
                  >
                    <Image src={unlinkIcon} width={28} height={14} alt="" />
                    <span>{isUnlinking ? "Unlinking..." : "Unlink"}</span>
                  </Button>
                )}
                {!data.link_active && (
                  <Button
                    type="button"
                    disabled={isRelinking}
                    onClick={handleRelinkQuiz}
                    size="sm"
                  >
                    <Image src={relinkIcon} width={28} height={14} alt="" />
                    <span>Relink</span>
                  </Button>
                )}
              </div>
            )}
            <Button
              variant="primary"
              size="sm"
              disabled={status === "loading"}
              onClick={() =>
                data.link_active ? handleUnlinkQuiz() : handleSaveChanges()
              }
            >
              {status === "loading" ? "Saving..." : "Save"}
            </Button>
            <Button size="sm">Save & Preview</Button>
            <Button size="sm">Publish</Button>
          </div>
        </div>
      </div>
      {isSuccess && (
        <div className="w-auto flex h-16 fixed z-50 bg-sfblue-500 overflow-hidden rounded-full top-20 left-1/2 -translate-x-1/2 animate-toast transition-all">
          <div className="w-full overflow-hidden relative">
            <div className="shrink grow overflow-hidden flex items-center justify-center h-16 px-24">
              <span className="text-white whitespace-nowrap">Quiz saved!</span>
            </div>
            <div className="w-16 h-16 rounded-full shrink-0 grow-0 flex items-center justify-center bg-white border-2 border-solid border-sfblue-500 absolute top-0 right-0">
              <Image src={checkIcon} alt="" />
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export { QuizHeader };
