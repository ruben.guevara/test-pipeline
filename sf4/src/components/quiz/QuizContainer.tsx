import { useCallback, useState } from "react";
import { HiOutlineCog8Tooth } from "react-icons/hi2";
import { RiHistoryLine } from "react-icons/ri";
import { TbTool } from "react-icons/tb";
import { QuizHeader } from "./QuizHeader";
import { BuilderContainer } from "./builder/BuilderContainer";
import { HistoryContainer } from "./history/HistoryContainer";
import { Settings } from "./settings/Seetings";

type Tab = "BUILDER" | "SETTINGS" | "HISTORY";

type QuizContainerProps = {
  /**
   * Wait for the script to finish loading to render the search
   * TODO: Have a nicer UI to handle the script loading state, instead of just not rendering anything
   */
  authorScriptLoaded: boolean;
};

export function QuizContainer({ authorScriptLoaded }: QuizContainerProps) {
  const [activeTab, setActiveTab] = useState<Tab>("BUILDER");

  const selectTab = useCallback(
    (val: Tab) => {
      if (activeTab !== val) {
        setActiveTab(val);
      }
    },
    [activeTab]
  );

  return (
    <div className="w-full bg-white">
      <div className="flex flex-col min-h-screen">
        <QuizHeader />
        <div className="flex justify-between items-start border-b-2 border-solid border-sfgray-400 pt-6">
          <div className="flex gap-x-2">
            <button
              type="button"
              className={`px-6 bg-white border-x-2 border-t-2 flex items-center rounded-t-lg gap-x-2 ${
                activeTab === "BUILDER"
                  ? "h-[3.375rem] -mb-0.5 border-sfgray-400 text-sfgray-600"
                  : "h-[3.25rem] border-sfblue-400 text-sfblue-400"
              }`}
              onClick={() => selectTab("BUILDER")}
            >
              <TbTool
                size={20}
                color={activeTab === "BUILDER" ? "#424242" : "#7FBDFB"}
              />
              <span>Builder</span>
            </button>
            <button
              type="button"
              className={`px-6 bg-white border-x-2 border-t-2 flex items-center rounded-t-lg gap-x-2 ${
                activeTab === "SETTINGS"
                  ? "h-[3.375rem] -mb-0.5 border-sfgray-400 text-sfgray-600"
                  : "h-[3.25rem] border-sfblue-400 text-sfblue-400"
              }`}
              onClick={() => selectTab("SETTINGS")}
            >
              <HiOutlineCog8Tooth
                size={20}
                color={activeTab === "SETTINGS" ? "#424242" : "#7FBDFB"}
              />
              <span>Settings</span>
            </button>
            <button
              type="button"
              className={`px-6 bg-white border-x-2 border-t-2 flex items-center rounded-t-lg gap-x-2 ${
                activeTab === "HISTORY"
                  ? "h-[3.375rem] -mb-0.5 border-sfgray-400 text-sfgray-600"
                  : "h-[3.25rem] border-sfblue-400 text-sfblue-400"
              }`}
              onClick={() => selectTab("HISTORY")}
            >
              <RiHistoryLine
                size={20}
                color={activeTab === "HISTORY" ? "#424242" : "#7FBDFB"}
              />
              <span>History</span>
            </button>
          </div>
        </div>
        <div className="flex flex-1">
          {activeTab === "BUILDER" && (
            <BuilderContainer authorScriptLoaded={authorScriptLoaded} />
          )}
          {activeTab === "SETTINGS" && <Settings />}
          {activeTab === "HISTORY" && <HistoryContainer />}
        </div>
      </div>
    </div>
  );
}
