import { useQuiz } from "../../context/quiz/QuizContext";

function BreadCrumb() {
  const { data, dispatch } = useQuiz();

  return (
    <div className="">
      <div className="font-normal text-lg leading-tight text-gray-700 flex items-center gap-3">
        <span className="whitespace-nowrap">{data.meta.region.name}</span> &gt;
        <span className="underline text-sfblue-600 whitespace-nowrap">
          {data.meta.course.name}
        </span>{" "}
        &gt;
        {!!data.meta.chapter && (
          <>
            <span className="underline text-sfblue-600 whitespace-nowrap">
              {data.meta.chapter.name}
            </span>{" "}
            &gt;
          </>
        )}
        <input
          id="quizName"
          className="w-full h-9 shadow-3xl px-4 italic"
          value={data.name}
          disabled={data.link_active}
          onChange={(e) =>
            dispatch({ type: "change_quiz_name", payload: e.target.value })
          }
        />
      </div>
    </div>
  );
}

export { BreadCrumb };
