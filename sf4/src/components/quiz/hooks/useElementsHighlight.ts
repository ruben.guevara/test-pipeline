import { useCallback, useEffect, useMemo, useRef, useState } from "react";

/**
 * Encapsulates the logic to get the elements to highlight
 */
function useElementsHighlight() {
  const [questions, setQuestions] = useState<string[][]>([]);
  const [poolPick, setPoolPick] = useState<{
    poolId: string | null;
    inputKeyValue: number;
  }>({
    poolId: null,
    inputKeyValue: 1,
  });
  const clearHighlightTimeoutIdRef = useRef<NodeJS.Timeout[]>([]);

  // Clear any remaining timeout when closing the page
  useEffect(() => {
    const clearHighlightTimeout = clearHighlightTimeoutIdRef.current;

    return () => {
      clearHighlightTimeout.forEach((timeoutId) => {
        clearTimeout(timeoutId);
      });
    };
  }, []);

  // Flattened questions to highlight
  const flattenQuestionsToHighlight = useMemo(() => {
    return questions.flat();
  }, [questions]);

  const addQuestionsToHighlight = useCallback((ids: string[]) => {
    setQuestions((current) => [...current, ids]);

    // Set the timeout to finish the animation, and also add the timeoutId in the
    // clearTimeoutIdsRef to make sure to clear it
    const timeoutId = setTimeout(() => {
      setQuestions((current) => {
        // Clear the timeoutId from the array
        clearHighlightTimeoutIdRef.current.shift();

        return current.slice(1);
      });
    }, 1600);
    clearHighlightTimeoutIdRef.current.push(timeoutId);
  }, []);

  const addPoolPicksToHighlight = useCallback((poolId: string) => {
    setPoolPick((current) => ({
      poolId,
      inputKeyValue: current.inputKeyValue + 1,
    }));
  }, []);

  const clearPoolPickToHighlight = useCallback(() => {
    setPoolPick((current) => ({
      poolId: null,
      inputKeyValue: current.inputKeyValue + 1,
    }));
  }, []);

  return {
    questionsToHighlight: flattenQuestionsToHighlight,
    addQuestionsToHighlight,
    poolPickToHighlight: poolPick,
    addPoolPicksToHighlight,
    clearPoolPickToHighlight,
  };
}

export { useElementsHighlight };
