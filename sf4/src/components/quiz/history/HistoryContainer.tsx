import { api } from "$lib/api";
import { useQuiz } from "../../../context/quiz/QuizContext";

interface HistoryProps {
  assessment_id: number;
}

function HistoryContainer() {
  const { data: quiz } = useQuiz();
  const { data, isLoading } = api.history.findAll.useQuery(quiz.id);

  if (isLoading) {
    return (
      <div className="w-full p-6">
        <h4 className="text-lg text-gray-600 font-semibold text-center italic">
          Loading...
        </h4>
      </div>
    );
  }

  return <div className="p-6 flex flex-col"></div>;
}

export { HistoryContainer };
