import { Loader as Spinner } from "$components/shared/Spinner";
import { mdiMenuDown, mdiMenuUp } from "@mdi/js";
import Icon from "@mdi/react";
import {
  Cell,
  Header,
  Table as ReactTable,
  flexRender,
} from "@tanstack/react-table";
import cn from "classnames";
import { MouseEventHandler, PropsWithChildren } from "react";

export function Table<TData extends unknown>({
  table,
}: {
  table: ReactTable<TData>;
}) {
  return (
    <table className="relative mb-4 min-w-full max-w-full overflow-hidden rounded-md bg-white px-4 py-2 shadow-md">
      <thead>
        {table.getHeaderGroups().map((headerGroup) => (
          <tr key={headerGroup.id} className="sticky top-0">
            {headerGroup.headers.map((header) => (
              <TableHeader key={header.id} header={header} />
            ))}
          </tr>
        ))}
      </thead>
      <tbody>
        {table.getRowModel().rows.map((row) => (
          <tr key={row.id} className="text-center hover:bg-orange-100">
            {row.getVisibleCells().map((cell) => (
              <TableCell key={cell.id} cell={cell} />
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
}

function TableHeader<TData extends unknown>({
  header,
}: {
  header: Header<TData, unknown>;
}) {
  const sortable = header.column.getCanSort();
  const sort = sortable && header.column.getIsSorted();
  const thClassName = cn(
    "bg-neutral-800 text-white font-semibold text-center",
    {
      "cursor-pointer": sortable,
    }
  );
  const spanClassName = cn(
    "flex items-center justify-center py-4 pl-4 gap-0 pr-6",
    {
      "pr-0": sort,
      "pr-6": !sort,
    }
  );
  return (
    <th
      className={thClassName}
      onClick={header.column.getToggleSortingHandler()}
    >
      {!header.isPlaceholder && (
        <span className={spanClassName}>
          {flexRender(header.column.columnDef.header, header.getContext())}
          {sort && (
            <Icon
              path={sort === "asc" ? mdiMenuUp : mdiMenuDown}
              size={1}
              className="flex-none"
            />
          )}
        </span>
      )}
    </th>
  );
}

function TableCell<TData extends unknown>({
  cell,
}: {
  cell: Cell<TData, unknown>;
}) {
  const type = cell.column.columnDef.meta?.type ?? "string";
  const className = cn("px-5 py-4 border", {
    "text-right": type === "number",
    "font-bold": type === "name",
    "text-left": type === "name",
    "whitespace-nowrap": ["name", "action", "id"].includes(type),
  });
  return (
    <td key={cell.id} className={className}>
      {flexRender(cell.column.columnDef.cell, cell.getContext())}
    </td>
  );
}

type TableActionProps = PropsWithChildren<{
  enabled: boolean;
  loading: boolean;
  onClick: MouseEventHandler<HTMLAnchorElement>;
}>;
export function TableAction({
  enabled,
  loading,
  onClick,
  children,
}: TableActionProps) {
  if (!enabled)
    return <span className="pointer-events-none text-gray-300">&#8212;</span>;
  const className = cn(
    "italic underline",
    loading && "pointer-events-none text-gray-400"
  );
  return (
    <a
      href="#"
      className={className}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      {loading && <Spinner />}
      {children}
    </a>
  );
}

export const TableDateFormat = new Intl.DateTimeFormat("en-GB", {
  month: "2-digit",
  day: "2-digit",
  year: "numeric",
});
