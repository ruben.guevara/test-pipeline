interface NumberFieldProps {
  label?: string;
  name: string;
  value: number;
  onChange: (value: number) => void;
  unit?: string;
  maximumFractionNumbers?: number;
}

function NumberField({
  label,
  name,
  value,
  onChange,
  maximumFractionNumbers = 2,
}: NumberFieldProps) {
  return (
    <div className="flex flex-col w-full">
      {!!label && (
        <label htmlFor={name} className="text-base font-semibold opacity-80">
          {label}
        </label>
      )}
      <input
        type="number"
        className="w-full h-9 shadow-3xl px-4 italic bg-white flex text-slate-400"
        value={new Intl.NumberFormat("en-US", {
          maximumFractionDigits: maximumFractionNumbers,
        }).format(value as number)}
        onChange={(e) => onChange(Number(e.target.value))}
      />
    </div>
  );
}

export { NumberField };
