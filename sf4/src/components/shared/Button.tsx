import cn from "classnames";
import Link from "next/link";
import { Route } from "nextjs-routes";
import { MouseEventHandler, ReactNode } from "react";

interface ButtonProps {
  variant?: "primary" | "secondary" | "danger";
  disabled?: boolean;
  children: ReactNode;
  type?: "submit" | "button";
  href?: string;
  size?: "sm" | "md" | "lg";
  onClick?: MouseEventHandler<HTMLButtonElement>;
  className?: string;
}

function Button({
  variant = "secondary",
  children,
  type = "button",
  size = "md",
  disabled,
  className = "",
  ...props
}: ButtonProps) {
  if (!!props.href) {
    return (
      <Link
        className={getClassNames({ variant, size, disabled, className })}
        href={props.href as unknown as Route}
      >
        {children}
      </Link>
    );
  }

  return (
    <button
      className={getClassNames({ variant, size, disabled, className })}
      type={type}
      disabled={disabled}
      onClick={props.onClick}
    >
      {children}
    </button>
  );
}

function getClassNames({
  variant,
  size,
  disabled,
  className,
}: {
  variant: "primary" | "secondary" | "danger";
  size: "sm" | "md" | "lg";
  disabled?: boolean;
  className: string;
}): string {
  return cn(defaultStyle, {
    "px-3.5 py-1.5": size === "sm",
    "px-5 py-3 min-w-[7.5rem]": size === "md",
    "h-16 px-12": size === "lg",
    "bg-white text-sfblue-500 border-sfblue-500 hover:bg-sfblue-50 active:bg-sfblue-100":
      variant === "secondary",
    "bg-white text-sfred-800 border-sfred-800 hover:bg-red-50 active:bg-red-100":
      variant === "danger",
    "bg-sfblue-500 text-white border-sfblue-500 hover:bg-sfblue-600 hover:border-sfblue-600 shadow-3xl active:shadow-none":
      variant === "primary",
    "bg-white border-gray-300 text-gray-400 pointer-events-none shadow-none":
      disabled,
    className,
  });
}

const defaultStyle =
  "flex gap-x-3 items-center justify-center txt-sm rounded-sm border cursor-pointer text-center disabled:bg-white disabled:border-gray-300 disabled:text-gray-400 disabled:pointer-events-none disabled:shadow-none";

export { Button };
