//TODO: fix this eslint-disable

/* eslint-disable react/display-name */
import cn from "classnames";
import {
  ComponentPropsWithRef,
  FC,
  ForwardedRef,
  HTMLProps,
  RefAttributes,
  forwardRef,
} from "react";

export const Input = forwardRef<HTMLInputElement>(
  ({ className, ref, ...props }: ComponentPropsWithRef<"input">) => (
    <input
      ref={ref}
      className={cn(
        "w-full border-none bg-white px-2 py-1 drop-shadow-md placeholder:text-xs placeholder:italic placeholder:text-gray-400",
        className
      )}
      {...props}
    />
  )
);

export const Form = forwardRef<HTMLFormElement>(
  ({ className, children, ref, ...props }: ComponentPropsWithRef<"form">) => (
    <form
      className={cn("mb-10 flex w-full max-w-xl flex-col gap-10", className)}
      {...props}
      ref={ref}
    >
      {children}
    </form>
  )
);

export const FormRow = ({
  className,
  children,
  ...props
}: HTMLProps<HTMLDivElement>) => (
  <div className="lex flex-col gap-2">{children}</div>
);

export const SubmitCancelRow = ({
  className,
  children,
  ...props
}: HTMLProps<HTMLDivElement>) => (
  <div className={cn("flex justify-between", className)} {...props}>
    {children}
  </div>
);

export const Label = ({
  className,
  children,
  ...props
}: HTMLProps<HTMLLabelElement>) => (
  <label className={cn("text-xl font-semibold", className)} {...props}>
    {children}
  </label>
);

export const Select = ({
  className,
  children,
  ref,
  ...props
}: HTMLProps<HTMLSelectElement>) => (
  <select
    ref={ref}
    className={cn(
      "w-full cursor-pointer border-none bg-white px-2 py-1 drop-shadow-md placeholder:text-xs placeholder:italic placeholder:text-gray-400",
      className
    )}
    {...props}
  >
    {children}
  </select>
);
