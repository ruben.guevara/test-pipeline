import * as RadixTooltip from "@radix-ui/react-tooltip";
import cn from "classnames";
import React from "react";

export type TooltipProps = {
  children: React.ReactNode;
  /**
   * Tooltip title
   */
  title: string | React.ReactNode;
  /**
   * Animate the tooltip close, with a fade out animation
   */
  animateClose?: boolean;
  triggerProps?: RadixTooltip.TooltipTriggerProps;
  portalProps?: RadixTooltip.TooltipPortalProps;
  contentProps?: RadixTooltip.TooltipContentProps;
  arrowProps?: RadixTooltip.TooltipArrowProps;
} & RadixTooltip.TooltipProps;

const Tooltip = (props: TooltipProps) => {
  const {
    children,
    title,
    animateClose = false,
    triggerProps,
    portalProps,
    contentProps,
    arrowProps,
    ...tooltipProps
  } = props;

  return (
    <RadixTooltip.Root {...tooltipProps} disableHoverableContent>
      <RadixTooltip.Trigger asChild {...triggerProps}>
        {children}
      </RadixTooltip.Trigger>
      <RadixTooltip.Portal {...portalProps}>
        <RadixTooltip.Content
          className={cn(
            "TooltipContent bg-sfgray-600 p-4 rounded-lg text-white text-sm max-w-xs z-50",
            { "TooltipContent-animate-close": animateClose }
          )}
          sideOffset={5}
          side="bottom"
          hideWhenDetached
          {...contentProps}
        >
          {title}
          <RadixTooltip.Arrow
            className="fill-sfgray-600"
            height={11}
            width={22}
            {...arrowProps}
          />
        </RadixTooltip.Content>
      </RadixTooltip.Portal>
    </RadixTooltip.Root>
  );
};

export { Tooltip };
