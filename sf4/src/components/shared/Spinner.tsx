import { mdiLoading } from "@mdi/js";
import Icon from "@mdi/react";
import { IconProps } from "@mdi/react/dist/IconProps";

export const Loader = (props: Pick<IconProps, "style" | "className">) => {
  const style = {
    marginRight: "0.5em",
    display: "inline",
    ...props.style,
  };
  return <Icon path={mdiLoading} size="1.2em" spin={1} style={style} />;
};
