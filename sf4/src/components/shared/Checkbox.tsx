import Image from "next/image";
import check from "./assets/check.svg";

interface CheckboxProps {
  name: string;
  checked: boolean;
  onChange: (checked: boolean) => void;
  label?: string;
  disabled?: boolean;
  size?: "sm" | "md";
}

function Checkbox({
  name,
  checked,
  onChange,
  label,
  disabled = false,
  size = "md",
  ...props
}: CheckboxProps) {
  return (
    <div className="flex items-center justify-start">
      <input
        type="checkbox"
        className="hidden"
        onChange={(e) => onChange(!e.target.checked)}
        id={name}
        name={name}
        disabled={disabled}
      />
      <label
        htmlFor={name}
        className={`flex items-center justify-start gap-x-2 cursor-pointer ${
          disabled ? "opacity-50" : ""
        }`}
      >
        <div
          className={`${
            size === "sm" ? "w-5 h-5" : "w-6 h-6"
          } rounded-md border border-solid border-gray-400 bg-white flex items-center justify-center shadow-input`}
        >
          {checked && <Image src={check} alt="" />}
        </div>
        {!!label && (
          <span className="text-lg text-slate-500 italic">{label}</span>
        )}
      </label>
    </div>
  );
}

export { Checkbox };
