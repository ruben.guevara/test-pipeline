import { mdiClose } from "@mdi/js";
import Icon from "@mdi/react";
import { ReactElement, ReactNode, cloneElement, isValidElement } from "react";
import { useModal } from "../../context/modal/ModalContext";
import { Button } from "./Button";

export type DialogProps = {
  title: string;
  type: "dialog";
  content: string;
  onConfirmLabel: string;
  onConfirm: Function;
  onDismissLabel: string;
};

export type FlyoutProps = {
  title: string;
  type: "flyout";
  content: string;
  onDismissLabel: string;
};

export interface PopUpProps {
  title: string;
  type: "container";
  children: ReactNode;
}

export type ModalProps = {
  id: string;
} & (DialogProps | FlyoutProps | PopUpProps);

function Modal(props: ModalProps) {
  const { remove } = useModal();

  function onDismiss() {
    remove(props.id);
  }

  switch (props.type) {
    case "dialog":
      return (
        <div
          className="fixed inset-1/2 bg-gray-50 flex flex-col -translate-x-1/2 -translate-y-1/2 px-8 rounded w-full max-w-lg z-50 min-h-min"
          role="dialog"
        >
          <div className="py-4 relative">
            <h4 className="text-xl font-semibold text-gray-700" role="heading">
              {props.title}
            </h4>
          </div>
          <div className="py-6">
            <p className="text-base text-gray-700">{props.content}</p>
          </div>
          <div className="flex justify-center items-center pt-6 pb-8 gap-x-12">
            <Button onClick={onDismiss} data-testid="dialog-dismiss-button">
              {props.onDismissLabel}
            </Button>
            <Button
              variant="primary"
              onClick={() => {
                props.onConfirm();
                onDismiss();
              }}
              data-testid="dialog-confirm-button"
            >
              {props.onConfirmLabel}
            </Button>
          </div>
        </div>
      );
    case "flyout":
      return (
        <div
          className="fixed inset-1/2 bg-gray-50 flex flex-col -translate-x-1/2 -translate-y-1/2 px-8 rounded w-full max-w-lg z-50 min-h-min"
          role="dialog"
        >
          <div className="py-4">
            <h4 className="text-xl font-semibold text-gray-700">
              {props.title}
            </h4>
          </div>
          <div className="py-6">
            <p className="text-base text-gray-700">{props.content}</p>
          </div>
          <div className="flex justify-center items-center pt-6 pb-8">
            <Button onClick={onDismiss} data-testid="flyout-dismiss-button">
              {props.onDismissLabel}
            </Button>
          </div>
        </div>
      );
    case "container":
      const content = isValidElement(props.children)
        ? cloneElement(props.children as ReactElement, {
            modal_id: props.id,
          })
        : props.children;
      return (
        <div className="fixed inset-1/2 bg-white flex flex-col -translate-x-1/2 -translate-y-1/2 px-6 rounded w-full z-50 min-h-min max-w-5xl h-full max-h-[80vh]">
          <div className="py-8 flex shrink-0 justify-center">
            <h4 className="text-2xl font-semibold text-gray-700 text-center">
              {props.title}
            </h4>
          </div>
          <div
            className="absolute top-3 right-3 w-8 h-8 cursor-pointer"
            onClick={onDismiss}
          >
            <Icon path={mdiClose} />
          </div>
          <div className="flex flex-1">{content}</div>
        </div>
      );
  }
}

export { Modal };
