import { Button } from "$components/shared/Button";
import { Loader } from "$components/shared/Spinner";
import { api } from "$lib/api";
import { Region } from "@prisma/client";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useForm } from "react-hook-form";

export type RegionFormProps = {
  region?: Region;
};

type FormDataType = {
  name: string;
  country_id: string | number | null;
};

export function RegionForm({ region }: RegionFormProps) {
  const {
    register,
    handleSubmit,
    formState: { errors },
    resetField,
  } = useForm<FormDataType>({
    values: region,
  });
  const utils = api.useContext();
  const {
    mutate: save,
    isLoading: saving,
    error: saveError,
  } = api.region.update.useMutation({
    onSuccess: () => {
      utils.region.list.invalidate();
      if (region) {
        utils.region.find.invalidate(region.id);
      }
      push("/manage/regions");
    },
  });
  const { push } = useRouter();
  const onSubmit = ({ name, country_id }: FormDataType) => {
    save({
      name,
      country_id: Number(country_id) || null,
      id: region?.id ?? null,
    });
  };
  const { data: countries, isLoading } = api.country.findAll.useQuery(
    undefined,
    { staleTime: 1000 * 60 * 60 }
  );
  useEffect(
    () =>
      resetField("country_id", {
        keepDirty: true,
        defaultValue: region?.country_id || "",
      }),
    [isLoading, resetField, region]
  );

  return (
    <form
      className="mb-10 flex w-full max-w-xl flex-col gap-10"
      onSubmit={handleSubmit(onSubmit)}
    >
      {saveError && <p role="alert">{saveError.message}</p>}
      <div className="lex flex-col gap-2">
        <label className="text-xl font-semibold">
          <span title="Required">*</span>
          Name
        </label>
        <input
          className="w-full border-none bg-white px-2 py-1 drop-shadow-md placeholder:text-xs placeholder:italic placeholder:text-gray-400"
          type="text"
          placeholder="Region Name"
          {...register("name", { required: true })}
        />
        {errors.name?.type === "required" && (
          <p role="alert">Name is required</p>
        )}
      </div>
      <div className="lex flex-col gap-2">
        <label className="text-xl font-semibold">
          <span title="Required">*</span>
          Country
        </label>
        <select
          className="w-full cursor-pointer border-none bg-white px-2 py-1 drop-shadow-md placeholder:text-xs placeholder:italic placeholder:text-gray-400"
          {...register("country_id", { required: "Select a country" })}
        >
          {isLoading ? (
            <option value={region?.country_id || undefined}>Loading...</option>
          ) : (
            <>
              <option disabled value="">
                Select a Country
              </option>
              {countries?.map((country) => (
                <option key={country.id} value={country.id}>
                  {country.name}
                </option>
              ))}
            </>
          )}
        </select>
      </div>
      <div className="flex justify-between">
        <Button href="/manage/regions" disabled={saving}>
          Cancel
        </Button>
        <Button disabled={saving} variant="primary" className="relative">
          {saving && (
            <Loader style={{ position: "absolute", left: "0.7rem" }} />
          )}
          {region ? "Save Changes" : "Create"}
        </Button>
      </div>
    </form>
  );
}
