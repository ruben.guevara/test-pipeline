import { Table, TableAction, TableDateFormat } from "$components/shared/Table";
import { api } from "$lib/api";
import type { AppRouter } from "$server/api/root";
import NoSSR from "@mpth/react-no-ssr";
import {
  SortingState,
  createColumnHelper,
  getCoreRowModel,
  getSortedRowModel,
  useReactTable,
} from "@tanstack/react-table";
import type { inferProcedureOutput } from "@trpc/server";
import Link from "next/link";
import { useCallback, useMemo, useState } from "react";

type Region = inferProcedureOutput<AppRouter["region"]["list"]>[0];

const columnHelper = createColumnHelper<Region>();

export function RegionTable() {
  const utils = api.useContext();
  const { mutate: deleteRegion } = api.region.delete.useMutation({
    onSuccess: () => {
      utils.region.list.invalidate();
    },
  });
  const { data: regions } = api.region.list.useQuery(undefined, {
    refetchOnMount: false,
    staleTime: 1000 * 60 * 5,
  });
  const [deleting, setDeleting] = useState<number[]>([]);
  const onDeleteClick = useCallback(
    (region: Region) => {
      if (confirm("Are you sure you want to delete this region?")) {
        setDeleting((prev) => [...prev, region.id]);
        deleteRegion(region.id, {
          onSuccess: () => {
            utils.region.list.invalidate();
          },
          onSettled: () => {
            setDeleting((prev) => prev.filter((id) => id !== region.id));
          },
        });
      }
    },
    [setDeleting, deleteRegion, utils.region.list]
  );

  const [sorting, setSorting] = useState<SortingState>([]);

  const columns = useMemo(
    () => [
      columnHelper.accessor("id", {
        header: "ID",
        meta: {
          type: "id",
        },
      }),
      columnHelper.accessor("name", {
        header: "Name",
        meta: {
          type: "name",
        },
      }),
      columnHelper.accessor((r) => r.country?.name, {
        header: "Country",
      }),
      columnHelper.accessor((r) => r.date_last_updated, {
        header: "Date Last Updated",
        cell: ({ getValue }) => {
          return <NoSSR>{TableDateFormat.format(getValue())}</NoSSR>;
        },
      }),
      columnHelper.accessor((r) => r._count.authors, {
        header: "Num Authors",
      }),
      columnHelper.accessor((r) => r._count.courses, {
        header: "Num Courses",
      }),
      columnHelper.accessor((r) => r._count.institutions, {
        header: "Num Institutions",
      }),
      columnHelper.accessor((r) => r._count.partners, {
        header: "Num Partners",
      }),
      columnHelper.display({
        header: "Edit",
        meta: { type: "action" },
        cell: ({ row }) => {
          const region = row.original;
          return (
            <Link
              href={{
                pathname: "/manage/regions/[id]/edit",
                query: { id: String(region.id) },
              }}
              className="italic underline"
            >
              Edit
            </Link>
          );
        },
      }),
      columnHelper.display({
        header: "Delete",
        meta: { type: "action" },
        cell: ({ row }) => {
          const region = row.original;
          const canDelete =
            region._count.authors === 0 &&
            region._count.courses === 0 &&
            region._count.institutions === 0 &&
            region._count.partners === 0;
          return (
            <TableAction
              enabled={canDelete}
              loading={deleting.includes(region.id)}
              onClick={() => onDeleteClick(region)}
            >
              Delete
            </TableAction>
          );
        },
      }),
    ],
    [deleting, onDeleteClick]
  );

  const table = useReactTable({
    columns,
    data: regions ?? [],
    state: {
      sorting,
    },
    onSortingChange: setSorting,
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
  });

  return <Table table={table} />;
}
