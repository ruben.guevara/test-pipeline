import { prisma } from "$server/db/prisma";
import { User } from "@prisma/client";

export const createAssessmentVersion = async (assessmentId: number, user?: User) =>{

  const assessment = await prisma.assessment.findUnique({
    where: {
      id: assessmentId
    },
    include: {
      assessment_questions: {
        include: {
          pool: true
        }
      },
      configurations: true,
    },
  });  
  
  await prisma.assessmentVersion.create({
    data: {
      assessment_id: assessmentId,
      user_id: user ? user.id : null,
      version_data: JSON.stringify(assessment),
      timestamp: new Date().getTime(),
    }
  })
} 