import { config } from "../config/config";

const LearnositySDK = require("learnosity-sdk-nodejs");

export interface LearnositySecurity {
  [key: string]: string;
}

export const LearnosityVersion = config.api.learnosity.version;

export class Learnosity {
  private consumerKey: string;
  private domain: string;
  private secret: string;
  private version: string;
  private sdk;

  private security: LearnositySecurity;

  constructor() {
    this.consumerKey = config.api.learnosity.consumerKey;
    this.domain = config.app.publicAppUrl.hostname;
    this.secret = config.api.learnosity.secret;
    this.version = config.api.learnosity.version;
    this.sdk = new LearnositySDK();

    this.security = {
      domain: this.domain,
      consumer_key: this.consumerKey,
    };
  }

  overrideSecurity(options: LearnositySecurity): LearnositySecurity {
    this.security = {
      ...this.security,
      ...options,
    };

    return this.security;
  }

  public generateApi(request: any, api: string, actionType?: string) {
    return this.sdk.init(api, this.security, this.secret, request, actionType);
  }

  async dataApiRequest(
    endpoint: string,
    request: any,
    actionType: string = "get"
  ) {
    const dataAPIRequest = this.generateApi(request, "data", actionType);

    const form = new FormData();
    form.append("security", dataAPIRequest.security);
    form.append("request", dataAPIRequest.request);
    form.append("action", dataAPIRequest.action);

    const url = `https://data.learnosity.com/${this.version}/${endpoint}`;

    const dataAPIResponse = await fetch(url, {
      method: "POST",
      body: form,
    });

    return dataAPIResponse.json();
  }
}
