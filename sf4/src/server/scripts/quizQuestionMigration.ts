import { createAssessmentVersion } from "$lib/version";
import { Learnosity } from "../../lib/learnosity";
import { prisma } from "../db/prisma";

(async() => { 

  let assessments;
  let pagination = 0;
  const queryLimit = 1000;
  const learnosity = new Learnosity();

  do {
    assessments = await prisma.assessment.findMany({
      skip: pagination * queryLimit,
      take: queryLimit,
      include: {
        assessment_questions: true,
      },
    });

    pagination++;
    console.error(`Proccesing page ${pagination} - take: ${queryLimit}, assessment found: ${assessments.length}`);
    const assessmentQuestions: any[] = [];
    const versions: any[] = [];

    await Promise.all(
      assessments.map(async (assessment) => {
        if (!assessment.reference) {
          console.error(`No item reference for assessment_id: ${assessment.id}`);
          return;
        }

        const assessmentQuestionsCount = assessment?.assessment_questions?.length ?? 0;
        if (assessmentQuestionsCount > 0) {
          console.error(`Ignoring assessment_id: ${assessment.id}, has ${assessmentQuestionsCount} assessment questions`);
          return;
        }

        /**
         * Get activities by assessment
         */
        const responseActivities: any = await learnosity.dataApiRequest('itembank/activities', {
          "item_references": {
            "all": [assessment.reference]
          }
        });

        if (!responseActivities || !responseActivities.meta || !responseActivities.meta.status) {
          console.error(`No response for assessment_id: ${assessment.id} - reference: "${assessment.reference}"`);
          return;
        }

        for (let i = 0; i < responseActivities.meta.records; i++) {
          const activity = responseActivities.data[i];

          /**
           * Iterating all activitie's item
           */
          for (let j = 0; j < activity.data.items.length; j++) {
            const item = activity.data.items[j];
            const itemReference = typeof item === 'object' ? item.reference : item;

            /**
             * Get questions and max_score
             */
            const responseQuestions: any = await learnosity.dataApiRequest('itembank/questions', {
              "item_references": [itemReference]
            });
    
            if (!responseQuestions || !responseQuestions.meta || !responseQuestions.meta.status) {
              console.error(`No response question for assessment_id: ${assessment.id} - item reference: "${itemReference}"`);
              return;
            }

            const maxScore = responseQuestions.data[itemReference].reduce((accumulator: number, object: any) => {
              return accumulator + (object.data?.validation?.max_score ?? 0);
            }, 0);

            const assessmentQuestion = {
              assessment_id: assessment.id,
              item_reference: itemReference,
              sort_order: j,
              weight: maxScore,
            }

            assessmentQuestions.push(assessmentQuestion);
          }
        }

        versions.push({
          assessmentId: assessment.id,
          userId: assessment.updated_by_id,
        })
      })
    )

    console.log(JSON.stringify(
      await prisma.assessmentQuestion.createMany({
        data: assessmentQuestions
      })
    ));

    await Promise.all(versions.map(async version => {
      await createAssessmentVersion(version.assessmentId, version.userId);
    }));

  } while (assessments && assessments.length == queryLimit);
  
})()