import { prisma } from "$server/db/prisma";
import { Context, Prisma, User } from "@prisma/client";

export const PermissionName = [
  "assessment_edit",
  "region_create",
  "region_edit",
  "region_delete",
  "class_student_manage",
  "class_teacher_manage",
  "class_student_view_as",
  "class_teacher_view_as",
  "class_change_dcourse",
  "group_join",
  "assessment_edit",
  "learnosity_item_create",
  "learnosity_item_edit",
  "learnosity_item_edit_all",
] as const;
export type PermissionName = (typeof PermissionName)[number];
export const isPermissionName = (name: string): name is PermissionName => {
  return PermissionName.includes(name as PermissionName);
};

export type CheckpointsParams<P extends PermissionName = PermissionName> = {
  user: RequireId<User> | null;
  permissions: P[];
  context?: RequireId<Context> | "root";
};

export async function checkpoints<P extends PermissionName>({
  user,
  permissions,
  context,
}: CheckpointsParams<P>): Promise<Record<P, boolean>> {
  let results = permissions.reduce(
    (acc, permission) => ({ ...acc, [permission]: false }),
    {}
  ) as Record<P, boolean>;
  if (!user) {
    return results;
  }
  const contextId = context === "root" ? 1 : context?.id;

  let rawResults: {
    permission: P;
    value: "ALLOW" | "DENY" | "NOT_SET";
  }[];

  if (contextId) {
    rawResults = await prisma.$queryRaw<typeof rawResults>`
SELECT permission, value
FROM (SELECT r.priority,
             p.name permission,
             pv.value
      FROM sf_role_assignment ra
          JOIN sf_role r ON ra.role_id = r.id
          JOIN sf_context c1 ON ra.context_id = c1.id
          JOIN sf_context c2 ON c2.lft >= c1.lft AND c2.rgt <= c1.rgt
          JOIN sf_permission_value pv ON r.id = pv.role_id
          JOIN sf_permission p ON pv.permission_id = p.id
      WHERE ra.user_id = ${user.id}
        AND ra.deleted = 0
        AND c2.id = ${contextId}
        AND p.name IN (${Prisma.join(permissions)})
      UNION
      DISTINCT
      SELECT r.priority,
             p.name permission,
             pv.value
      FROM sf_role r
          JOIN sf_permission_value pv ON r.id = pv.role_id
          JOIN sf_permission p ON p.id = pv.permission_id
      WHERE r.name = 'guest'
        AND p.name IN (${Prisma.join(permissions)})) a
ORDER BY permission, priority
`;
  } else {
    rawResults = await prisma.$queryRaw<typeof rawResults>`
SELECT permission, value
FROM (SELECT r.priority,
             p.name permission,
             pv.value
      FROM sf_role_assignment ra
          JOIN sf_role r ON ra.role_id = r.id
          JOIN sf_permission_value pv ON r.id = pv.role_id
          JOIN sf_permission p ON pv.permission_id = p.id
      WHERE ra.user_id = ${user.id}
        AND ra.deleted = 0
        AND p.name IN (${Prisma.join(permissions)})
      UNION
      DISTINCT
      SELECT r.priority,
             p.name permission,
             pv.value
      FROM sf_role r
          JOIN sf_permission_value pv ON r.id = pv.role_id
          JOIN sf_permission p ON p.id = pv.permission_id
      WHERE r.name = 'guest'
        AND p.name IN (${Prisma.join(permissions)})) a
ORDER BY permission, priority
    `;
  }

  let prevPermission: P | null = null;
  for (let { permission, value } of rawResults) {
    if (value === "ALLOW") {
      if (permission !== prevPermission) {
        results[permission] = true;
      }
    } else if (value === "DENY") {
      if (permission !== prevPermission) {
        results[permission] = false;
      }
    } else if (value === "NOT_SET") {
      continue;
    }
    prevPermission = permission;
  }

  return results;
}

export type CheckpointParams<P extends PermissionName> = {
  user: RequireId<User> | null;
  permission: P;
  context?: RequireId<Context> | "root";
};

export async function checkpoint({ user, permission, context }: CheckpointParams<PermissionName>) {
  const result = await checkpoints({ user, permissions: [permission], context });
  return result[permission];
}

export async function checkpointOr<P extends PermissionName>(params: CheckpointsParams<P>) {
  const result = await checkpoints(params);
  return Object.values(result).some((value) => value);
}

export async function checkpointAnd<P extends PermissionName>(params: CheckpointsParams<P>) {
  const result = await checkpoints(params);
  return Object.values(result).every((value) => value);
}
