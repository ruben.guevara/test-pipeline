import { AbortProps } from "$customTypes/Errors";
import { User } from "@prisma/client";
import unserialize from "locutus/php/var/unserialize";
import { GetServerSidePropsContext } from "next";
import { RequestCookies } from "next/dist/server/web/spec-extension/cookies";
import { prisma } from "../db/prisma";
import { redis } from "../redis";
import { CheckpointsParams, checkpointOr, checkpoints } from "./accessControl";

export default async function getUserFromCookies(
  cookies: RequestCookies | Partial<{ [key: string]: string }>
) {
  const sessionId =
    cookies instanceof RequestCookies
      ? cookies.get("PHPSESSID")?.value
      : cookies.PHPSESSID;
  if (sessionId) {
    // console.log("sessionId", sessionId);
    const rawPhpSession = await redis?.get(`session${sessionId}`);
    // console.log("rawPhpSession", rawPhpSession);
    const security = unserialize(rawPhpSession?.split("|")[1])?._security_main;
    const userId = parseInt(
      unserialize(
        security?.substring(security?.indexOf(`AppBundle\\Entity\\User`) + 28)
      )[5]
    );
    return !userId
      ? null
      : await prisma.user.findUnique({
          where: {
            id: userId,
          },
        });
  }

  return null;
}

export async function getUser(
  req: GetServerSidePropsContext["req"],
  permissionsOr?: CheckpointsParams["permissions"],
  context?: CheckpointsParams["context"]
): Promise<[User, null] | [null, AbortProps]> {
  const user = await getUserFromCookies(req.cookies);
  if (!user) {
    return [null, { statusCode: 401 }];
  }

  if (permissionsOr) {
    const hasPermission = await checkpointOr({
      user,
      permissions: permissionsOr,
      context,
    });
    if (!hasPermission) {
      return [null, { statusCode: 403 }];
    }
  }

  return [user, null];
}

export async function validateUserPermissions(
  user: User,
  permissionsOr: CheckpointsParams["permissions"],
): Promise<[User, null] | [null, AbortProps]> {
  const hasPermission = await checkpointOr({
    user,
    permissions: permissionsOr
  });
  if (!hasPermission) {
    return [null, { statusCode: 403 }];
  }
  
  return [user, null];
}