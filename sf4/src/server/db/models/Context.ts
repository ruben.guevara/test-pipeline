import { Prisma, PrismaClient } from "@prisma/client";

// don't allow nested create with children
type ContextCreateInputWithLftRgtChildren =
  | (Prisma.Without<
      Omit<Prisma.ContextCreateInput, "lft" | "rgt" | "children">,
      Omit<Prisma.ContextUncheckedCreateInput, "lft" | "rgt" | "children">
    > &
      Omit<Prisma.ContextUncheckedCreateInput, "lft" | "rgt" | "children">)
  | (Prisma.Without<
      Omit<Prisma.ContextUncheckedCreateInput, "lft" | "rgt" | "children">,
      Omit<Prisma.ContextCreateInput, "lft" | "rgt" | "children">
    > &
      Omit<Prisma.ContextCreateInput, "lft" | "rgt" | "children">);
type ContextCreateInput = Parameters<PrismaClient["context"]["create"]>[0]["data"];

export default function Context(prismaContext: PrismaClient["context"]) {
  const customCreate = async (args: {
    select?: Prisma.ContextSelect | null;
    include?: Prisma.ContextInclude | null;
    data: ContextCreateInputWithLftRgtChildren;
  }) => {
    // insert without lft and rgt values
    const data: ContextCreateInput = {
      ...args.data,
      lft: 0,
      rgt: 0,
    };
    // make sure parent is included
    const include = {
      ...args.include,
      parent: true,
    };
    const context = await prismaContext.create({ ...args, include, data });
    const getHighestRgt = async () => {
      return (
        (
          await prismaContext.findFirst({
            select: { rgt: true },
            orderBy: { rgt: "desc" },
            where: { parent: { is: null }, rgt: { gt: 0 } },
          })
        )?.rgt ?? 0
      );
    };

    const lft = context.parent?.rgt ?? (await getHighestRgt()) + 1;
    const rgt = lft + 1;

    // make room for new context
    await prismaContext.updateMany({
      data: { rgt: { increment: 2 } },
      where: { rgt: { gte: lft } },
    });
    await prismaContext.updateMany({
      data: { lft: { increment: 2 } },
      where: { lft: { gte: lft } },
    });

    // place context in new space
    return await prismaContext.update({
      // pass through original select/include to this update
      ...args,
      data: { lft, rgt },
      where: { id: context.id },
    });
  };
  return Object.assign(prismaContext, { insert: customCreate });
}
