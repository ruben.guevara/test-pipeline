import { PrismaClient } from "@prisma/client";
import customContextModel from "./models/Context";

// PrismaClient is attached to the `global` object in development to prevent
// exhausting your database connection limit.
// Learn more: https://pris.ly/d/help/next-js-best-practices

function createPrisma(): PrismaClient {
  const prisma = new PrismaClient({
    log: [{ emit: "event", level: "query" }],
  });
  prisma.$on("query", (e) => {
    console.log("Query: " + e.query);
    console.log("Params: " + e.params);
    console.log("Duration: " + e.duration + "ms");
  });
  return prisma;
}

let prisma: ReturnType<typeof createPrisma>;

// use global prisma client in dev to prevent maxing out connection limit
if (process.env.NODE_ENV === "production") {
  prisma = createPrisma();
} else {
  if (!(global as any).prisma) {
    (global as any).prisma = createPrisma();
  }
  prisma = (global as any).prisma;
}

export { prisma };
