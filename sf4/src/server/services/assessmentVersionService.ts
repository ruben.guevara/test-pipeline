import { prisma } from "$server/db/prisma";
import { User } from "@prisma/client";
import { TRPCError } from "@trpc/server";

export class AssessmentVersionService {

  readonly MSG_ERROR_NOT_FOUND = 'Version not found';

  public async getVersions (assessmentId: number) {
    const data = await prisma.assessmentVersion.findMany({
      where: {
        assessment_id: assessmentId
      },
      include: {
        user: true
      },
      orderBy: {
        timestamp: 'desc'
      }
    });

    return data.map((version: any) => {
      version.version_data = JSON.parse(version.version_data);
      return version;
    })
  }

  public async restore (versionId: number, user?: User) {
    const version = await prisma.assessmentVersion.findFirst({
      where: {
        id: versionId
      }
    });

    if (!version) {
      throw new TRPCError({
        code: "NOT_FOUND",
        message: this.MSG_ERROR_NOT_FOUND,
      });
    }
    
    await prisma.assessmentVersion.create({
      data: {
        assessment_id: version.assessment_id,
        user_id: user ? user.id : null,
        version_data: version.version_data,
        timestamp: new Date().getTime(),
      }
    });

    return (await this.getVersions(version.assessment_id))[0];
  }
};
