import { TRPCError } from "@trpc/server";
import { prisma } from "$server/db/prisma";
import { User } from "@prisma/client";
import { createAssessmentVersion } from "$lib/version";

export class AssessmentService {

  readonly MSG_ERROR_NOT_FOUND = 'Assessment not found';
  readonly MSG_ERROR_NO_PARENT = 'Assessment has not a parent id';

  /**
   * action:
   * load the quiz and the parent quiz
   * mark the quiz as unlinked from the parent
   * copy the list of questions from the parent quiz to the child questions
   * 
   * output:
   * the unlinked cloned quiz and the list of questions to initialize the quiz builder
   * If the quiz does not exist return a 404
   * If the quiz is already unlinked or does not have a parent then just return the quiz and list of questions
   * 
   * @param input Assessment ID
   * @param user User session
   * @returns 
   */
  public async unlink (input: number, user: User) {

    let assessment = await prisma.assessment.findFirst({
      where: {
        id: input
      }
    });

    if (!assessment) {
      throw new TRPCError({
        code: "NOT_FOUND",
        message: this.MSG_ERROR_NOT_FOUND,
      });
    }

    const parentId = assessment.parent_id;
    const questions = await prisma.assessmentQuestion.findMany({
      where: {
        assessment_id: parentId && assessment.link_active ? parentId : assessment.id
      }
    });

    if (!parentId || !assessment.link_active) {
      return {assessment, questions};
    }

    for (let i = 0; i < questions.length; i++) {
      const assessmentQuestion: any = questions[i];
      delete assessmentQuestion.id;
      assessmentQuestion.assessment_id = assessment.id;

      questions[i] = await prisma.assessmentQuestion.create({
        data: assessmentQuestion
      });
    }

    assessment = await prisma.assessment.update({
      where: {
        id: input
      },
      data: {
        link_active: false
      },
    });

    await createAssessmentVersion(input, user);

    return {
      parent_id: parentId,
      assessment,
      questions
    };
  }

  /**
   * action:
   *  Get the id of the parent and the link_active boolean from the assessment
   *  If there is no parent then return an error
   *  If there is a parent and the link is already active, then do nothing
   *  Set the link_active to true, and delete all records in the sf_learnosity_assessment_questions 
   *    table associated with the linked assessment, and the attached pools from the sf_learnosity_pools table
   * 
   * output:
   *  The output data should be the questions and all metadata from the parent/source assessment
   * 
   * @param input Assessment ID
   * @param user User session
   * @returns 
   */
  public async relink (input: number, user: User) {
    let assessment = await prisma.assessment.findFirst({
      where: {
        id: input
      },
      include: {
        assessment_questions: {
          include: {
            pool: true,
          },
        },
      },
    });

    if (!assessment) {
      throw new TRPCError({
        code: "NOT_FOUND",
        message: this.MSG_ERROR_NOT_FOUND,
      });
    }

    const parentId = assessment.parent_id;
    if (!parentId) {
      throw new TRPCError({
        code: "BAD_REQUEST",
        message: this.MSG_ERROR_NO_PARENT,
      });
    }

    const parent = await prisma.assessment.findUnique({
      where: {
        id: parentId,
      },
      include: {
        assessment_questions: {
          include: {
            pool: true,
          },
        },
      },
    })

    if (assessment.link_active) {
      return {parent, assessment};
    }

    await prisma.assessmentQuestion.deleteMany({
      where: {
        assessment_id: input
      }
    })

    await prisma.assessment.update({
      where: {
        id: input
      },
      data: {
        link_active: true
      },
    });

    assessment.link_active = true;
    
    await createAssessmentVersion(input, user);

    return {
      parent,
      assessment,
    };
  }
};
