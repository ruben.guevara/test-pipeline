interface DictionaryType {
  front: string | {
    name: string,
    format?: string, // will be used when a Front value is returned
    params?: any[]
  },
  back: string | {
    name: string,
    format?: string, // will be used when a Back value is returned
    params?: any[]
  },
}

export class ConfigService {
  public ASSESSMENT_DICTIONARY: DictionaryType[] = [
    {
      front: 'maximumNumberOfAttempts',
      back: 'custom:max_attempts',
    }, {
      front: {
        name: 'timeLimit',
        format: 'integerToTimeString',
      },
      back: {
        name: 'time.max_time',
        format: 'timeStringToInteger'
      },
    }, {
      front: {
        name: 'timeLimitWarning',
        format: 'integerToTimeString',
      },
      back: {
        name: 'time.warning_time',
        format: 'timeStringToInteger'
      },
    }, {
      front: {
        name: 'hideTimeSpent',
        format: 'configChoice',
        params: ['YES_NO', false],
      },
      back: {
        name: 'custom:hideTimeSpent',
        format: 'configChoice',
        params: ['YES_NO', true],
      },
    }, {
      front: {
        name: 'allowCheckingAnswersDuringAttempt',
        format: 'configChoice',
        params: ['YES_NO', false],
      },
      back: {
        name: 'questions_api_init_options.showCorrectAnswers',
        format: 'configChoice',
        params: ['YES_NO', true],
      },
    }, {
      front: {
        name: 'previewTime',
        format: 'integerToTimeString',
      },
      back: {
        name: 'configuration.reading_mode.reading_time',
        format: 'timeStringToInteger'
      },
    }, {
      front: {
        name: 'previewTimeWarning',
        format: 'integerToTimeString',
      },
      back: {
        name: 'configuration.reading_mode.warning_time',
        format: 'timeStringToInteger'
      },
    }, {
      front: 'percentageCompleteRequiredToSubmit',
      back: 'configuration.submit_criteria.threshold',
    }, {
      front: 'password',
      back: 'custom:password',
    }, {
      front: {
        name: 'instructions',
        format: 'decodeHTML',
      },
      back: {
        name: 'custom:instructions',
        format: 'stripScriptsAndEventHandlers',
      },
    }, {
      front: {
        name: 'layout',
        format: 'configChoice',
        params: ['LAYOUT', false],
      },
      back: {
        name: 'custom:layout',
        format: 'configChoice',
        params: ['LAYOUT', true],
      },
    }, {
      front: {
        name: 'lockReviews',
        format: 'configChoice',
        params: ['YES_NO', false],
      },
      back: {
        name: 'custom:review_lockout',
        format: 'configChoice',
        params: ['YES_NO', true],
      },
    }, {
      front: {
        name: 'gradeAggregation',
        format: 'configChoice',
        params: ['GRADE_AGGREGATION', false],
      },
      back: {
        name: 'custom:grade_aggregation',
        format: 'configChoice',
        params: ['GRADE_AGGREGATION', true],
      },
    }, {
      front: {
        name: 'showSolutions',
        format: 'configChoice',
        params: ['YES_NO', false],
      },
      back: {
        name: 'custom:show_solutions',
        format: 'configChoice',
        params: ['YES_NO', true],
      },
    }, {
      front: {
        name: 'showGradePercentages',
        format: 'configChoice',
        params: ['YES_NO', false],
      },
      back: {
        name: 'custom:show_grade_percentages',
        format: 'configChoice',
        params: ['YES_NO', true],
      },
    }, {
      front: {
        name: 'showScores',
        format: 'configChoice',
        params: ['YES_NO', false],
      },
      back: {
        name: 'custom:show_scores',
        format: 'configChoice',
        params: ['YES_NO', true],
      },
    },
  ];

  protected static CONFIGURATION_CHOICES = {
    'LAYOUT': [
      'Standard',
      'Vertical',
    ],
    'GRADE_AGGREGATION': [
      'Highest Grade',
      'Latest Grade',
      'Average Grade'
    ],
    'YES_NO': [
      'No',
      'Yes',
    ],
  };
  

  /**
   * Will transform/translate a config also will replace the values with the correct format
   * 
   * @param from        could be "front" o "back". Is the original config
   * @param dictionary  list of key-translate to use
   * @param config      original config to translate
   * @returns 
   */
  public transformConfig (
    from: keyof DictionaryType,
    dictionary: DictionaryType[],
    config: Record<string, any>,
    allowUndefined: boolean = false,
  ) {
    const configTransformed = {};
    const to = from == 'front' ? 'back' : 'front';

    for (const dict of dictionary) {
      let fromKey;
      let toKey;
      let toFormat;
      let toParams = [];
      if (typeof dict[from] == 'string') {
        fromKey = dict[from];
      } else {
        fromKey = (dict[from] as Record<string, any>).name;
      }

      if (typeof dict[to] == 'string') {
        toKey = dict[to];
      } else {
        toKey = (dict[to] as Record<string, any>).name;

        if ((dict[to] as Record<string, any>).hasOwnProperty('format')) {
          toFormat = (this as any)[(dict[to] as Record<string, any>).format];
        }
        if ((dict[to] as Record<string, any>).hasOwnProperty('params')) {
          toParams = (dict[to] as Record<string, any>).params;
        }
      }

      const value = this.getPropertyObject(fromKey, config, toFormat, ...toParams);      
      if (value !== undefined || allowUndefined) {
        this.setPropertyObject(toKey, configTransformed, value);
      }
    }

    return configTransformed;
  }

  /**
   * Create/update a property in object base on key param
   * @param key     name of key, if it is inside a object use "." (period). Ex: "newProperty", "newProperty.childProperty"
   * @param object  object where property will be created/updated
   * @param value   value to assign
   */
  protected setPropertyObject (key: string, object: Record<string, any>, value?: any) {
    const keys = key.split('.');
    const currentKey = keys[0];

    if (!object.hasOwnProperty(currentKey)) {
      object[currentKey] = {};
    }

    if (keys.length == 1) {
      object[currentKey] = value;
    } else {
      keys.shift();
      this.setPropertyObject(keys.join('.'), object[currentKey], value);
    }
  }

  /**
   * Get a value from and object base on key and also will use a format to return value
   * @param key     name of key, if it is inside a object use "." (period). Ex: "newProperty", "newProperty.childProperty"
   * @param object  object where property will be read
   * @param format  Function to use to format value
   * @returns 
   */
  protected getPropertyObject (key: string, object: Record<string, any>, format?: Function, ...params: any) : any {
    const keys = key.split('.');
    const currentKey = keys[0];

    if (!object.hasOwnProperty(currentKey)) {
      return undefined;
    }

    if (keys.length == 1) {      
      if (format && (typeof format == "function")) {
        return format(object[currentKey], ...params);
      }

      return object[currentKey];
    } else {
      keys.shift();
      return this.getPropertyObject(keys.join('.'), object[currentKey], format, ...params);
    }
  }

  protected integerToTimeString(num: number | null) {
    return num !== null ?
      ('0' + Math.floor(num / 3600)).slice(-2) + ':' +
      ('0' + Math.floor((num % 3600) / 60)).slice(-2) :
      null;
  }

  protected timeStringToInteger(timeStr: string | null) {
    if (timeStr == null) {
      return null;
    } else if(!isNaN(parseFloat(timeStr)) && isFinite(Number(timeStr))) {
      return parseInt(timeStr);
    } else if (/^(\d+):(\d{2}):(\d{2})$/.test(timeStr)) {
      var matches: any = timeStr.match(/^(\d+):(\d{2}):(\d{2})$/);
      return parseInt(matches[1]) * 3600 + parseInt(matches[2]) * 60 + parseInt(matches[3]);
    } else if (/^(\d+):(\d{2})$/.test(timeStr)) {
      var matches: any = timeStr.match(/^(\d+):(\d{2})$/);
      return parseInt(matches[1]) * 3600 + parseInt(matches[2]) * 60;
    } else {
      return timeStr !== null ? 0 : null;
    }
  }

  protected configChoice(value: string | number | boolean | null, type: keyof typeof ConfigService.CONFIGURATION_CHOICES, returnKey: boolean = false) {
    if (value == null) {
      return null;
    }

    if (!ConfigService.CONFIGURATION_CHOICES.hasOwnProperty(type)) {
      return null;
    }
    const choices: string[] = ConfigService.CONFIGURATION_CHOICES[type];
    return returnKey ? 
      choices.findIndex((choice) => String(choice).toLowerCase() == String(value).toLowerCase()) :
      choices[Number(value)];
  }

  protected encodeHTML(str: string | null) {
    if (!str) {
      return str;
    }

    return str.replace(/([\u00A0-\u9999<>&])(.|$)/g, function(full, char, next) {
      if(char !== '&' || next !== '#'){
        if(/[\u00A0-\u9999<>&]/.test(next))
          next = '&#' + next.charCodeAt(0) + ';';

        return '&#' + char.charCodeAt(0) + ';' + next;
      }

      return full;
    });
  }

  protected decodeHTML(str: string | null) {
    if (!str) {
      return str;
    }

    return str.replace(/&#([0-9]+);/g, function(full, int) {
        return String.fromCharCode(parseInt(int));
    });
  }

  protected stripScriptsAndEventHandlers(value: string | null) {
    if(value) {
      let decoded = this.decodeHTML(value);
  
      decoded = (decoded as string).replace(
        /(?<!<[^>]+)on\w+\s?=\s?("|')(?:[^"\\]|\\.)*\1/ig,
        ''
      ).replace(
        /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*(?:<\/script>|\/>)/ig,
        ''
      );
  
      value = this.encodeHTML(decoded);
    }
  
    return value;
  }

}