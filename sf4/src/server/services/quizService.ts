import { Learnosity } from "$lib/learnosity";
import { createAssessmentVersion } from "$lib/version";
import { prisma } from "$server/db/prisma";
import { AssessmentQuestion, User } from "@prisma/client";
import { z } from "zod";

export const QuizType = z.object({
  assessment_id: z.number(),
  items: z.array(
    z.object({
      id: z.number().optional(),
      item_reference: z.string(),
      sort_order: z.number(),
      weight: z.number(),
    }).or(
      z.object({
        pool_id: z.number().optional(),
        sort_order: z.number(),
        weight: z.number(),
        num_to_select: z.number().positive().int(),
        questions: z.object({
          id: z.number().optional(),
          item_reference: z.string(),
        })
        .array()
      })
      .refine((data) => data.num_to_select <= data.questions.length, {
        message: "num_to_select number shall not be higher than the number of questions",
        path: ["num_to_select"],
      })
    )
  )
});

type QuizItemsType = z.infer<typeof QuizType.shape.items>;

export class QuizService {

  public async maxScore(references: string[]) {
    const learnosity = new Learnosity();
    const response: {[key: string]: number} = {};
    await Promise.all(references.map(async (reference) => {
      try {        
        const responseQuestions: any = await learnosity.dataApiRequest('itembank/questions', {
          "item_references": [reference]
        });
  
        if (!responseQuestions || !responseQuestions.meta || !responseQuestions.meta.status) {
          response[reference] = 0;
          return;
        }
        
        const maxScore = responseQuestions.data[reference].reduce((accumulator: number, object: any) => {
          return accumulator + (object.data?.validation?.max_score ?? 0);
        }, 0);
  
        response[reference] = maxScore;
      } catch (error) {
        console.error(error);
        response[reference] = 0;
      }  
    }));

    return response;
  }

  public async save(assessmentId: number, items: QuizItemsType, user: User) {
    const questions: AssessmentQuestion[] = [];
    const itemQuestions: Partial<AssessmentQuestion>[] = [];

    for (let i = 0; i < items.length; i++) {
      const item = items[i];

      if ('questions' in item) {
        const data = {
          id: item.pool_id,
          num_to_select: item.num_to_select,
          weight: item.weight
        }

        const pool = await prisma.pool.upsert({
          create: {...data, created_at: new Date() },
          update: {...data, updated_at: new Date() },
          where: { id: data.id ?? 0 },
        });

        for (let i = 0; i < item.questions.length; i++) {
          const question: Partial<AssessmentQuestion> = item.questions[i];
          question.pool_id = pool.id;
          question.sort_order = item.sort_order;
          question.weight = item.weight;
          itemQuestions.push(question);
        }
      } else {
        itemQuestions.push(item);
      }
    }

    const ids = itemQuestions.flatMap((item) => (!item.id ? [] : item.id));

    // removing old questions
    await prisma.assessmentQuestion.deleteMany({
      where: {
        assessment_id: assessmentId,
        id: {
          notIn: ids
        }
      }
    });

    await Promise.all(itemQuestions.map(async item => {
      const question: any = item;
      question.assessment_id = assessmentId;
      
      const upsert = await prisma.assessmentQuestion.upsert({
        create: question,
        update: question,
        where: { id: question.id ?? 0 },
      });

      questions.push(upsert);
    }));

    await createAssessmentVersion(assessmentId, user);
    return questions;
  }

}