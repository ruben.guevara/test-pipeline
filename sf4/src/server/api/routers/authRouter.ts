import {
  checkpoint,
  checkpointAnd,
  checkpointOr,
  checkpoints,
  isPermissionName,
} from "$server/auth/accessControl";
import { z } from "zod";
import { authedProcedure, createTRPCRouter } from "../trpc";

const checkpointInput = z.object({
  permission: z.string().refine(isPermissionName),
  context: z.number().optional(),
});

const multiCheckpointInput = z.object({
  permissions: z.array(z.string().refine(isPermissionName)).min(1),
  context: z.number().optional(),
});

export const authRouter = createTRPCRouter({
  checkpoint: authedProcedure
    .input(checkpointInput)
    .query(({ ctx, input: { permission, context } }) => {
      return checkpoint({
        user: ctx.user,
        permission,
        context: context ? { id: context } : undefined,
      });
    }),
  checkpointOr: authedProcedure
    .input(multiCheckpointInput)
    .query(({ ctx, input: { permissions, context } }) => {
      return checkpointOr({
        user: ctx.user,
        permissions,
        context: context ? { id: context } : undefined,
      });
    }),
  checkpointAnd: authedProcedure
    .input(multiCheckpointInput)
    .query(({ ctx, input: { permissions, context } }) => {
      return checkpointAnd({
        user: ctx.user,
        permissions,
        context: context ? { id: context } : undefined,
      });
    }),
  checkpoints: authedProcedure
    .input(multiCheckpointInput)
    .query(({ ctx, input: { permissions, context } }) => {
      return checkpoints({
        user: ctx.user,
        permissions,
        context: context ? { id: context } : undefined,
      });
    }),
});
