import { Learnosity } from "$lib/learnosity";
import { validateUserPermissions } from "$server/auth/auth";
import { TRPCError } from "@trpc/server";
import { z } from "zod";
import { itemSchema, questionSchema } from "../schemas";
import { authedProcedure, createTRPCRouter } from "../trpc";

export const questionRouter = createTRPCRouter({
  findAll: authedProcedure.input(z.string()).query(async ({ ctx, input }) => {
    console.log(
      ctx.user.firstname +
        " " +
        ctx.user.lastname +
        " is looking for question " +
        input
    );
    const [, error] = await validateUserPermissions(ctx.user, [
      "assessment_edit",
    ]);
    if (error) {
      throw new TRPCError({
        code: "FORBIDDEN",
        message: "Cannot search any question",
      });
    }

    const learnosity = new Learnosity();
    const response = await learnosity.dataApiRequest("itembank/items", {
      references: [input],
      limit: 50,
    });
    const { data, meta } = itemSchema.parse(response);

    if (!meta.records) {
      return { meta, data: [] };
    }

    const items = await Promise.all(
      data.map(async (item) => {
        const references = item.questions.map((question) => question.reference);

        const res = await learnosity.dataApiRequest("itembank/questions", {
          references: references,
          limit: 50,
        });

        const parsed = questionSchema.parse(res);
        return { ...item, questions: parsed };
      })
    );

    return { meta, data: items };
  }),
  findSelected: authedProcedure
    .input(z.array(z.string()))
    .query(async ({ ctx, input }) => {
      const [, error] = await validateUserPermissions(ctx.user, [
        "assessment_edit",
      ]);
      if (error) {
        throw new TRPCError({
          code: "FORBIDDEN",
          message: "Cannot search any question",
        });
      }
      const learnosity = new Learnosity();
      const response = await learnosity.dataApiRequest("itembank/items", {
        references: input,
        limit: 100,
      });
      const { data } = itemSchema.parse(response);

      if (!data.length) {
        return [];
      }

      const items = await Promise.all(
        data.map(async (item) => {
          const references = item.questions.map(
            (question) => question.reference
          );

          const res = await learnosity.dataApiRequest("itembank/questions", {
            references: references,
            limit: 50,
          });

          const parsed = questionSchema.parse(res);
          return { ...item, questions: parsed };
        })
      );

      return items;
    }),
  findLatest: authedProcedure.query(async ({ ctx }) => {
    const author = await ctx.prisma.author.findFirst({
      where: {
        user_id: ctx.user.id,
      },
    });

    if (!author) {
      return [];
    }

    const learnosity = new Learnosity();
    const response = await learnosity.dataApiRequest("itembank/items", {
      sort_field: "updated",
      tags: [
        {
          type: "Author",
          name: `${author.id}`,
        },
      ],
      limit: 5,
    });    

    const { data } = itemSchema.parse(response);

    if (!data.length) {
      return [];
    }

    const items = await Promise.all(
      data.map(async (item) => {
        const references = item.definition.widgets.map(
          (question) => question.reference
        );

        const res = await learnosity.dataApiRequest("itembank/questions", {
          references: references,
        });

        const parsed = questionSchema.parse(res);
        return { ...item, questions: parsed };
      })
    );

    return items;
  }),
});
