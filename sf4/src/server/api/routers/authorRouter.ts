import { authedProcedure, createTRPCRouter } from "../trpc";

export const authorRouter = createTRPCRouter({
  findAll: authedProcedure.query(({ ctx }) => {
    return ctx.prisma.author.findMany({
      select: {
        id: true,
        name: true,
      },
    });
  }),
});
