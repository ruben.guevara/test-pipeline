import { TRPCError } from "@trpc/server";
import { z } from "zod";
import { createTRPCRouter, publicProcedure } from "../trpc";

export const countryRouter = createTRPCRouter({
  findById: publicProcedure.input(z.number()).query(({ ctx, input }) => {
    const country = ctx.prisma.country.findUnique({
      where: {
        id: input,
      },
    });
    if (!country) {
      throw new TRPCError({
        code: "NOT_FOUND",
        message: "Country not found",
      });
    }
    return country;
  }),
  findAll: publicProcedure.query(({ ctx }) => {
    return ctx.prisma.country.findMany();
  }),
});
