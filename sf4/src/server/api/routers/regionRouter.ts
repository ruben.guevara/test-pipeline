import { TRPCError } from "@trpc/server";
import { z } from "zod";
import { authedProcedure, createTRPCRouter } from "../trpc";

export const regionRouter = createTRPCRouter({
  find: authedProcedure.input(z.number()).query(({ ctx, input }) => {
      console.log(
          ctx.user.firstname +
          " " +
          ctx.user.lastname +
          " is looking for region " +
          input
      );
      const region = ctx.prisma.region.findUnique({
          where: {
              id: input,
          },
      });
      if (!region) {
          throw new TRPCError({
              code: "NOT_FOUND",
              message: "Region not found",
          });
    }
    return region;
  }),

  list: authedProcedure.query(({ ctx }) => {
    return ctx.prisma.region.findMany({
      select: {
        id: true,
        name: true,
        date_last_updated: true,
        country: { select: { name: true } },
        _count: {
          select: {
            authors: true,
            courses: true,
            institutions: true,
            partners: true,
          },
        },
      },
    });
  }),

  update: authedProcedure
    .input(
      z.object({
        id: z.number().nullable(),
        name: z.string(),
        country_id: z.number().nullable(),
      })
    )
    .mutation(async ({ ctx, input }) => {
      const { id, name, country_id } = input;
      const data = {
        name,
        country_id,
        date_last_updated: new Date(),
      };
      if (id) {
        return await ctx.prisma.region.update({
          where: { id },
          data,
        });
      } else {
        return await ctx.prisma.region.create({
          data: {
            date_created: new Date(),
            ...data,
          },
        });
      }
    }),

  delete: authedProcedure.input(z.number()).mutation(async ({ ctx, input }) => {
    const region = await ctx.prisma.region.findUnique({
      where: { id: input },
      select: {
        id: true,
        _count: {
          select: {
            authors: true,
            courses: true,
            institutions: true,
            partners: true,
          },
        },
      },
    });
    if (!region) {
      throw new TRPCError({
        code: "NOT_FOUND",
        message: "Region not found",
      });
    }
    const keys = Object.keys(region._count) as (keyof typeof region._count)[];
    keys.forEach((key) => {
      if (region._count[key]) {
        throw new TRPCError({
          code: "FORBIDDEN",
          message: `Cannot delete region with ${key}`,
        });
      }
    });
    await ctx.prisma.region.delete({
      where: { id: input },
    });
    return true;
  }),
});
