import { AssessmentVersionService } from "$server/services/assessmentVersionService";
import { z } from "zod";
import { authedProcedure, createTRPCRouter } from "../trpc";

export const historyRouter = createTRPCRouter({
  findAll: authedProcedure.input(z.number()).query(async ({ ctx, input }) => {
    const service = new AssessmentVersionService();
    return service.getVersions(input);
  }),
  retore: authedProcedure.input(z.number()).query(async ({ ctx, input }) => {
    const service = new AssessmentVersionService();
    return service.restore(input, ctx.user);
  }),
});
