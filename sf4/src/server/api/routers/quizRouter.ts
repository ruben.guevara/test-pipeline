import { Learnosity } from "$lib/learnosity";
import { checkpoints } from "$server/auth/accessControl";
import { validateUserPermissions } from "$server/auth/auth";
import { QuizService, QuizType } from "$server/services/quizService";
import { TRPCError } from "@trpc/server";
import { randomUUID } from "crypto";
import { z } from "zod";
import { authedProcedure, createTRPCRouter } from "../trpc";

const validatePermissionQuiz = (
  currentUser: any,
  permissions: Record<string, boolean>,
  assessmentUserID: number | null,
  quizId: number | undefined
) => {
  if (!quizId && !permissions["learnosity_item_create"]) {
    throw new TRPCError({
      code: "FORBIDDEN",
      message: "You do not have permission to create a new quiz",
    });
  } else if (quizId && permissions["learnosity_item_edit_all"]) {
    return true;
  } else if (quizId && assessmentUserID != currentUser.id) {
    throw new TRPCError({
      code: "FORBIDDEN",
      message: "You do not have permission to update a quiz",
    });
  }
};

export const quizRouter = createTRPCRouter({
  find: authedProcedure.input(z.number()).query(({ ctx, input }) => {
    const quiz = ctx.prisma.assessment.findUnique({
      where: {
        id: input,
      },
      include: {
        assessment_questions: {
          include: {
            pool: true,
          },
        },
      },
    });
    if (!quiz) {
      throw new TRPCError({
        code: "NOT_FOUND",
        message: "Quiz not found",
      });
    }
    return quiz;
  }),
  findCourse: authedProcedure.input(z.number()).query(({ ctx, input }) => {
    const course = ctx.prisma.course.findUnique({
      where: {
        id: input,
      },
    });
    if (!course) {
      throw new TRPCError({
        code: "NOT_FOUND",
        message: "Course not found",
      });
    }
    return course;
  }),
  findChapter: authedProcedure.input(z.number()).query(({ ctx, input }) => {
    const chapter = ctx.prisma.chapter.findUnique({
      where: {
        id: input,
      },
    });
    if (!chapter) {
      throw new TRPCError({
        code: "NOT_FOUND",
        message: "Chapter not found",
      });
    }
    return chapter;
  }),
  maxScore: authedProcedure
    .input(z.string().array())
    .query(({ ctx, input }) => {
      console.log(
        ctx.user.firstname +
          " " +
          ctx.user.lastname +
          " is getting maxScore of: " +
          JSON.stringify(input)
      );
      const service = new QuizService();
      return service.maxScore(input);
    }),
  save: authedProcedure.input(QuizType).mutation(async ({ ctx, input }) => {
    console.log(
      ctx.user.firstname +
        " " +
        ctx.user.lastname +
        " is saving quiz data: " +
        JSON.stringify(input)
    );

    const permissions: any = [
      "learnosity_item_create",
      "learnosity_item_edit",
      "learnosity_item_edit_all",
    ];
    const [, error] = await validateUserPermissions(ctx.user, permissions);

    if (error) {
      throw new TRPCError({
        code: "FORBIDDEN",
        message: "Cannot use this method",
      });
    }

    const allow = await checkpoints({
      user: ctx.user,
      permissions,
    });
    const assessment = await ctx.prisma.assessment.findUnique({
      where: {
        id: input.assessment_id,
      },
    });

    if (!assessment) {
      throw new TRPCError({
        code: "NOT_FOUND",
        message: "Assessment not found",
      });
    }

    for (let i = 0; i < input.items.length; i++) {
      const item = input.items[i];

      if ("questions" in item) {
        for (let i = 0; i < item.questions.length; i++) {
          const question = item.questions[i];
          validatePermissionQuiz(
            ctx.user,
            allow,
            assessment.created_by_id,
            question.id
          );
        }
      } else {
        validatePermissionQuiz(
          ctx.user,
          allow,
          assessment.created_by_id,
          item.id
        );
      }
    }

    const service = new QuizService();
    return service.save(input.assessment_id, input.items, ctx.user);
  }),
  previewInit: authedProcedure
    .input(
      z.object({
        reference: z.string(),
      })
    )
    .query(async ({ input, ctx }) => {
      const learnosity = new Learnosity();
      return learnosity.generateApi(
        {
          rendering_type: "inline",
          retrieve_tags: true,
          type: "local_practice",
          name: "Item Preview",
          user_id: String(ctx.user.id),
          items: [input.reference],
          session_id: randomUUID(),
          config: {
            configuration: {
              idle_timeout: false,
              reading_mode: {
                reading_time: 0,
                warning_time: 0,
              },
              submit_criteria: {
                threshold: 0,
                type: "attempted",
              },
            },
            questions_api_init_options: {
              showCorrectAnswers: false,
              attribute_overrides: {
                instant_feedback: true,
              },
              beta_flags: { reactive_views: true },
              showInstructorStimulus: false,
              show_distractor_rationale: true,
            },
            "custom:show_grade_percentages": 1,
            "custom:show_scores": 1,
          },
        },
        "items"
      );
    }),
  //TODO: Replace with actual procedure from wrike item #1111101452
  authorInit: authedProcedure
    .query(async ({ input, ctx }) => {
      const learnosity = new Learnosity();
      return learnosity.generateApi(
        {
          mode: "item_list",
          organisation_id: 468,
          user: {
              id: "1",
              firstname: "Admin",
              lastname: "User",
              email: "fake1@email.com"
          },
          config: {
              container: {
                  height: "auto",
                  fixed_footer_height: 0,
                  scroll_into_view_selector: ".container"
              },
              dependencies: {
                question_editor_api: {
                  init_options: [],
                  preload: false
                },
                questions_api: {
                  init_options: [],
                    preload: false
                }
              },
              global: {
                  disable_onbeforeunload: true,
                  show_tags: [
                      {
                          type: "Project Submission"
                      },
                      {
                          type: "Author"
                      }
                  ],
                  items: {
                      read_only: {
                          enabled: false,
                          filter: {
                              duplicate: false
                          }
                      }
                  }
              },
              label_bundle: {
                  dateTimeLocale: "",
                  toolTipDateTimeSeparator: "at",
                  toolTipDateFormat: "MMM D, YYYY",
                  toolTipTimeFormat: "hh:mm a",
                  editorTogglePreview: "Quick Preview"
              },
              widget_templates: {
                  back: false,
                  save: false,
                  widget_types: {
                      default: "questions",
                      show: true
                  }
              },
              item_list: {
                  filter: {
                      restricted: {
                          current_user: false,
                          status: [
                              "published",
                              "unpublished"
                          ],
                          tags: {
                              all: [
                                  {
                                      type: "Item Type",
                                      name: "Student Question"
                                  }
                              ],
                              none: [
                                  {
                                      type: "Item Type",
                                      name: "Teacher Feedback"
                                  }
                              ]
                          }
                      }
                  },
                  item: {
                      enable_selection: false,
                      status: false,
                      url: "",
                      title: {
                          show: true,
                          show_reference: true
                      }
                  },
                  limit: 50,
                  toolbar: {
                      add: false,
                      search: {
                          show: true,
                          controls: [
                              "title",
                              "reference",
                              "content"
                          ],
                          widget_type: true,
                          status: true,
                          tags: {
                              show: true
                          }
                      }
                  }
              }
          }
        },
        'author'
      );
    })
});
