import { Learnosity } from "$lib/learnosity";
import { z } from "zod";
import { authedProcedure, createTRPCRouter } from "../trpc";

export const learnosityRouter = createTRPCRouter({
  initQuizBuilderSearch: authedProcedure.query(({ ctx }) => {
    const learnosity = new Learnosity();

    // TODO: front end should fill this in when they are working on the search
    const request: any = {};

    return learnosity.generateApi(request, 'author');
  }),
});
