import { createAssessmentVersion } from "$lib/version";
import { prisma } from "$server/db/prisma";
import { AssessmentService } from "$server/services/assessmentService";
import { AssessmentVersionService } from "$server/services/assessmentVersionService";
import { ConfigService } from "$server/services/configService";
import { TRPCError } from "@trpc/server";
import { z } from "zod";
import { authedProcedure, createTRPCRouter } from "../trpc";

export const assessmentRouter = createTRPCRouter({
  unlink: authedProcedure.input(z.number()).mutation(async ({ ctx, input }) => {
    console.log(
      ctx.user.firstname + " " + ctx.user.lastname + " is unlink id: " + input
    );

    const service = new AssessmentService();
    return service.unlink(input, ctx.user);
  }),
  relink: authedProcedure.input(z.number()).mutation(async ({ ctx, input }) => {
    console.log(
      ctx.user.firstname +
        " " +
        ctx.user.lastname +
        " is relinking id: " +
        input
    );

    const service = new AssessmentService();
    return service.relink(input, ctx.user);
  }),
  fetchConfig: authedProcedure.input(z.number()).query(async ({ input }) => {
    const config = await prisma.assessmentConfiguration.findFirst({
      where: {
        assessment_id: input,
        type: "assessment",
      },
    });

    if (!config || !config.config) {
      throw new TRPCError({
        code: "NOT_FOUND",
        message: "Assessment configuration not found",
      });
    }

    const service = new ConfigService();
    return service.transformConfig(
      "back",
      service.ASSESSMENT_DICTIONARY,
      JSON.parse(config.config) as Record<string, any>
    );
  }),
  saveConfig: authedProcedure
    .input(
      z.object({
        assessment_id: z.number(),
        config: z.any(),
      })
    )
    .mutation(async ({ ctx, input }) => {
      const service = new ConfigService();
      const config = service.transformConfig(
        "front",
        service.ASSESSMENT_DICTIONARY,
        input.config
      );

      const configuration = {
        assessment_id: input.assessment_id,
        type: "assessment",
        config: JSON.stringify(config),
      };

      const exists = await prisma.assessmentConfiguration.findFirst({
        where: {
          assessment_id: input.assessment_id,
          type: "assessment",
        },
      });

      if (exists) {
        await prisma.assessmentConfiguration.update({
          data: configuration,
          where: {
            id: exists.id,
          },
        });
      } else {
        await prisma.assessmentConfiguration.create({
          data: configuration,
        });
      }

      await createAssessmentVersion(input.assessment_id, ctx.user);

      return input.config;
    }),
  versions: authedProcedure.input(z.number()).mutation(async ({ ctx, input }) => {
    console.log(
      ctx.user.firstname + " " + ctx.user.lastname + " is getting assessment id version: " + input
    );

    const service = new AssessmentVersionService();
    return service.getVersions(input);
  }),
});
