import { createInnerTRPCContext, createTRPCRouter } from "$server/api/trpc";
import { createProxySSGHelpers } from "@trpc/react-query/ssg";
import superjson from "superjson";
import { assessmentRouter } from "./routers/assessmentRouter";
import { authRouter } from "./routers/authRouter";
import { authorRouter } from "./routers/authorRouter";
import { countryRouter } from "./routers/countryRouter";
import { historyRouter } from "./routers/historyRouter";
import { learnosityRouter } from "./routers/learnosityRouter";
import { questionRouter } from "./routers/questionRouter";
import { quizRouter } from "./routers/quizRouter";
import { regionRouter } from "./routers/regionRouter";

/**
 * This is the primary router for your server.
 *
 * All routers added in /api/routers should be manually added here
 */
export const appRouter = createTRPCRouter({
  auth: authRouter,
  author: authorRouter,
  assessment: assessmentRouter,
  country: countryRouter,
  history: historyRouter,
  learnosity: learnosityRouter,
  question: questionRouter,
  quiz: quizRouter,
  region: regionRouter,
});

export async function createServerSideAppRouter(
  contextOptions: Parameters<typeof createInnerTRPCContext>[0]
) {
  return createProxySSGHelpers({
    router: appRouter,
    ctx: await createInnerTRPCContext(contextOptions),
    transformer: superjson,
  });
}

// export type definition of API
export type AppRouter = typeof appRouter;
