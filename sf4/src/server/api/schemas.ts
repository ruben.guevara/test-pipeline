import { z } from "zod";

export const QUESTION_TYPES = [
  "imageclozetext",
  "audio",
  "chemistryessayV2",
  "chemistry",
  "choicematrix",
  "classification",
  "clozeassociation",
  "clozechemistry",
  "clozedropdown",
  "imageclozechemistry",
  "clozeformulaV2",
  "clozeformula",
  "drawing",
  "longtextV2",
  "fileupload",
  "clozetext",
  "graphplotting",
  "gridded",
  "hotspot",
  "imageclozeassociationV2",
  "imageclozeformula",
  "imageclozedropdown",
  "imageupload",
  "association",
  "formulaessayV2",
  "mcq",
  "numberline",
  "numberlineplot",
  "orderlist",
  "plaintext",
  "rating",
  "shorttext",
  "simplechart",
  "simpleshading",
  "sortlist",
  "tokenhighlight",
  "video",
] as const;

const itemSchema = z.object({
  meta: z.object({
    status: z.boolean(),
    timestamp: z.number(),
    records: z.number(),
  }),
  data: z.array(
    z.object({
      reference: z.string(),
      description: z.string().nullable(),
      title: z.string(),
      status: z.string(),
      dt_created: z.string().optional(),
      dt_updated: z.string().optional(),
      definition: z.object({
        widgets: z.array(
          z.object({
            reference: z.string(),
          })
        ),
      }),
      questions: z.array(
        z.object({
          reference: z.string(),
          type: z.string(),
        })
      ),
    })
  ),
});

const questionSchema = z.object({
  meta: z.object({
    records: z.number(),
  }),
  data: z.array(
    z.object({
      type: z.string(),
      reference: z.string(),
      widget_type: z.string(),
      data: z.object({
        type: z.string(),
        stimulus: z.string().optional(),
        options: z
          .array(
            z.object({
              label: z.string(),
              value: z.string().or(z.number()),
            })
          )
          .optional(),
        validation: z.any().optional(),
      }),
    })
  ),
});

const learnosityItem = z.array(
  z.object({
    reference: z.string(),
    description: z.string().nullable(),
    title: z.string(),
    status: z.string(),
    dt_created: z.string().optional(),
    dt_updated: z.string().optional(),
    questions: z.object({
      meta: z.object({
        records: z.number(),
      }),
      data: z.array(
        z.object({
          type: z.string(),
          reference: z.string(),
          widget_type: z.string(),
          data: z.object({
            type: z.string(),
            stimulus: z.string().optional(),
            options: z
              .array(
                z.object({
                  label: z.string(),
                  value: z.string().or(z.number()),
                })
              )
              .optional(),
            validation: z
              .object({
                max_score: z.number().optional(),
                valid_response: z
                  .object({
                    score: z.number(),
                    value: z.array(z.string().or(z.number())).or(z.string()),
                  })
                  .optional(),
              })
              .optional(),
          }),
        })
      ),
    }),
  })
);

export { itemSchema, learnosityItem, questionSchema };
