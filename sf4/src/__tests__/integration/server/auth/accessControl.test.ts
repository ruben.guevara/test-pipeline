import { checkpoints } from "$server/auth/accessControl";
import { prisma } from "$server/db/prisma";
import { contextFixtureTree } from "$tests/fixtures/contexts";
import { permissionFixtures } from "$tests/fixtures/permissions";
import { userFixture } from "$tests/fixtures/users";
import { clearDatabase } from "$tests/support/prismaHelpers";
import { Context, User } from "@prisma/client";
import { beforeAll, beforeEach, describe, expect, it } from "vitest";

describe("checkpoint", () => {
  let user: User;
  let contexts: Resolved<ReturnType<typeof contextFixtureTree>>;

  beforeAll(async () => {
    await clearDatabase();
    user = await userFixture();
    contexts = await contextFixtureTree();
    await permissionFixtures();
  });

  beforeEach(async () => {
    await prisma.roleAssignment.deleteMany();
  });

  it("should fail with no user", async () => {
    const result = await checkpoints({
      user: null,
      permissions: ["region_create"],
    });

    expect(result.region_create).toBe(false);
  });

  it("should pass with root admin and no context", async () => {
    await addRoleAssignment(user.id, "admin", contexts.root.id);

    const result = await checkpoints({
      user,
      permissions: ["class_student_view_as", "class_teacher_view_as"],
    });

    expect(result).toEqual({
      class_student_view_as: true,
      class_teacher_view_as: true,
    });
  });

  it("should fail with student and no context", async () => {
    await addRoleAssignment(user.id, "student", contexts.grandchildren[0].id);

    const result = await checkpoints({
      user,
      permissions: ["class_student_view_as", "class_teacher_view_as"],
    });

    expect(result).toEqual({
      class_student_view_as: false,
      class_teacher_view_as: false,
    });
  });

  it("should pass with teacher from current context", async () => {
    await addRoleAssignment(user.id, "teacher", contexts.children[0].id);

    const result = await checkpoints({
      user,
      permissions: ["class_student_view_as"],
      context: contexts.children[0],
    });

    expect(result.class_student_view_as).toBe(true);
  });

  it("should pass with teacher from parent context", async () => {
    await addRoleAssignment(user.id, "teacher", contexts.root.id);

    const result = await checkpoints({
      user,
      permissions: ["class_student_view_as"],
      context: contexts.children[0],
    });

    expect(result.class_student_view_as).toBe(true);
  });

  it("should fail with teacher from child context", async () => {
    await addRoleAssignment(user.id, "teacher", contexts.grandchildren[0].id);

    const result = await checkpoints({
      user,
      permissions: ["class_student_view_as"],
      context: contexts.children[0],
    });

    expect(result.class_student_view_as).toBe(false);
  });

  it("should fail with teacher from sibling context", async () => {
    await addRoleAssignment(user.id, "teacher", contexts.children[1].id);

    const result = await checkpoints({
      user,
      permissions: ["class_student_view_as"],
      context: contexts.children[0],
    });

    expect(result.class_student_view_as).toBe(false);
  });

  it("should fail without role assignment", async () => {
    const result = await checkpoints({
      user,
      permissions: ["class_student_view_as"],
      context: contexts.grandchildren[0],
    });

    expect(result.class_student_view_as).toBe(false);
  });

  it("should pass without denied roles", async () => {
    await addRoleAssignment(user.id, "student", contexts.grandchildren[0].id);

    const result = await checkpoints({
      user,
      permissions: ["group_join"],
      context: contexts.grandchildren[0],
    });

    expect(result.group_join).toBe(true);
  });

  it("should fail with denied roles", async () => {
    await addRoleAssignment(user.id, "teacher", contexts.grandchildren[0].id);
    await addRoleAssignment(user.id, "student", contexts.grandchildren[0].id);

    const result = await checkpoints({
      user,
      permissions: ["group_join"],
      context: contexts.grandchildren[0],
    });

    expect(result.group_join).toBe(false);
  });

  it("should fail with denied roles in another context", async () => {
    await addRoleAssignment(user.id, "teacher", contexts.grandchildren[0].id);
    await addRoleAssignment(user.id, "student", contexts.grandchildren[3].id);

    const result = await checkpoints({
      user,
      permissions: ["group_join"],
      context: contexts.grandchildren[0],
    });

    expect(result.group_join).toBe(false);
  });

  it("should pass without denied roles and no context", async () => {
    await addRoleAssignment(user.id, "student", contexts.grandchildren[0].id);

    const result = await checkpoints({
      user,
      permissions: ["group_join"],
    });

    expect(result.group_join).toBe(true);
  });

  it("should fail with denied roles and no context", async () => {
    await addRoleAssignment(user.id, "teacher", contexts.grandchildren[0].id);
    await addRoleAssignment(user.id, "student", contexts.grandchildren[0].id);

    const result = await checkpoints({
      user,
      permissions: ["group_join"],
    });

    expect(result.group_join).toBe(false);
  });
});

async function addRoleAssignment(userId: number, roleName: string, contextId: number) {
  return await prisma.roleAssignment.create({
    data: {
      user: { connect: { id: userId } },
      role: { connect: { name: roleName } },
      context: { connect: { id: contextId } },
    },
  });
}
