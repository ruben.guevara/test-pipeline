import Context from "$server/db/models/Context";
import { prisma } from "$server/db/prisma";
import { contextFixtureTree as contextTreeFixture, fakeContext } from "$tests/fixtures/contexts";
import { clearDatabase } from "$tests/support/prismaHelpers";
import { faker } from "@faker-js/faker";
import { beforeEach, describe, expect, it } from "vitest";

describe("custom context create", () => {
  beforeEach(async () => {
    await clearDatabase();
  });
  it("should auto assign the lft and rgt", async () => {
    const context = await Context(prisma.context).insert({
      data: fakeContext(),
    });

    expect(context.lft).toBe(1);
    expect(context.rgt).toBe(2);

    const context2 = await Context(prisma.context).insert({
      data: fakeContext(),
    });

    expect(context2.lft).toBe(3);
    expect(context2.rgt).toBe(4);
  });
  it("should auto update the whole tree when parent is specified", async () => {
    const tree = await contextTreeFixture();

    await Context(prisma.context).insert({
      data: {
        name: faker.lorem.word(),
        date_created: new Date(),
        date_last_updated: new Date(),
        parent: { connect: { id: tree.children[0].id } },
      },
    });

    const allContexts = await prisma.context.findMany({
      select: { id: true, lft: true, rgt: true, parent_id: true },
      orderBy: { lft: "asc" },
    });

    expect(allContexts).toEqual([
      { id: 1, lft: 1, parent_id: null, rgt: 16 },
      { id: 2, lft: 2, parent_id: 1, rgt: 9 },
      { id: 4, lft: 3, parent_id: 2, rgt: 4 },
      { id: 5, lft: 5, parent_id: 2, rgt: 6 },
      { id: 8, lft: 7, parent_id: 2, rgt: 8 },
      { id: 3, lft: 10, parent_id: 1, rgt: 15 },
      { id: 6, lft: 11, parent_id: 3, rgt: 12 },
      { id: 7, lft: 13, parent_id: 3, rgt: 14 },
    ]);
  });
});
