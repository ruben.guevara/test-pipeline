import { beforeAll, describe, expect, test } from "vitest";
import { clearDatabase } from "../../support/prismaHelpers";
import { Assessment, AssessmentQuestion, User } from "@prisma/client";
import { AssessmentService } from "$server/services/assessmentService";
import { TRPCError } from "@trpc/server";
import { assessmentFixture, fakeAssessment } from "$tests/fixtures/asessments";
import { assessmentQuestionFixture } from "$tests/fixtures/asessmentQuestions";
import { prisma } from "$server/db/prisma";
import { userFixture } from "$tests/fixtures/users";
import { AssessmentVersionService } from "$server/services/assessmentVersionService";

describe("Assessment Service", () => {
  let user: User;
  let assessment: Assessment;
  let assessmentChild: Assessment;
  let question: AssessmentQuestion;
  let assessmentService: AssessmentService;
  let assessmentVersionService: AssessmentVersionService;

  beforeAll(async () => {
    await clearDatabase();
    assessmentService = new AssessmentService();
    assessmentVersionService = new AssessmentVersionService();
    user = await userFixture();
  });

  describe("unlik", async () => {

    test("should return 404 eror", async () => {
      await expect(assessmentService.unlink(123))
        .rejects
        .toThrowError(new TRPCError({
          code: "NOT_FOUND",
          message: assessmentService.MSG_ERROR_NOT_FOUND,
        }));
    });

    test("should return assessment without parent", async () => {
      assessment = await assessmentFixture();

      const response = await assessmentService.unlink(assessment.id, user);
      expect(response.assessment.id).toEqual(assessment.id);
      expect(response.questions).toHaveLength(0);
      expect(response.parent_id).toBeUndefined();
    });
  
    test("should return assessment, parent and question (with version)", async () => {
      
      const versions = (await assessmentVersionService.getVersions(2)).length;

      assessmentChild = await assessmentFixture({ id: 2, parent_id: assessment.id, link_active: true });
      question = await assessmentQuestionFixture({ assessment_id: assessment.id })

      const response = await assessmentService.unlink(assessmentChild.id, user);
      expect(response.assessment.id).toEqual(assessmentChild.id);
      expect(response.assessment.parent_id).toEqual(assessment.id);
      expect(response.assessment.link_active).toBeFalsy();
      expect(response.parent_id).toEqual(assessment.id);
      expect(response.questions).toHaveLength(1);
      expect(response.questions[0].assessment_id).toEqual(assessmentChild.id);

      const currentVersions = (await assessmentVersionService.getVersions(2)).length;
      expect(versions + 1).toEqual(currentVersions);
    });

    test("should return assessment, question without parent", async () => {
      const response = await assessmentService.unlink(assessmentChild.id, user);
      expect(response.assessment.id).toEqual(assessmentChild.id);
      expect(response.parent_id).toBeUndefined();
      expect(response.assessment.parent_id).toEqual(assessment.id);
      expect(response.link_active).toBeFalsy();
      expect(response.questions).toHaveLength(1);
      expect(response.questions[0].assessment_id).toEqual(assessmentChild.id);
    });

  });

  describe('relink', async () => {

    test("should return 404 eror", async () => {
      await expect(assessmentService.relink(123, user))
        .rejects
        .toThrowError(new TRPCError({
          code: "NOT_FOUND",
          message: assessmentService.MSG_ERROR_NOT_FOUND,
        }));
    });

    test('should not relink - throw an exception: no parent', async () => {
      const newAssessment = await fakeAssessment({ id: 3 });
      await expect(assessmentService.relink(newAssessment.id, user))
        .rejects
        .toThrowError(new TRPCError({
          code: "BAD_REQUEST",
          message: assessmentService.MSG_ERROR_NOT_FOUND,
        }));
    });

    test('should not relink - link_active:true', async () => {
      await prisma.assessment.update({
        where: {
          id: assessmentChild.id
        }, 
        data: {
          link_active: true,
        }
      });
      const response = await assessmentService.relink(assessmentChild.id, user);
      
      expect(response.parent.id).toEqual(assessment.id);
      expect(response.parent.assessment_questions).length(1);
      expect(response.parent.assessment_questions[0].assessment_id).toEqual(assessment.id);

      expect(response.assessment.id).toEqual(assessmentChild.id);
      expect(response.assessment.link_active).toBeTruthy();
      expect(response.assessment.assessment_questions).length(1);
      expect(response.assessment.assessment_questions[0].assessment_id).toEqual(assessmentChild.id);
    });

    test('should relink (with version)', async () => {
      await prisma.assessment.update({
        where: {
          id: assessmentChild.id
        }, 
        data: {
          link_active: false,
        }
      });
      
      const versions = (await assessmentVersionService.getVersions(2)).length;
      const response = await assessmentService.relink(assessmentChild.id, user);
      
      expect(response.parent.id).toEqual(assessment.id);
      expect(response.parent.assessment_questions).length(1);
      expect(response.parent.assessment_questions[0].assessment_id).toEqual(assessment.id);

      expect(response.assessment.id).toEqual(assessmentChild.id);
      expect(response.assessment.link_active).toBeTruthy();
      expect(response.assessment.assessment_questions).length(1);
      expect(response.assessment.assessment_questions[0].assessment_id).toEqual(assessmentChild.id);

      const questions = await prisma.assessmentQuestion.findMany({
        where: {
          assessment_id: assessmentChild.id
        }
      });

      expect(questions).length(0);

      const currentVersions = (await assessmentVersionService.getVersions(2)).length;
      expect(versions + 1).toEqual(currentVersions);
    });

    test('should not relink - link_active:true', async () => {
      const response = await assessmentService.relink(assessmentChild.id, user);
      
      expect(response.parent.id).toEqual(assessment.id);
      expect(response.parent.assessment_questions).length(1);
      expect(response.parent.assessment_questions[0].assessment_id).toEqual(assessment.id);

      expect(response.assessment.id).toEqual(assessmentChild.id);
      expect(response.assessment.link_active).toBeTruthy();
      expect(response.assessment.assessment_questions).length(0);
    });
  })
})