import { beforeEach, beforeAll, describe, expect, test, vi } from "vitest";
import { clearDatabase } from "../../support/prismaHelpers";
import { assessmentFixture } from "$tests/fixtures/asessments";
import { QuizService } from "$server/services/quizService";
import { prisma } from "$server/db/prisma";
import { Assessment, Pool, User } from "@prisma/client";
import { fakeAssessmentQuestion } from "$tests/fixtures/asessmentQuestions";
import { userFixture } from "$tests/fixtures/users";
import { AssessmentVersionService } from "$server/services/assessmentVersionService";

global.fetch = vi.fn();

function fetchMockResponse(data: any) {
  return { json: () => new Promise((resolve) => resolve(data)) }
}

function createMockMaxScoreJson(references: {[key: string]: (number | null)[]} ) {
  const mock: any = {
    meta: {
      status: true,
    },
    data: {}
  };

  for (const reference of Object.keys(references)) {
    const scores = references[reference];
    const dataScore: any[] = [];
    for (let i = 0; i < scores.length; i++) {
      const score = scores[i];
      if (score === null) {
        dataScore.push({
          "data": {},
        });

        continue;
      }

      dataScore.push({
        "data": {
            "validation": {
                "max_score": score,
            }
        },
      });
    }

    mock.data[reference] = dataScore;
  }
  
  return mock;
}

describe("Quiz Service", () => {
  let user: User;
  let service: QuizService;
  let assessmentVersionService: AssessmentVersionService;

  beforeAll(async () => {
    await clearDatabase();
    service = new QuizService();
    assessmentVersionService = new AssessmentVersionService();
    user = await userFixture();
  });

  beforeEach(() => {
    fetch.mockClear();
  });

  describe("maxScore", async () => {

    test("should return 20 as max score", async () => {
      const reference = 'abc-123';
      const mockResponse = createMockMaxScoreJson({
        [reference]: [10, 10]
      });

      fetch.mockResolvedValueOnce(fetchMockResponse(mockResponse));

      const response = await service.maxScore([reference]);
      expect(response).toEqual({
        [reference]: 20
      });
    });

    test("should return 10 as max score", async () => {
      const reference = 'abc-123';
      const mockResponse = createMockMaxScoreJson({
        [reference]: [10, 0]
      });

      fetch.mockResolvedValueOnce(fetchMockResponse(mockResponse));

      const response = await service.maxScore([reference]);
      expect(response).toEqual({
        [reference]: 10
      });
    });

    test("should return 10 as max score (null example)", async () => {
      const reference = 'abc-123';
      const mockResponse = createMockMaxScoreJson({
        [reference]: [10, null]
      });

      fetch.mockResolvedValueOnce(fetchMockResponse(mockResponse));

      const response = await service.maxScore([reference]);
      expect(response).toEqual({
        [reference]: 10
      });
    });

    test("should return 0 as max score (status false response)", async () => {
      const reference = 'abc-123';
      const mockResponse = createMockMaxScoreJson({
        [reference]: [10, 10]
      });

      mockResponse.meta.status = false;
      fetch.mockResolvedValueOnce(fetchMockResponse(mockResponse));

      const response = await service.maxScore([reference]);
      expect(response).toEqual({
        [reference]: 0
      });
    });

    test("should return 0 as max score (empty response)", async () => {
      const reference = 'abc-123';
      fetch.mockResolvedValueOnce(fetchMockResponse({}));

      const response = await service.maxScore([reference]);
      expect(response).toEqual({
        [reference]: 0
      });
    });

    test("should return 0 as max score (wrong response)", async () => {
      const reference = 'abc-123';
      fetch.mockRejectedValue(() => Promise.reject("API is down"));

      const response = await service.maxScore([reference]);
      expect(response).toEqual({
        [reference]: 0
      });
    });

    test("should return 10, 15, 0 as max score (3 references)", async () => {
      const reference = 'abc-123';
      const reference2 = 'abc-456';
      const reference3 = 'abc-789';
      const mockResponse = createMockMaxScoreJson({
        [reference]: [3, 3 , 3, 1],
        [reference2]: [5, 0, 10],
        [reference3]: [null],
      });
      
      fetch.mockResolvedValue(fetchMockResponse(mockResponse));

      const response = await service.maxScore([reference, reference2, reference3]);
      expect(response).toEqual({
        [reference]: 10,
        [reference2]: 15,
        [reference3]: 0,
      });
    });
  });

  describe('save', async () => {
    let assessment: Assessment;
    let pool: Pool;
    let versions: number;

    beforeAll(async () => {
      assessment = await assessmentFixture();
      versions = (await assessmentVersionService.getVersions(assessment.id)).length;
    });

    test('should create a question', async () => {
      let quizAmount = await prisma.assessmentQuestion.aggregate({
        where: {
          assessment_id: assessment.id
        },
        _count: true
      });

      expect(quizAmount._count).equal(0);

      const item: any = fakeAssessmentQuestion();
      item.id = undefined;

      const questions = await service.save(assessment.id, [item], user);
      expect(questions).length(1);

      quizAmount = await prisma.assessmentQuestion.aggregate({
        where: {
          assessment_id: assessment.id
        },
        _count: true
      });

      expect(quizAmount._count).equal(1);

      versions++;
      const currentVersions = (await assessmentVersionService.getVersions(assessment.id)).length;
      expect(versions).toEqual(currentVersions);
    });

    test('should create 3 questions and remove previous question', async () => {
      let quizAmount = await prisma.assessmentQuestion.aggregate({
        where: {
          assessment_id: assessment.id
        },
        _count: true
      });

      expect(quizAmount._count).equal(1);

      const items: any[] = [
        fakeAssessmentQuestion(),
        fakeAssessmentQuestion(),
        fakeAssessmentQuestion(),
      ];
      for (let i = 0; i < items.length; i++) {
        const item = items[i];
        item.id = undefined;
      }

      const questions = await service.save(assessment.id, items, user);
      expect(questions).length(3);

      quizAmount = await prisma.assessmentQuestion.aggregate({
        where: {
          assessment_id: assessment.id
        },
        _count: true
      });

      expect(quizAmount._count).equal(3);

      versions++;
      const currentVersions = (await assessmentVersionService.getVersions(assessment.id)).length;
      expect(versions).toEqual(currentVersions);
    });

    test('should update 2 questions', async () => {
      const questions = await prisma.assessmentQuestion.findMany({
        where: {
          assessment_id: assessment.id
        }
      });

      for (let i = 1; i < questions.length; i++) {
        const question = questions[i];
        question.weight = i;
      }

      const saved = await service.save(assessment.id, questions, user);
      expect(saved).length(3);

      const assessmentQuestions = await prisma.assessmentQuestion.findMany({
        where: {
          assessment_id: assessment.id
        }
      });

      expect(assessmentQuestions.length).equal(3);

      for (let i = 0; i < assessmentQuestions.length; i++) {
        const saved = assessmentQuestions[i];
        
        expect(questions[i].id).equal(saved.id);
        expect(questions[i].weight).equal(saved.weight);
      }

      versions++;
      const currentVersions = (await assessmentVersionService.getVersions(assessment.id)).length;
      expect(versions).toEqual(currentVersions);
    });

    test('should update 2 questions and create a new one (with pool)', async () => {
      const questions:any[] = await prisma.assessmentQuestion.findMany({
        where: {
          assessment_id: assessment.id
        }
      });

      const data = [{
        sort_order: 10,
        weight: 20,
        num_to_select: 2,
        questions: [
          { 
            id: questions[0].id,
            item_reference: questions[0].item_reference,
          },{ 
            id: questions[1].id,
            item_reference: questions[1].item_reference,
          },
        ]
      }];

      const idToDelete = questions[2].id;
      questions[2] = fakeAssessmentQuestion();
      questions[2].id = undefined;

      data.push(questions[2]);

      const saved = await service.save(assessment.id, data, user);
      expect(saved).length(3);

      const assessmentQuestions = await prisma.assessmentQuestion.findMany({
        where: {
          assessment_id: assessment.id
        }
      });

      pool = await prisma.pool.findFirst({
        orderBy: {
          id: 'desc',
        },
      });


      expect(pool.weight).equal(data[0].weight);
      expect(pool.num_to_select).equal(data[0].num_to_select);

      expect(assessmentQuestions.length).equal(3);

      for (let i = 0; i < 2; i++) {
        const saved = assessmentQuestions[i];
        
        expect(saved.id).equal(questions[i].id);
        expect(saved.weight).equal(data[0].weight);
        expect(saved.sort_order).equal(data[0].sort_order);
        expect(saved.pool_id).equal(pool.id);
      }
      
      expect(assessmentQuestions[2].id).equal(idToDelete + 1);
      expect(assessmentQuestions[2].pool_id).toBeNull();

      versions++;
      const currentVersions = (await assessmentVersionService.getVersions(assessment.id)).length;
      expect(versions).toEqual(currentVersions);
    });

    test('should update 2 questions and create a new one (with pool updated)', async () => {
      const questions:any[] = await prisma.assessmentQuestion.findMany({
        where: {
          assessment_id: assessment.id,
          pool_id: pool.id
        }
      });

      const data = [{
        pool_id: pool.id,
        sort_order: 1,
        weight: 2,
        num_to_select: 1,
        questions,
      }];

      const saved = await service.save(assessment.id, data, user);
      expect(saved).length(2);

      const assessmentQuestions = await prisma.assessmentQuestion.findMany({
        where: {
          assessment_id: assessment.id
        }
      });

      const poolUpdated = await prisma.pool.findUnique({
        where: {
          id: pool.id
        }
      });
      expect(poolUpdated.weight).equal(data[0].weight);
      expect(poolUpdated.num_to_select).equal(data[0].num_to_select);

      expect(assessmentQuestions.length).equal(2);

      for (let i = 0; i < 2; i++) {
        const saved = assessmentQuestions[i];
        
        expect(saved.id).equal(questions[i].id);
        expect(saved.weight).equal(data[0].weight);
        expect(saved.sort_order).equal(data[0].sort_order);
        expect(saved.pool_id).equal(poolUpdated.id);
      }

      versions++;
      const currentVersions = (await assessmentVersionService.getVersions(assessment.id)).length;
      expect(versions).toEqual(currentVersions);
    });
  });
});