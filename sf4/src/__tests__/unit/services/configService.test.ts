import { beforeAll, describe, expect, test, vi } from "vitest";
import { ConfigService } from "$server/services/configService";

class ConfigServiceFake extends ConfigService {
  formatFront(value: any) {
    if (!value) {
      return value;
    }

    return Number(value) * 2;
  }

  formatBack(value: any) {
    if (!value) {
      return value;
    }

    return (Number(value) / 2);
  }

  formatParams (value: any, param1: number, param2: number) {    
    if (!value) {
      return value;
    }

    return (Number(value) * 2) + param1 + param2;
  }
}

describe("Config Service", () => {

  let service: ConfigServiceFake;
  beforeAll(async () => {
    service = new ConfigServiceFake();
  });

  describe("setPropertyObject", async () => {

    test("should set a new property", async () => {
      const object:any = {};
      service['setPropertyObject']('newProperty', object, '123');
      
      expect(object.newProperty).not.toBeUndefined();
      expect(object.newProperty).toEqual('123');
    });

    test("should set a grand child property", async () => {
      const object:any = {};
      service['setPropertyObject']('newProperty.childProperty', object, '123');
      
      expect(object.newProperty).not.toBeUndefined();
      expect(object.newProperty.childProperty).not.toBeUndefined();
      expect(object.newProperty.childProperty).toEqual('123');
    });

    test("should set a grand child property with a object filled", async () => {
      const object:any = {
        newProperty: {
          keyA: '345',
          childProperty: '345',
          keyB: '345',
        }
      };
      service['setPropertyObject']('newProperty.childProperty', object, '123');

      expect(object.newProperty).not.toBeUndefined();
      expect(object.newProperty.keyA).not.toBeUndefined();
      expect(object.newProperty.keyB).not.toBeUndefined();
      expect(object.newProperty.childProperty).not.toBeUndefined();
      expect(object.newProperty.keyA).toEqual('345');
      expect(object.newProperty.keyB).toEqual('345');
      expect(object.newProperty.childProperty).toEqual('123');
    });

  });

  describe("getPropertyObject", async () => {

    test("should get a property", async () => {
      const object:any = {
        newProperty: '123'
      };
      const value = service['getPropertyObject']('newProperty', object);
      expect(value).toEqual('123');
    });

    test("should get a grand child property", async () => {
      const object:any = {
        newProperty: {
          keyA: '345',
          childProperty: '123',
        }
      };
      const value = service['getPropertyObject']('newProperty.childProperty', object);
      expect(value).toEqual('123');
    });

    test("should get a grand child property with a object filled", async () => {
      const object:any = {
        newProperty: {
          keyA: '345',
          childProperty: '123',
          keyB: '345',
        }
      };
      const value = service['getPropertyObject']('newProperty.childProperty', object);
      expect(value).toEqual('123');
    });

    test("should get a property with function format", async () => {
      const object:any = {
        newProperty: {
          keyA: '345',
          childProperty: 123,
          keyB: '345',
        }
      };
      const value = service['getPropertyObject']('newProperty.childProperty', object, (val: number) => val * 2 );
      expect(value).toEqual(246);
    });

    test("should get a property with function format + params", async () => {
      const object:any = {
        newProperty: {
          keyA: '345',
          childProperty: 123,
          keyB: '345',
        }
      };

      const format = (val: number, number1: number, number2: number) => val * 2 + number1 + number2;
      const value = service['getPropertyObject']('newProperty.childProperty', object, format, 1, 2 );
      expect(value).toEqual(249);
    });

  });

  describe("configChoice", async () => {

    test("should get correct keys/values", async () => {
      const tests: any[] = [
        [null, 'YES_NO', true, null],
        ['No', 'YES_NO', true, 0],
        ['Yes', 'YES_NO', true, 1],
        ['yes', 'YES_NO', true, 1],
        ['0', 'YES_NO', false, 'No'],
        [1, 'YES_NO', false, 'Yes'],
        [2, 'GRADE_AGGREGATION', false, 'Average Grade'],
      ];

      for (let i = 0; i < tests.length; i++) {
        const test = tests[i];
        const value = service['configChoice'](test[0], test[1], test[2]);
        expect(value).toEqual(test[3]);
      }
    });

  });

  describe("stripScriptsAndEventHandlers", async () => {

    test("should return an string escaped", async () => {
      const tests: any[] = [
        ["'my test'", "'my test'"],
        ['my test', 'my test'],
        ["A 'quote' is <b>bold</b>", "A 'quote' is &#60;b&#62;bold&#60;/b&#62;"],
        ["\"hello\" <script>alert('asas')</script>", '"hello" '],
        ["\"hello\" <script>alert('asas')</script> again", '"hello"  again'],
        [null, null],
        ["", ""],
        [undefined, undefined],
      ];

      for (let i = 0; i < tests.length; i++) {
        const test = tests[i];
        const value = service['stripScriptsAndEventHandlers'](test[0]);
        expect(value).toEqual(test[1]);
      }
    });

  });

  describe("transformConfig", async () => {

    test("should transform config", async () => {
      const objectFrom:any = {
        keyA: '123',
        keyB: '456',
      };

      const dictionary = [
        {
          front: 'keyA',
          back: 'properties.A',
        },{
          front: 'keyB',
          back: 'properties.B',
        }
      ];

      const configBack:any = service.transformConfig('front', dictionary, objectFrom);
      
      expect(configBack.properties).not.toBeUndefined();
      expect(configBack.properties.A).not.toBeUndefined();
      expect(configBack.properties.B).not.toBeUndefined();
      expect(configBack.properties.A).toEqual('123');
      expect(configBack.properties.B).toEqual('456');

      // reverting
      const configFront:any = service.transformConfig('back', dictionary, configBack);

      expect(configFront.keyA).not.toBeUndefined();
      expect(configFront.keyB).not.toBeUndefined();
      expect(configFront.keyA).toEqual('123');
      expect(configFront.keyB).toEqual('456');
    });

    test("should transform config - less properties than dictionary", async () => {
      const objectFrom:any = {
        keyA: '123',
      };

      const dictionary = [
        {
          front: 'keyA',
          back: 'properties.A',
        },{
          front: 'keyB',
          back: 'properties.B',
        }
      ];

      const configBack:any = service.transformConfig('front', dictionary, objectFrom);
      
      expect(configBack.properties).not.toBeUndefined();
      expect(configBack.properties.A).not.toBeUndefined();
      expect(configBack.properties.B).toBeUndefined();
      expect(configBack.properties.A).toEqual('123');

      const objectBack:any = {
        properties: {
          B: '123'
        },
      };

      // reverting
      const configFront:any = service.transformConfig('back', dictionary, objectBack, true);
      
      expect(configFront).haveOwnProperty('keyA');
      expect(configFront.keyB).not.toBeUndefined();
      expect(configFront.keyB).toEqual('123');
    });

    test("should transform config using format function", async () => {
      const objectFrom:any = {
        keyA: '123',
        keyB: '456',
      };

      const dictionary = [
        {
          front: {
            name: 'keyA',
            format: 'formatFront',
          },
          back: 'properties.A',
        },{
          front: {
            name: 'keyB',
            format: 'formatFront',
          },
          back: {
            name: 'properties.B',
            format: 'formatBack',
          },
        }
      ];

      const configBack:any = service.transformConfig('front', dictionary, objectFrom);
      
      expect(configBack.properties).not.toBeUndefined();
      expect(configBack.properties.A).not.toBeUndefined();
      expect(configBack.properties.B).not.toBeUndefined();
      expect(configBack.properties.A).toEqual('123');
      expect(configBack.properties.B).toEqual(228);

      // reverting
      const configFront:any = service.transformConfig('back', dictionary, configBack);

      expect(configFront.keyA).not.toBeUndefined();
      expect(configFront.keyB).not.toBeUndefined();
      expect(configFront.keyA).toEqual(246);
      expect(configFront.keyB).toEqual(456);
    });

    test("should transform config using format function + params", async () => {
      const objectFrom:any = {
        keyA: '123',
        keyB: '456',
      };

      const dictionary = [
        {
          front: {
            name: 'keyA',
            format: 'formatParams',
            params: [1, 2]
          },
          back: 'properties.A',
        },{
          front: {
            name: 'keyB',
            format: 'formatFront',
          },
          back: {
            name: 'properties.B',
            format: 'formatBack',
          },
        }
      ];

      const configBack:any = service.transformConfig('front', dictionary, objectFrom);
      
      expect(configBack.properties).not.toBeUndefined();
      expect(configBack.properties.A).not.toBeUndefined();
      expect(configBack.properties.B).not.toBeUndefined();
      expect(configBack.properties.A).toEqual('123');
      expect(configBack.properties.B).toEqual(228);

      // reverting
      const configFront:any = service.transformConfig('back', dictionary, configBack);

      expect(configFront.keyA).not.toBeUndefined();
      expect(configFront.keyB).not.toBeUndefined();
      expect(configFront.keyA).toEqual(249);
      expect(configFront.keyB).toEqual(456);
    });
  });
});