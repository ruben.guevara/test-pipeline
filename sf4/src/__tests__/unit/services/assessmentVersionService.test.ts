import { beforeAll, describe, expect, test } from "vitest";
import { clearDatabase } from "../../support/prismaHelpers";
import { Assessment, AssessmentQuestion, User } from "@prisma/client";
import { AssessmentService } from "$server/services/assessmentService";
import { TRPCError } from "@trpc/server";
import { assessmentFixture, fakeAssessment } from "$tests/fixtures/asessments";
import { assessmentQuestionFixture } from "$tests/fixtures/asessmentQuestions";
import { prisma } from "$server/db/prisma";
import { userFixture } from "$tests/fixtures/users";
import { AssessmentVersionService } from "$server/services/assessmentVersionService";
import { createAssessmentVersion } from "$lib/version";

describe("Assessment Version Service", () => {
  let user: User;
  let versions: number = 0;
  let assessment: Assessment;
  let assessmentChild: Assessment;
  let question: AssessmentQuestion;
  let assessmentService: AssessmentService;
  let assessmentVersionService: AssessmentVersionService;

  beforeAll(async () => {
    await clearDatabase();
    assessmentVersionService = new AssessmentVersionService();
    user = await userFixture();
    assessment = await assessmentFixture();
  });

  describe("create version", async () => {

    test("should return create a assessment version", async () => {
      await createAssessmentVersion(assessment.id, user);
      const currentVersions = await assessmentVersionService.getVersions(assessment.id);
      versions++;
      expect(versions).toEqual(currentVersions.length);
      expect(currentVersions[0].version_data.id).toEqual(assessment.id);
      expect(currentVersions[0].version_data.link_active).toEqual(assessment.link_active);
      expect(currentVersions[0].version_data.name).toEqual(assessment.name);
    });
  });

  describe("get versions", async () => {

    test("should return all assessment versions", async () => {

      await prisma.assessment.update({
        where: {
          id: assessment.id
        },
        data: {
          link_active: true,
          name: 'name updated',
        }
      });

      for (let i = 0; i < 10; i++) {
        await createAssessmentVersion(assessment.id, user);
        versions++;
      }

      const currentVersions = await assessmentVersionService.getVersions(assessment.id);
      
      expect(versions).toEqual(currentVersions.length);
      expect(currentVersions[0].version_data.id).toEqual(assessment.id);
      expect(currentVersions[0].version_data.link_active).toEqual(true);
      expect(currentVersions[0].version_data.name).toEqual('name updated');
    });
  });

  describe("restore version", async () => {

    test("should restore a previous version", async () => {
      
      const currentVersion = await assessmentVersionService.restore(1, user);
      const currentVersions = await assessmentVersionService.getVersions(assessment.id);

      versions++;
      expect(versions).toEqual(currentVersions.length);
      expect(currentVersion.version_data.id).toEqual(assessment.id);
      expect(currentVersion.version_data.link_active).toEqual(assessment.link_active);
      expect(currentVersion.version_data.name).toEqual(assessment.name);
    });
  });

})