import { fireEvent, render, screen } from "@testing-library/react";
import "@testing-library/react";
import { describe, expect, it, test } from "vitest";
import {
  QuizContext,
  QuizContextProvider,
} from "../../../context/quiz/QuizContext";
import { IQuestion, IQuiz } from "../../../context/quiz/types";

const dummyItem: IQuestion = {
  id: "some-id",
  type: "question",
  title: "New question",
  status: "published",
  description: null,
  questions: {
    data: [
      {
        reference: "some-reference",
        type: "mcq",
        widget_type: "some-widget-type",
        data: {
          type: "mcq",
          stimulus: "Some question stimulus",
          options: [
            {
              label: "some question option label",
              value: 1,
            },
          ],
        },
      },
    ],
  },
};

const dummyQuizData: IQuiz = {
  id: 0,
  name: "Dummy Quiz",
  meta: {
    chapter: {
      id: 1,
      name: "Unit Testing 101",
    },
    course: {
      id: 1,
      name: "Test Course",
    },
    region: {
      id: 1,
      name: "British Columbia",
    },
  },
  items: [],
};

describe("QuizProvider", () => {
  it("provides region, course, chapter and quiz name", () => {
    render(
      <QuizContextProvider initialData={dummyQuizData}>
        <QuizContext.Consumer>
          {({ data }) => (
            <>
              <span>{data.meta.region?.name}</span>
              <span>{data.meta.course?.name}</span>
              <span>{data.meta.chapter?.name}</span>
              <span>{data.name}</span>
            </>
          )}
        </QuizContext.Consumer>
      </QuizContextProvider>
    );

    const regionName = screen.getByText(
      dummyQuizData.meta.region?.name as string
    );
    const courseName = screen.getByText(
      dummyQuizData.meta.course?.name as string
    );
    const chapterName = screen.getByText(
      dummyQuizData.meta.chapter?.name as string
    );
    const quizName = screen.getByText(dummyQuizData.name);

    expect(regionName).toBeInTheDocument();
    expect(courseName).toBeInTheDocument();
    expect(chapterName).toBeInTheDocument();
    expect(quizName).toBeInTheDocument();
  });

  it("adds and remove items from the quiz", () => {
    render(
      <QuizContextProvider initialData={dummyQuizData}>
        <QuizContext.Consumer>
          {({ data, dispatch }) => (
            <>
              <span data-testid="num-items">
                number of items {data.items.length}
              </span>
              <button
                data-testid="dispatch-button-add"
                onClick={() =>
                  dispatch({
                    type: "add_item_to_quiz",
                    payload: dummyItem,
                  })
                }
              >
                Add Item
              </button>
              <button
                data-testid="dispatch-button-remove"
                onClick={() =>
                  dispatch({
                    type: "remove_item_from_quiz",
                    payload: ["some-id"],
                  })
                }
              >
                Remove Item
              </button>
            </>
          )}
        </QuizContext.Consumer>
      </QuizContextProvider>
    );

    const addButton = screen.getByTestId("dispatch-button-add");
    const removeButton = screen.getByTestId("dispatch-button-remove");

    expect(screen.getByTestId("num-items")).toHaveTextContent(
      "number of items 0"
    );
    fireEvent.click(addButton);
    expect(screen.getByTestId("num-items")).toHaveTextContent(
      "number of items 1"
    );
    fireEvent.click(removeButton);
    expect(screen.getByTestId("num-items")).toHaveTextContent(
      "number of items 0"
    );
  });
});
