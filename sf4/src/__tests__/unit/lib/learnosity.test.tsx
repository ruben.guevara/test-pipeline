import { Learnosity } from "$lib/learnosity";
import { describe, expect, test } from "vitest";

describe("Learnosity", () => {
  test("dataApiRequest", async () => {
    const learnosity = new Learnosity();
    const data: any = await learnosity.dataApiRequest('jobs', {
      limit: 50 
    })
            
    expect(data).not.toBeUndefined();
    expect(data.meta).not.toBeUndefined();
    expect(data.meta.status).toEqual(true);
  });

  test("overrideSecurity", async () => {
    const learnosity = new Learnosity();
    learnosity.overrideSecurity({ 'consumer_key': 'wrong consumer key'})
    const data: any = await learnosity.dataApiRequest('jobs', {
      limit: 50 
    })
            
    expect(data).not.toBeUndefined();
    expect(data.meta).not.toBeUndefined();
    expect(data.meta.status).toEqual(false);
  });
})