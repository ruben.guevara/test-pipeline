import { QuizItem } from "$components/quiz/builder/QuizItem";
import "@testing-library/react";
import { render, screen } from "@testing-library/react";
import { describe, expect, it, vi } from "vitest";
import { IQuestion } from "../../../../../context/quiz/types";

const scrollIntoView = vi.fn();

const questionProps: IQuestion & { index: number } = {
  index: 0,
  id: "examgen-376e64a81e264dd5b6e3d115ff2b0a20",
  title: "This a test title",
  description: "",
  status: "published",
  type: "question",
  weight: 1,
  questions: {
    data: [
      {
        type: "mcq",
        reference: "question-reference",
        widget_type: "response",
        data: {
          type: "mcq",
          stimulus: "<p>This the description of the question</p>",
          options: [
            { label: "First Option", value: 0 },
            { label: "Last Option", value: 1 },
          ],
        },
      },
    ],
  },
};

const questionProps2: IQuestion & { index: number } = {
  index: 1,
  id: "GENO_REFERENCE_TEST",
  title: "This a test title",
  description: "",
  type: "question",
  status: "published",
  weight: 1,
  questions: {
    data: [
      {
        type: "plaintext",
        reference: "question-reference",
        widget_type: "response",
        data: {
          type: "plaintext",
          stimulus: "<p>This the description of the question</p>",
        },
      },
    ],
  },
};

describe("SearchQuestionItem", () => {
  it("Renders all of it props", () => {
    render(
      <QuizItem
        item={questionProps}
        index={0}
        scrollIntoView={scrollIntoView}
      />
    );

    const reference = screen.getByTestId("question-reference");
    const title = screen.getByTestId("question-title");

    expect(reference).toHaveTextContent(`ID: ${questionProps.id}`);
    expect(title).toHaveTextContent(questionProps.title as string);
  });

  it("render the options if its from the type mcq", () => {
    render(
      <QuizItem
        item={questionProps}
        index={0}
        scrollIntoView={scrollIntoView}
      />
    );

    const option1 = screen.getByText("First Option");
    const option2 = screen.getByText("Last Option");

    expect(option1).toBeInTheDocument();
    expect(option2).toBeInTheDocument();
  });

  it("show a label contextual response if its type is plaintext", () => {
    render(
      <QuizItem
        item={questionProps2}
        index={0}
        scrollIntoView={scrollIntoView}
      />
    );

    const label = screen.getByText("*contextual response*");

    expect(label).toBeInTheDocument();
  });
});
