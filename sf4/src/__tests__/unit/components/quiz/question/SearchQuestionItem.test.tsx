import { SearchQuestionItem } from "$components/quiz/builder/SearchBarQuestionItem";
import { IQuestion } from "$components/quiz/question/types";
import { render, screen } from "@testing-library/react";
import "@testing-library/react";
import { describe, expect, it } from "vitest";

const questionProps: IQuestion & { isDuplicate: boolean } = {
  isDuplicate: false,
  reference: "GENO_REFERENCE_TEST",
  title: "This a test title",
  description: "",
  status: "",
  questions: {
    meta: {
      records: 1,
    },
    data: [
      {
        type: "mcq",
        reference: "question-reference",
        widget_type: "response",
        data: {
          type: "mcq",
          stimulus: "<p>This the description of the question</p>",
          options: [
            { label: "First Option", value: 0 },
            { label: "Last Option", value: 1 },
          ],
        },
      },
    ],
  },
};

const questionProps2: IQuestion & { isDuplicate: boolean } = {
  isDuplicate: false,
  reference: "GENO_REFERENCE_TEST",
  title: "This a test title",
  description: "",
  status: "",
  questions: {
    meta: {
      records: 1,
    },
    data: [
      {
        type: "plaintext",
        reference: "question-reference",
        widget_type: "response",
        data: {
          type: "plaintext",
          stimulus: "<p>This the description of the question</p>",
        },
      },
    ],
  },
};

describe("SearchQuestionItem", () => {
  it("Renders all of it props", () => {
    render(<SearchQuestionItem {...questionProps} />);

    const reference = screen.getByTestId("question-reference");
    const title = screen.getByTestId("question-title");

    expect(reference).toHaveTextContent(`Ref: ${questionProps.reference}`);
    expect(title).toHaveTextContent(questionProps.title as string);
  });

  it("changes its opacity to 50% if its duplicate", () => {
    render(<SearchQuestionItem {...questionProps} isDuplicate={true} />);

    const question = screen.getByTestId("question-container");

    expect(question).toHaveClass("opacity-50");
  });

  it("render the options if its from the type mcq", () => {
    render(<SearchQuestionItem {...questionProps} />);

    const option1 = screen.getByText("First Option");
    const option2 = screen.getByText("Last Option");

    expect(option1).toBeInTheDocument();
    expect(option2).toBeInTheDocument();
  });

  it("show a label contextual response if its type is plaintext", () => {
    render(<SearchQuestionItem {...questionProps2} />);

    const label = screen.getByText("*contextual response*");

    expect(label).toBeInTheDocument();
  });
});
