import { Modal, ModalProps } from "$components/shared/Modal";
import { render, screen } from "@testing-library/react";
import "@testing-library/react";
import { describe, expect, test } from "vitest";

const mockFn = () => {};

const dialogModal: ModalProps = {
  type: "dialog",
  id: "dialog-id",
  title: "Dialog Modal Test",
  content: "This is test content for the dialog modal",
  onConfirm: mockFn,
  onConfirmLabel: "Confirm",
  onDismissLabel: "Cancel",
};

const flyoutModal: ModalProps = {
  type: "flyout",
  id: "flyout-id",
  title: "Flyout Modal Test",
  content: "This is test content for the flyout modal",
  onDismissLabel: "Close",
};

describe("Dialog Modal", () => {
  test("renders the props", () => {
    render(<Modal {...dialogModal} />);

    const modal = screen.getByRole("dialog");

    const title = screen.getByRole("heading");
    const content = modal.querySelector("p");
    const confirmButton = screen.getByTestId("dialog-confirm-button");
    const dismissButton = screen.getByTestId("dialog-dismiss-button");

    expect(title).toHaveTextContent(dialogModal.title);
    expect(content).toHaveTextContent(dialogModal.content);
    expect(confirmButton).toHaveTextContent(dialogModal.onConfirmLabel);
    expect(dismissButton).toHaveTextContent(dialogModal.onDismissLabel);
  });
});

describe("Flyout Modal", () => {
  test("renders the props", () => {
    render(<Modal {...flyoutModal} />);

    const modal = screen.getByRole("dialog");

    const title = screen.getByRole("heading");
    const content = modal.querySelector("p");
    const onDismissLabel = screen.getByTestId("flyout-dismiss-button");

    expect(title).toHaveTextContent(flyoutModal.title);
    expect(content).toHaveTextContent(flyoutModal.content);
    expect(onDismissLabel).toHaveTextContent(flyoutModal.onDismissLabel);
  });
});
