import { Table } from "$components/shared/Table";
import { render, screen } from "@testing-library/react";
import { describe, expect, test } from "vitest";

type Person = {
  id: number;
  name: string;
  birthday: Date;
  age: number;
  favouriteThing: string;
  link: { href: AbsoluteUrl; text: string };
};

const kids: Person[] = [
  {
    id: 1,
    name: "Theo",
    birthday: new Date("Jan 20, 2019"),
    age: 4,
    favouriteThing: "Cars",
    link: { href: "https://example1.com", text: "Example 1" },
  },
  {
    id: 2,
    name: "Ellie",
    birthday: new Date("Sep 21, 2021"),
    age: 1,
    favouriteThing: "Dogs",
    link: { href: "https://example2.com", text: "Example 2" },
  },
];

const adult: Person = {
  id: 3,
  name: "Kenny",
  birthday: new Date("Jan 27, 1991"),
  age: 32,
  favouriteThing: "Video Games",
  link: { href: "https://example3.com", text: "Example 3" },
};

describe("Table", () => {
  test("default settings", () => {
    render(
      <Table
        items={kids}
        columns={{
          id: "id",
          name: "name",
          birthday: "date",
          age: "number",
          favouriteThing: "string",
          link: "link",
        }}
      />
    );
    const table = screen.getByRole("table");

    const headers = Array.from(table.querySelectorAll("th")).map(
      (header) => header.textContent
    );
    expect(headers).toEqual([
      "Id",
      "Name",
      "Birthday",
      "Age",
      "Favourite Thing",
      "Link",
    ]);

    const rows = Array.from(table.querySelectorAll("tbody tr"))
      .map((row) =>
        Array.from(row.querySelectorAll("td")).map((cell) => cell.textContent)
      )
      .filter((row) => row.length);
    expect(rows).toEqual([
      ["1", "Theo", "20/01/2019", "4", "Cars", "Example 1"],
      ["2", "Ellie", "21/09/2021", "1", "Dogs", "Example 2"],
    ]);

    const links = Array.from(table.querySelectorAll("tbody td a")).map((link) =>
      link.getAttribute("href")
    );
    expect(links).toEqual(["https://example1.com", "https://example2.com"]);
  });

  test("custom titles", () => {
    render(
      <Table
        items={kids}
        columns={{
          id: { type: "id", title: "ID" },
          name: { type: "name", title: "NAME" },
          birthday: { type: "date", title: "DATE" },
          age: { type: "number", title: "NUMBER" },
          favouriteThing: { type: "string", title: "FAVOURITE" },
          link: { type: "link", title: "LINK" },
        }}
      />
    );
    const table = screen.getByRole("table");
    const headers = Array.from(table.querySelectorAll("th")).map(
      (header) => header.textContent
    );
    expect(headers).toEqual([
      "ID",
      "NAME",
      "DATE",
      "NUMBER",
      "FAVOURITE",
      "LINK",
    ]);
  });

  test("custom static values", () => {
    render(
      <Table
        items={kids}
        columns={{
          id: { type: "id", value: adult.id },
          name: { type: "name", value: adult.name },
          birthday: { type: "date", value: adult.birthday },
          age: { type: "number", value: adult.age },
          favouriteThing: { type: "string", value: adult.favouriteThing },
          link: {
            type: "link",
            value: adult.link,
          },
        }}
      />
    );
    const table = screen.getByRole("table");
    const rows = Array.from(table.querySelectorAll("tbody tr"))
      .map((row) =>
        Array.from(row.querySelectorAll("td")).map((cell) => cell.textContent)
      )
      .filter((row) => row.length);
    expect(rows).toEqual([
      ["3", "Kenny", "27/01/1991", "32", "Video Games", "Example 3"],
      ["3", "Kenny", "27/01/1991", "32", "Video Games", "Example 3"],
    ]);

    const links = Array.from(table.querySelectorAll("tbody td a")).map((link) =>
      link.getAttribute("href")
    );
    expect(links).toEqual(["https://example3.com", "https://example3.com"]);
  });

  test("custom dynamic values", () => {
    render(
      <Table
        items={kids}
        columns={{
          id: {
            type: "id",
            value: (kid) => (kid.id === 2 ? adult.id : kid.id),
          },
          name: {
            type: "name",
            value: (kid) => (kid.id === 2 ? adult.name : kid.name),
          },
          birthday: {
            type: "date",
            value: (kid) => (kid.id === 2 ? adult.birthday : kid.birthday),
          },
          age: {
            type: "number",
            value: (kid) => (kid.id === 2 ? adult.age : kid.age),
          },
          favouriteThing: {
            type: "string",
            value: (kid) =>
              kid.id === 2 ? adult.favouriteThing : kid.favouriteThing,
          },
          link: {
            type: "link",
            value: (kid) => (kid.id === 2 ? adult.link : kid.link),
          },
        }}
      />
    );
    const table = screen.getByRole("table");
    const rows = Array.from(table.querySelectorAll("tbody tr"))
      .map((row) =>
        Array.from(row.querySelectorAll("td")).map((cell) => cell.textContent)
      )
      .filter((row) => row.length);
    expect(rows).toEqual([
      ["1", "Theo", "20/01/2019", "4", "Cars", "Example 1"],
      ["3", "Kenny", "27/01/1991", "32", "Video Games", "Example 3"],
    ]);

    const links = Array.from(table.querySelectorAll("tbody td a")).map((link) =>
      link.getAttribute("href")
    );
    expect(links).toEqual(["https://example1.com", "https://example3.com"]);
  });

  test("custom component", () => {
    render(
      <Table
        items={kids}
        columns={{
          greet: {
            type: "custom",
            tdProps: (kid) => ({ title: `hi ${kid.name}` }),
            render: (kid: Person) => <div>Hello {kid.name}</div>,
          },
        }}
      />
    );
    const table = screen.getByRole("table");
    const rowsText = Array.from(table.querySelectorAll("tbody tr"))
      .map((row) =>
        Array.from(row.querySelectorAll("td")).map((cell) => cell.textContent)
      )
      .filter((row) => row.length);
    expect(rowsText).toEqual([["Hello Theo"], ["Hello Ellie"]]);
    const rowsAttr = Array.from(table.querySelectorAll("tbody tr"))
      .map((row) =>
        Array.from(row.querySelectorAll("td")).map((cell) =>
          cell.getAttribute("title")
        )
      )
      .filter((row) => row.length);
    expect(rowsAttr).toEqual([["hi Theo"], ["hi Ellie"]]);
  });
});
