import { prisma } from "$server/db/prisma";
import { Role } from "@prisma/client";

export const roles: Role[] = [
  {
    id: 1,
    name: "guest",
    description: "Default role if none is assigned",
    date_created: new Date(),
    date_last_updated: new Date(),
    created_by_id: null,
    updated_by_id: null,
    priority: 99,
  },
  {
    id: 2,
    name: "admin",
    description: "Global Admin",
    date_created: new Date(),
    date_last_updated: new Date(),
    created_by_id: null,
    updated_by_id: null,
    priority: 1,
  },
  {
    id: 6,
    name: "teacher",
    description: "Teacher",
    date_created: new Date(),
    date_last_updated: new Date(),
    created_by_id: null,
    updated_by_id: null,
    priority: 2,
  },
  {
    id: 5,
    name: "student",
    description: "Student",
    date_created: new Date(),
    date_last_updated: new Date(),
    created_by_id: null,
    updated_by_id: null,
    priority: 3,
  },
];

export const roleFixtures = async () => {
  for (const role of roles) {
    await prisma.role.upsert({
      create: role,
      update: role,
      where: { id: role.id },
    });
  }
};
