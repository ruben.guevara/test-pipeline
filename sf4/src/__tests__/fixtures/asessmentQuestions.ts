import { prisma } from "$server/db/prisma";
import { faker } from "@faker-js/faker";
import { AssessmentQuestion } from "@prisma/client";

export function fakeAssessmentQuestion(opts?: Partial<AssessmentQuestion>): AssessmentQuestion {
  return {
    id: 1,
    assessment_id: null,
    pool_id: null,
    item_reference: faker.random.alphaNumeric(32).toString(),
    sort_order: 0,
    weight: 1,
    ...opts,
  };
}

export async function assessmentQuestionFixture(opts?: Partial<AssessmentQuestion>): Promise<AssessmentQuestion> {
  let assessmentQuestion: AssessmentQuestion = fakeAssessmentQuestion(opts);

  assessmentQuestion = await prisma.assessmentQuestion.upsert({
    create: assessmentQuestion,
    update: assessmentQuestion,
    where: { id: assessmentQuestion.id },
  });
  return assessmentQuestion;
}
