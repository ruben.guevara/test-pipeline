import { prisma } from "$server/db/prisma";
import { faker } from "@faker-js/faker";
import { User } from "@prisma/client";

export function fakeUser(opts?: Partial<User>): User {
  const firstname = faker.name.firstName();
  const lastname = faker.name.lastName();
  const email = faker.internet.email(firstname, lastname);
  const username = faker.internet.userName(firstname, lastname);
  return {
    id: 1,
    email,
    email_canonical: email,
    password: faker.internet.password(),
    active: true,
    enabled: true,
    firstname,
    lastname,
    roles: "",
    timezone: "America/Vancouver",
    username,
    username_canonical: username,
    institution_id: null,
    default_course_id: null,
    auth: "manual",
    email_stop: false,
    acc_settings: "{}",
    christian: false,
    confirmation_token: null,
    default_course_auto_set: false,
    email_confirmed: true,
    first_access: faker.date.past(),
    last_access: faker.date.recent(),
    last_login: faker.date.recent(),
    force_password_change: false,
    last_ip: null,
    lti_consumer_key: null,
    lti_user_id: null,
    lti_username: null,
    password_changed: false,
    password_requested_at: null,
    region_id: null,
    requested_role: null,
    salt: null,
    school_name: faker.company.name(),
    show_next_video_popup: false,
    sign_up_reason: faker.lorem.paragraph(),
    ...opts,
  };
}

export async function userFixture(opts?: Partial<User>): Promise<User> {
  let user: User = fakeUser(opts);

  user = await prisma.user.upsert({
    create: user,
    update: user,
    where: { id: user.id },
  });
  return user;
}
