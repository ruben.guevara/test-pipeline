import { prisma } from "$server/db/prisma";
import { faker } from "@faker-js/faker";
import { Permission } from "@prisma/client";
import { roleFixtures } from "./roles";

export function fakePermission(opts?: Partial<Permission>): Permission {
  return {
    id: 1,
    name: "fake_permission",
    description: "Fake permission",
    date_created: faker.date.past(),
    date_last_updated: faker.date.past(),
    created_by_id: null,
    updated_by_id: null,
    ...opts,
  };
}

export const permissions: Permission[] = [
  fakePermission({
    id: 21,
    name: "region_create",
    description: "Create a region",
  }),
  fakePermission({
    id: 22,
    name: "region_delete",
    description: "Delete a region",
  }),
  fakePermission({
    id: 23,
    name: "region_edit",
    description: "Edit a region",
  }),
  fakePermission({
    id: 33,
    description: "Remove a student from a class",
    name: "class_student_manage",
  }),
  fakePermission({
    id: 35,
    description: "Remove a teacher from a class",
    name: "class_teacher_manage",
  }),
  fakePermission({
    id: 48,
    description: "View a student's progress or reports",
    name: "class_student_view_as",
  }),
  fakePermission({
    id: 49,
    description: "View a teacher's progress or reports",
    name: "class_teacher_view_as",
  }),
  fakePermission({
    id: 73,
    description: "Permission to change the default course and course region of a class",
    name: "class_change_dcourse",
  }),
  fakePermission({
    id: 126,
    description: "Can join other groups with a group code",
    name: "group_join",
  }),
];

const permissionValues = [
  {
    role_id: 2,
    permission_id: 21,
    value: "ALLOW",
  },
  {
    role_id: 2,
    permission_id: 22,
    value: "ALLOW",
  },
  {
    role_id: 2,
    permission_id: 23,
    value: "ALLOW",
  },
  {
    role_id: 2,
    permission_id: 33,
    value: "ALLOW",
  },
  {
    role_id: 6,
    permission_id: 33,
    value: "ALLOW",
  },
  {
    role_id: 2,
    permission_id: 35,
    value: "ALLOW",
  },
  {
    role_id: 2,
    permission_id: 48,
    value: "ALLOW",
  },
  {
    role_id: 6,
    permission_id: 48,
    value: "ALLOW",
  },
  {
    role_id: 2,
    permission_id: 49,
    value: "ALLOW",
  },
  {
    role_id: 2,
    permission_id: 73,
    value: "ALLOW",
  },
  {
    role_id: 6,
    permission_id: 73,
    value: "ALLOW",
  },
  {
    role_id: 2,
    permission_id: 126,
    value: "DENY",
  },
  {
    role_id: 6,
    permission_id: 126,
    value: "DENY",
  },
  {
    role_id: 5,
    permission_id: 126,
    value: "ALLOW",
  },
  {
    role_id: 1,
    permission_id: 126,
    value: "ALLOW",
  },
];

export async function permissionFixtures() {
  await roleFixtures();
  for (const permission of permissions) {
    await prisma.permission.upsert({
      create: permission,
      update: permission,
      where: { id: permission.id },
    });
  }
  await prisma.permissionValue.createMany({
    data: permissionValues,
  });
}
