import { prisma } from "$server/db/prisma";
import { faker } from "@faker-js/faker";
import { Assessment } from "@prisma/client";

export function fakeAssessment(opts?: Partial<Assessment>): Assessment {
  return {
    id: 1,
    created_by_id: null,
    updated_by_id: null,
    reference: faker.random.alphaNumeric(32).toString(),
    render_type: Number(faker.random.numeric(1)),
    time_limit: null,
    max_number_of_attempts: 10,
    name: faker.lorem.sentence(5),
    date_created: new Date(),
    date_last_updated: faker.date.recent(),
    grade_aggregation: 'Natural',
    config: null,
    theme_id: null,
    css: '',
    date_compiled: faker.date.recent(),
    course_id: null,
    chapter_id: null,
    parent_id: null,
    sort_order: null,
    number: null,
    link_active: false,
    date_published: faker.date.recent(),
    ...opts,
  };
}

export async function assessmentFixture(opts?: Partial<Assessment>): Promise<Assessment> {
  let assessment: Assessment = fakeAssessment(opts);

  assessment = await prisma.assessment.upsert({
    create: assessment,
    update: assessment,
    where: { id: assessment.id },
  });
  return assessment;
}
