import { prisma } from "$server/db/prisma";
import { faker } from "@faker-js/faker";
import type { Context, PrismaClient } from "@prisma/client";

export function fakeContext(opts?: Partial<Context>): Context {
  return {
    id: 1,
    name: faker.lorem.word(),
    date_created: new Date(),
    date_last_updated: new Date(),
    partner_id: null,
    institution_id: null,
    group_id: null,
    created_by_id: null,
    updated_by_id: null,
    parent_id: null,
    lft: 0,
    rgt: 0,
    ...opts,
  };
}

export async function contextFixture(opts?: Partial<Context>): Promise<Context> {
  let context = fakeContext(opts);

  context = await prisma.context.upsert({
    create: context,
    update: context,
    where: { id: context.id },
  });
  return context;
}

export async function contextFixtureTree() {
  const root = await contextFixture({ id: 1, lft: 1, rgt: 14, name: "root" });
  const child1 = await contextFixture({ id: 2, lft: 2, rgt: 7, parent_id: 1 });
  const child2 = await contextFixture({ id: 3, lft: 8, rgt: 13, parent_id: 1 });
  const grandchild1 = await contextFixture({ id: 4, lft: 3, rgt: 4, parent_id: 2 });
  const grandchild2 = await contextFixture({ id: 5, lft: 5, rgt: 6, parent_id: 2 });
  const grandchild3 = await contextFixture({ id: 6, lft: 9, rgt: 10, parent_id: 3 });
  const grandchild4 = await contextFixture({ id: 7, lft: 11, rgt: 12, parent_id: 3 });

  return {
    root,
    children: [child1, child2],
    grandchildren: [grandchild1, grandchild2, grandchild3, grandchild4],
  } as const;
}
