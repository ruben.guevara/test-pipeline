import { loadEnvConfig } from "@next/env";

export function setup() {
  // load the environment variables the same way Next.js does
  loadEnvConfig(process.cwd());
}
