import { QUESTION_TYPES } from "$server/api/schemas";
import { AssessmentQuestion, Pool } from "@prisma/client";
import cloneDeep from "lodash.clonedeep";
import {
  IActionsStack,
  IAssessment,
  IAssessmentPool,
  IAssessmentQuestion,
  IPool,
  IQuestion,
  IQuiz,
  IReducerState,
  LearnosityItem,
} from "./types";

type QuestionTypesTuple = typeof QUESTION_TYPES;
type QuestionType = QuestionTypesTuple[number];
interface InitializeProps {
  db: IQuiz;
  storage: IQuiz | null;
}

function listInitialQuizItems(
  items: (AssessmentQuestion & {
    pool: Pool | null;
  })[],
  data: LearnosityItem
): Array<IQuestion | IPool> {
  const list = items
    .sort((a, b) => {
      return a.sort_order < b.sort_order
        ? -1
        : a.sort_order > b.sort_order
        ? 1
        : 0;
    })
    .reduce<Array<IQuestion | IPool>>((accumulated, current) => {
      const learnosityData = data.find(
        (item) => item.reference === current.item_reference
      );

      const question: IQuestion = {
        id: current.item_reference as string,
        question_id: current.id,
        description: learnosityData?.description as string,
        status: learnosityData?.status as string,
        title: learnosityData?.title as string,
        weight: current.weight,
        type: "question",
        questions: {
          data:
            learnosityData?.questions.data.map((q) => {
              return {
                type: q.type as unknown as QuestionType,
                reference: q.reference,
                widget_type: q.widget_type,
                data: {
                  type: q.type as unknown as QuestionType,
                  stimulus: q.data.stimulus,
                },
              };
            }) || [],
        },
        deactivated: !learnosityData,
      };

      if (current.pool_id) {
        if (accumulated.find((item) => Number(item.id) === current.pool_id)) {
          return accumulated.map((q) => {
            if (q.type === "pool" && q.pool_id === current.pool_id) {
              return { ...q, questions: [...q.questions, question] };
            }
            return q;
          });
        }

        const pool: IPool = {
          id: `${current.pool_id}`,
          assessment_id: current.assessment_id as number,
          pick: current.pool?.num_to_select as number,
          type: "pool",
          weight: current.pool?.weight as number,
          pool_id: current.pool_id,
          questions: [question],
        };

        return [...accumulated, pool];
      }

      return [...accumulated, question];
    }, []);

  return list;
}

function initializeReducer({ db, storage }: InitializeProps): IReducerState {
  // TODO: once we have the load questions function we check which is the most recent to show on screen

  if (!storage || storage.id !== db.id || db.link_active) {
    if (typeof window !== "undefined") {
      localStorage.removeItem("@studyforge:quizbuilder-data");
    }

    return {
      actionsStack: [
        {
          ...db,
          items: updatePercentage(db.items),
        },
      ],
      pointer: 0,
      direction: "new",
      referencesToVerify: [],
    };
  }

  // Get the references for all the questions
  const referencesToVerify = storage.items.reduce((accumulated, current) => {
    if (current.type === "question") {
      return [...accumulated, current.id];
    } else {
      return [...accumulated, ...current.questions.map((q) => q.id)];
    }
  }, [] as string[]);

  return {
    actionsStack: [
      {
        ...storage,
        link_active: db.link_active,
        items: updatePercentage(storage.items),
        meta: { ...db.meta },
      },
    ],
    pointer: 0,
    direction: "new",
    referencesToVerify,
  };
}

function getChangesFromState(state: IQuiz): IAssessment {
  const payload: IAssessment = {
    assessment_id: state.id,
    items: state.items.map((item, index) => {
      switch (item.type) {
        case "pool":
          const pool: IAssessmentPool = {
            pool_id:
              item.id.indexOf("new-pool") === -1 ? Number(item.id) : undefined,
            num_to_select: item.pick,
            weight: item.weight,
            sort_order: index,
            questions: item.questions.map((q) => ({
              id: q.question_id,
              item_reference: q.id,
            })),
          };
          return pool;
        case "question":
          const question: IAssessmentQuestion = {
            id: item.question_id,
            item_reference: item.id,
            sort_order: index,
            weight: item.weight,
          };
          return question;
      }
    }),
  };

  return payload;
}

function updatePercentage(
  list: Array<IQuestion | IPool>
): Array<IQuestion | IPool> {
  const sum = list.reduce((accumulator, current) => {
    if (current.weight) {
      return accumulator + current.weight;
    }

    return accumulator;
  }, 0);

  const items = list.map((i) => {
    const percentage = (i.weight / sum) * 100;
    return { ...i, percentage: percentage };
  });

  return items;
}

function changeItemWeight(
  state: IActionsStack[number],
  itemId: string,
  weight: number
): IActionsStack[number] {
  const items = state.items.map((i) => {
    if (i.id === itemId) {
      return { ...i, weight: weight };
    }

    return i;
  });

  return {
    ...state,
    items: updatePercentage(items),
    dt_updated: Date.now().toString(),
  };
}

function addItemToQuiz(
  state: IActionsStack[number],
  item: IPool | IQuestion
): IActionsStack[number] {
  const items = [...state.items, item];

  return {
    ...state,
    items: updatePercentage(items),
    dt_updated: Date.now().toString(),
  };
}

function addItemToQuizAtIndex(
  state: IActionsStack[number],
  item: IPool | IQuestion,
  index: number
): IActionsStack[number] {
  const items = [...state.items];
  items.splice(index, 0, item);

  const quizItems = updatePercentage(items);

  return {
    ...state,
    items: quizItems,
    dt_updated: Date.now().toString(),
  };
}

function mergeQuestions(
  state: IActionsStack[number],
  questions: IQuestion[],
  insertionIndex: number
): IActionsStack[number] {
  const items = cloneDeep(state.items);
  let idsToRemove = questions.map((q) => q.id);

  const pool: IPool = {
    id: "new-pool" + Date.now().toString(),
    assessment_id: state.id,
    type: "pool",
    weight: questions[0].weight || 1,
    pick: 1,
    questions: questions,
  };

  // Add the new pool in the list
  items.splice(insertionIndex, 0, pool);

  // Remove merged pools from list
  const newItems = items.filter((item) => !idsToRemove.includes(item.id));

  return {
    ...state,
    items: updatePercentage(newItems),
    dt_updated: Date.now().toString(),
  };
}

function addQuestionToPool(
  state: IActionsStack[number],
  pool_id: string,
  question: IQuestion
): IActionsStack[number] {
  const items = cloneDeep(state.items);
  const newItems = items
    .map((item) => {
      if (item.id === pool_id && item.type === "pool") {
        return { ...item, questions: [...item.questions, question] };
      }
      return item;
    })
    .filter((item) => item.id !== question.id);

  return {
    ...state,
    items: updatePercentage(newItems),
    dt_updated: Date.now().toString(),
  };
}

/**
 * Merge the pools into a new one.
 * It is used when we drag a pool onto another
 *
 * The pools should be destroyed and a new one should be created with question from both of the previous ones
 */
function mergePools(
  state: IActionsStack[number],
  pools: IPool[],
  insertionIndex: number
): IActionsStack[number] {
  const items = cloneDeep(state.items);

  const pool_ids: string[] = pools.map((p) => p.id);
  const questions = pools.reduce<IQuestion[]>((prev, curr) => {
    return [...prev, ...curr.questions];
  }, []);

  const pool: IPool = {
    id: "new-pool" + Date.now().toString(),
    assessment_id: state.id,
    type: "pool",
    weight: 1,
    pick: 1,
    questions: questions,
  };

  items.splice(insertionIndex, 0, pool);
  const newItems = items.filter((item) => !pool_ids.includes(item.id));

  return {
    ...state,
    items: updatePercentage(newItems),
    dt_updated: Date.now().toString(),
  };
}

/**
 * Merge the selected questions and pools
 *
 * There are two types of merge:
 * 1 Pool + N questions; N >= 1 | The original pools is maintained, the selected questions are added in the selected pool and removed from the list
 * X pools + Y questions; X != 1 | A new pool is created with the questions from all selected pools, plus all selected questions.
 * All selected pools/questiosn are removed from the list.
 */
function mergeQuestionsAndPools(
  state: IActionsStack[number],
  ids: string[],
  insertionIndex: number
): IActionsStack[number] {
  const items = cloneDeep(state.items);
  let idsToRemove = [...ids];

  const selectedItems = items.filter((item) => ids.includes(item.id));

  // The number of pools within the provided selected ids
  const poolsCount = selectedItems.filter(
    (item) => item.type === "pool"
  ).length;

  if (poolsCount === 1) {
    // 1 Pool + N questions

    // Select the pool that will have its questions updated
    const poolToUpdate = selectedItems.find(
      (item) => item.type === "pool"
    ) as IPool;

    // Update the pool's questions
    poolToUpdate.questions = [
      ...poolToUpdate.questions,
      ...(selectedItems.filter(
        (item) => item.type === "question"
      ) as IQuestion[]),
    ];

    // Only remove the selected questions, keep the pool
    idsToRemove = selectedItems
      .filter((item) => item.type === "question")
      .map((item) => item.id);
  } else {
    // N pools + X questions N != 1

    // Create new pool with questions and add it to the list
    const pool: IPool = {
      id: "new-pool" + Date.now().toString(),
      assessment_id: state.id,
      type: "pool",
      weight: selectedItems[0].weight || 1,
      pick: 1,
      questions: selectedItems.reduce((prev, cur) => {
        if (cur.type === "question") {
          return [...prev, cur];
        }

        return [...prev, ...cur.questions];
      }, [] as IQuestion[]),
    };

    // Add the new pool in the list
    items.splice(insertionIndex, 0, pool);
  }

  // Remove merged pools from list
  const newItems = items.filter((item) => !idsToRemove.includes(item.id));

  return {
    ...state,
    items: updatePercentage(newItems),
    dt_updated: Date.now().toString(),
  };
}

function removeItemFromQuiz(
  state: IActionsStack[number],
  ids: string[]
): IActionsStack[number] {
  const list = state.items.filter((item) => !ids.includes(item.id)) || [];
  const quizItems = updatePercentage(list);

  return {
    ...state,
    items: quizItems,
    dt_updated: Date.now().toString(),
  };
}

/**
 * Move one question out of a pool of questions, and back to the quiz questions/pools list
 */
function moveQuestionOutOfPool(
  state: IActionsStack[number],
  poolId: string,
  questionIndex: number
): IActionsStack[number] {
  const items = cloneDeep(state.items);

  // Remove the question from the pool, save the question
  const poolIndex = items.findIndex((item) => item.id === poolId);
  const pool = items[poolIndex] as IPool;
  const questionToMove = pool?.questions.splice(questionIndex, 1)[0];

  if (!pool || !questionToMove) {
    throw new Error("Pool or question not found");
  }

  // Check the pool pick, correct it if necessary
  if (pool.pick > pool.questions.length) {
    pool.pick = pool.questions.length;
  }

  // Add the removed question to the main list
  items.splice(poolIndex + 1, 0, { ...questionToMove, weight: 1 });

  return {
    ...state,
    items: updatePercentage(items),
    dt_updated: Date.now().toString(),
    _ignoreEntry: pool.questions.length < 2,
  };
}

function moveAllQuestionsOutOfPool(
  state: IActionsStack[number],
  poolId: string
): IActionsStack[number] {
  const items = cloneDeep(state.items);

  // Remove the question from the pool, save the question
  const poolIndex = items.findIndex((item) => item.id === poolId);
  const pool = items[poolIndex] as IPool;

  if (!pool || !Array.isArray(pool.questions)) {
    throw new Error("Pool or question not found");
  }

  const questionsToRemove = pool.questions
    .filter((item) => !item.deactivated)
    .map((question) => ({ ...question, weight: 1 }));

  // Add the removed question to the main list, maintain deactivated items
  items.splice(poolIndex + 1, 0, ...questionsToRemove);

  if (pool.questions.length === questionsToRemove.length) {
    // If the pool does not contain deactivated items, remove the pool from the item list
    items.splice(poolIndex, 1);
  } else {
    // If not, maintain the deactivated questions in the pool
    pool.questions = pool.questions.filter((item) => Boolean(item.deactivated));
  }

  return {
    ...state,
    items: updatePercentage(items),
    dt_updated: Date.now().toString(),
  };
}

/**
 * Delete one question from a pool
 */
function deleteQuestionFromPool(
  state: IActionsStack[number],
  poolId: string,
  questionIndex: number
): IActionsStack[number] {
  const items = cloneDeep(state.items);

  // Find the pool
  const pool = items.find((item) => item.id === poolId) as IPool;

  if (!pool) {
    throw new Error("Pool or question not found");
  }

  // Remove the question from the pool
  pool.questions.splice(questionIndex, 1);

  // Check the pool pick, correct it if necessary
  if (pool.pick > pool.questions.length) {
    pool.pick = pool.questions.length;
  }

  return {
    ...state,
    items,
    dt_updated: Date.now().toString(),
  };
}

/**
 * Change the number of picked questions from a pool
 */
function changePoolPick(
  state: IActionsStack[number],
  poolId: string,
  pick: number
): IActionsStack[number] {
  const items = cloneDeep(state.items);

  // Find the pool
  const pool = items.find((item) => item.id === poolId) as IPool;

  if (!pool) {
    throw new Error("Pool or question not found");
  }

  pool.pick = pick;

  return {
    ...state,
    items,
    dt_updated: Date.now().toString(),
  };
}

function sendItemToThePreviousPosition(
  state: IActionsStack[number],
  id: string
): IActionsStack[number] {
  const index = state.items.findIndex((item) => item.id === id);
  const questions = [...state.items];
  const question = questions.splice(index, 1)[0];
  questions.splice(index - 1, 0, question);

  return {
    ...state,
    items: [...questions],
    dt_updated: Date.now().toString(),
  };
}

function sendItemToTheNextPosition(
  state: IActionsStack[number],
  id: string
): IActionsStack[number] {
  const index = state.items.findIndex((item) => item.id === id);
  const questions = [...state.items];
  const question = questions.splice(index, 1)[0];
  questions.splice(index + 1, 0, question);

  return {
    ...state,
    items: [...questions],
    dt_updated: Date.now().toString(),
  };
}

function sendItemToTheFirstPosition(
  state: IActionsStack[number],
  id: string
): IActionsStack[number] {
  const index = state.items.findIndex((item) => item.id === id);
  const questions = [...state.items];
  const question = questions.splice(index, 1)[0];

  return {
    ...state,
    items: [question, ...questions],
    dt_updated: Date.now().toString(),
  };
}

function sendItemToTheLastPosition(
  state: IActionsStack[number],
  id: string
): IActionsStack[number] {
  const index = state.items.findIndex((item) => item.id === id);
  const questions = [...state.items];
  const question = questions.splice(index, 1)[0];

  return {
    ...state,
    items: [...questions, question],
    dt_updated: Date.now().toString(),
  };
}

export {
  addItemToQuiz,
  addItemToQuizAtIndex,
  addQuestionToPool,
  changeItemWeight,
  getChangesFromState,
  initializeReducer,
  mergePools,
  mergeQuestions,
  mergeQuestionsAndPools,
  listInitialQuizItems,
  removeItemFromQuiz,
  deleteQuestionFromPool,
  changePoolPick,
  moveQuestionOutOfPool,
  moveAllQuestionsOutOfPool,
  sendItemToTheFirstPosition,
  sendItemToTheLastPosition,
  sendItemToTheNextPosition,
  sendItemToThePreviousPosition,
};
