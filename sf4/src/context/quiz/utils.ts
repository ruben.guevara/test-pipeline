import { RouterOutputs } from "$lib/api";
import { IActionsStack, IPool, IQuestion, IQuiz } from "./types";

type DataChanges = null | {
  questions?: { poolId: null | string; ids: string[] };
  poolPick?: { poolId: string };
};

type Changes = {
  questions?: { poolId: null | string; ids: string[] };
  poolPick?: { poolId: string };
};

function findElementsToHighlight(
  prevState: IActionsStack[number],
  state: IActionsStack[number],
  poolDetailsOpenedId: string | null
): DataChanges {
  const changes = calculateChanges(prevState, state);

  /**
   * Check if questions added to the list are from an opened pool
   */
  if (!!poolDetailsOpenedId && changes?.questions?.poolId === null) {
    const prevPool = prevState.items.find(
      (item) => item.id === poolDetailsOpenedId
    ) as IPool;
    const currentPool = state.items.find(
      (item) => item.id === poolDetailsOpenedId
    ) as IPool;

    if (
      !!prevPool &&
      !!currentPool &&
      prevPool.questions?.length !== currentPool.questions.length
    ) {
      delete changes.questions;
    }
  }

  // Clean the result
  if (Object.keys(changes).length === 0) {
    return null;
  }

  return changes;
}

function calculateChanges(prevData: IQuiz, currentData: IQuiz): Changes {
  const res: Changes = {};

  // Check new items in quiz base list
  // const baseListIdsDiff = currentData.items.map(item => item.id).filter((el) => prevData.)
  const baseListIdsDiff = getNewItems(
    prevData.items.map((item) => item.id),
    currentData.items.map((item) => item.id)
  );
  if (baseListIdsDiff.length > 0) {
    res.questions = {
      poolId: null,
      ids: baseListIdsDiff,
    };
  }

  // Veerify the differences of each pool
  for (const item of currentData.items) {
    if (item.type === "pool") {
      const prevPool = prevData.items.find((e) => e.id === item.id) as IPool;

      if (Array.isArray(prevPool?.questions)) {
        // Check for pool pick change.
        // Start by checking if the previous data has the pool, then check for difference in value
        if (prevPool.pick !== item.pick) {
          res.poolPick = { poolId: item.id };
        }

        const poolIdsDiff = getNewItems(
          prevPool.questions.map((question) => question.id),
          item.questions.map((question) => question.id)
        );
        if (poolIdsDiff.length > 0) {
          res.questions = {
            poolId: item.id,
            ids: poolIdsDiff,
          };
        }
      }
    }
  }

  return res;
}

function getNewItems(prevArr: string[], currArr: string[]) {
  return currArr.filter((el) => !prevArr.includes(el));
}

/**
 * Sort the questions to be shown in a pool
 */
function sortPoolQuestions(questions: IQuestion[]) {
  if (!Array.isArray(questions)) {
    return [];
  }

  return questions.sort((a, b) => {
    if (a.deactivated) {
      return -1;
    }

    if (b.deactivated) {
      return 1;
    }

    return a.title < b.title ? -1 : a.title > b.title ? 1 : 0;
  });
}

function mapDeactivatedQuestions(
  quiz: IQuiz,
  learnosityItems: RouterOutputs["question"]["findSelected"]
): IQuiz {
  const checkIfExists = (reference: string) => {
    return (
      learnosityItems.findIndex((item) => item.reference === reference) !== -1
    );
  };

  const items = quiz.items.map((item) => {
    if (item.type === "question" && !checkIfExists(item.id)) {
      return { ...item, deactivated: true };
    }

    if (item.type === "pool") {
      return {
        ...item,
        questions: item.questions.map((poolItem) => {
          if (!checkIfExists(poolItem.id)) {
            return { ...poolItem, deactivated: true };
          }

          return poolItem;
        }),
      };
    }

    return item;
  });

  return {
    ...quiz,
    items,
  };
}

export { findElementsToHighlight, sortPoolQuestions, mapDeactivatedQuestions };
