import { useElementsHighlight } from "$components/quiz/hooks/useElementsHighlight";
import { api } from "$lib/api";
import {
  Dispatch,
  ReactNode,
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useReducer,
  useState,
} from "react";
import { useLocalStorage } from "../../hooks/useLocalStorage";
import { usePrevious } from "../../hooks/usePrevious";
import { actionStackReducer } from "./actionStackReducer";
import { initializeReducer } from "./reducerFunctions";
import { IQuiz, IReducerAction } from "./types";
import { findElementsToHighlight } from "./utils";

interface ProviderProps {
  initialData: IQuiz;
  children: ReactNode;
}

type QuizContextProps = {
  data: IQuiz;
  dispatch: Dispatch<IReducerAction>;
  loadingItems: boolean;
  poolDetailsOpenedId: string | null;
  openPoolDetails: (id: string) => void;
  closePoolDetails: () => void;
  questionsToHighlight: string[];
  poolPickToHighlight: { poolId: string | null; inputKeyValue: number };
  canUndo: boolean;
  canRedo: boolean;
  undo: () => void;
  redo: () => void;
  selected: string[];
  allItemsSelected: boolean;
  onSelectItem: (item: string) => void;
  onRemoveItems: (items: string[], removingEmptyPool?: boolean) => void;
  onSelectAll: () => void;
  mergeSelected: () => void;
};

const QuizContext = createContext({} as QuizContextProps);
QuizContext.displayName = "QuizContext";

const useQuiz = () => {
  return useContext(QuizContext);
};

function QuizContextProvider({ children, initialData }: ProviderProps) {
  const [selected, setSelected] = useState<string[]>([]);
  const [poolDetailsOpenedId, setPoolDetailsOpenedId] =
    useState<QuizContextProps["poolDetailsOpenedId"]>(null);
  const [saved] = useLocalStorage<IQuiz | null>("quizbuilder-data", null);
  const {
    questionsToHighlight,
    addQuestionsToHighlight,
    poolPickToHighlight,
    addPoolPicksToHighlight,
    clearPoolPickToHighlight,
  } = useElementsHighlight();

  const initializerProps = {
    db: initialData,
    storage: saved,
  };

  const [state, dispatch] = useReducer(
    actionStackReducer,
    initializerProps,
    initializeReducer
  );

  const data = useMemo(() => {
    return state.actionsStack[state.pointer];
  }, [state]);
  const prevState = usePrevious(data);

  const { canUndo, canRedo } = useMemo(() => {
    return {
      canUndo: state.pointer > 0,
      canRedo: state.pointer < state.actionsStack.length - 1,
    };
  }, [state]);

  const { data: learnosityItems } = api.question.findSelected.useQuery(
    state.referencesToVerify || [],
    {
      enabled:
        Array.isArray(state.referencesToVerify) &&
        state.referencesToVerify.length > 0,
    }
  );

  // Verify if the questions loaded from localStorage are deactivated
  useEffect(() => {
    if (learnosityItems) {
      dispatch({
        type: "verify_deactivated_questions",
        payload: learnosityItems,
      });
    }
  }, [learnosityItems]);

  // Handle highlighting of elements
  useEffect(() => {
    if (state.direction === "new") {
      return;
    }

    const elementsToHighlight = findElementsToHighlight(
      prevState,
      data,
      poolDetailsOpenedId
    );
    if (!elementsToHighlight) {
      return;
    }

    if (elementsToHighlight.questions) {
      addQuestionsToHighlight(
        (elementsToHighlight.questions as Record<string, any>).ids
      );
      setPoolDetailsOpenedId(elementsToHighlight.questions.poolId);
    } else if (elementsToHighlight.poolPick) {
      // TODO: Highlight the pool pick input
      addPoolPicksToHighlight(elementsToHighlight.poolPick.poolId);
      setPoolDetailsOpenedId(elementsToHighlight.poolPick.poolId);
    }
  }, [
    addQuestionsToHighlight,
    addPoolPicksToHighlight,
    data,
    poolDetailsOpenedId,
    prevState,
    state.direction,
  ]);

  const flattenQuestionsToHighlight = useMemo(() => {
    return questionsToHighlight.flat();
  }, [questionsToHighlight]);

  const allItemsSelected = useMemo(() => {
    const selectableItemsCount = data.items.filter((item) => {
      if (item.type === "question") {
        return !item.deactivated;
      }

      return true;
    }).length;

    return selected.length > 0 && selected.length === selectableItemsCount;
  }, [data, selected]);

  const loadingItems = useMemo(() => {
    return (
      Array.isArray(state.referencesToVerify) &&
      state.referencesToVerify.length > 0
    );
  }, [state.referencesToVerify]);

  const onSelectItem = (item: string) => {
    if (selected.includes(item)) {
      setSelected((current) => current.filter((i) => i !== item) || []);
    } else {
      setSelected((current) => [...current, item]);
    }
  };

  const onSelectAll = () => {
    if (allItemsSelected) {
      setSelected([]);
    } else {
      setSelected(() => {
        return data.items
          .filter((item) => {
            if (item.type === "question") {
              return !item.deactivated;
            }

            return true;
          })
          .map((item) => item.id);
      });
    }
  };

  const onRemoveItems = useCallback((items: string[]) => {
    dispatch({
      type: "remove_item_from_quiz",
      payload: items,
    });
    setSelected((current) => {
      return current.filter((item) => !items.includes(item)) || [];
    });
  }, []);

  const handleUndoAction = useCallback(() => {
    if (canUndo) {
      dispatch({ type: "undo" });
    }
  }, [canUndo]);

  const handleRedoAction = useCallback(() => {
    if (canRedo) {
      dispatch({ type: "redo" });
    }
  }, [canRedo]);

  const handleMergeSelected = useCallback(() => {
    // Must have at least two selected items to merge
    if (selected.length < 2) {
      return;
    }

    const index = data.items.findIndex((item) => item.id === selected[0]);
    dispatch({
      type: "merge_questions_and_pools",
      payload: {
        index: index,
        ids: selected,
      },
    });

    setSelected([]);
  }, [selected, data]);

  const handleOpenPoolDetails = useCallback((id: string) => {
    setPoolDetailsOpenedId(id);

    // Clear the selected array on opening pool details, to prevent errors
    setSelected([]);
  }, []);

  const handleClosePoolDetails = useCallback(() => {
    setPoolDetailsOpenedId(null);
    clearPoolPickToHighlight();
    dispatch({
      type: "close_opened_pool_details",
      payload: poolDetailsOpenedId as string,
    });
  }, [poolDetailsOpenedId, clearPoolPickToHighlight]);

  return (
    <QuizContext.Provider
      value={{
        data,
        selected,
        loadingItems,
        allItemsSelected,
        poolDetailsOpenedId,
        openPoolDetails: handleOpenPoolDetails,
        closePoolDetails: handleClosePoolDetails,
        questionsToHighlight: flattenQuestionsToHighlight,
        poolPickToHighlight,
        onSelectItem: onSelectItem,
        onSelectAll,
        dispatch,
        undo: handleUndoAction,
        redo: handleRedoAction,
        canUndo,
        canRedo,
        mergeSelected: handleMergeSelected,
        onRemoveItems,
      }}
    >
      {children}
    </QuizContext.Provider>
  );
}

export { QuizContext, QuizContextProvider, useQuiz };
