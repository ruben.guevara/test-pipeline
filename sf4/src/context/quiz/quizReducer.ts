import {
  addItemToQuiz,
  addItemToQuizAtIndex,
  addQuestionToPool,
  changeItemWeight,
  changePoolPick,
  deleteQuestionFromPool,
  mergePools,
  mergeQuestions,
  mergeQuestionsAndPools,
  moveAllQuestionsOutOfPool,
  moveQuestionOutOfPool,
  removeItemFromQuiz,
  sendItemToTheFirstPosition,
  sendItemToTheLastPosition,
  sendItemToTheNextPosition,
  sendItemToThePreviousPosition,
} from "./reducerFunctions";
import { IActionsStack, IReducerAction } from "./types";

function quizReducer(
  state: IActionsStack[number],
  action: IReducerAction
): IActionsStack[number] {
  switch (action.type) {
    case "change_item_weight":
      return changeItemWeight(
        state,
        action.payload.itemId,
        action.payload.weight
      );

    case "add_item_to_quiz":
      return addItemToQuiz(state, action.payload);

    case "add_item_to_quiz_at_index":
      return addItemToQuizAtIndex(
        state,
        action.payload.item,
        action.payload.index
      );

    case "add_question_to_pool":
      return addQuestionToPool(
        state,
        action.payload.pool_id,
        action.payload.question
      );

    case "change_quiz_name":
      return {
        ...state,
        name: action.payload,
        dt_updated: Date.now().toString(),
      };

    case "drop_item_on_quiz":
      return {
        ...state,
        items: action.payload,
        dt_updated: Date.now().toString(),
      };

    case "merge_pools":
      return mergePools(
        state,
        action.payload.pools,
        action.payload.insertionIndex
      );

    case "merge_questions":
      return mergeQuestions(
        state,
        action.payload.questions,
        action.payload.index
      );

    case "merge_questions_and_pools":
      return mergeQuestionsAndPools(
        state,
        action.payload.ids,
        action.payload.index
      );

    case "remove_item_from_quiz":
      return removeItemFromQuiz(state, action.payload);

    case "move_question_out_of_pool":
      return moveQuestionOutOfPool(
        state,
        action.payload.poolId,
        action.payload.questionIndex
      );

    case "move_all_questions_out_of_pool":
      return moveAllQuestionsOutOfPool(state, action.payload);

    case "delete_question_from_pool":
      return deleteQuestionFromPool(
        state,
        action.payload.poolId,
        action.payload.questionIndex
      );

    case "change_pool_pick":
      return changePoolPick(state, action.payload.poolId, action.payload.pick);

    case "send_item_up":
      return sendItemToThePreviousPosition(state, action.payload);

    case "send_item_down":
      return sendItemToTheNextPosition(state, action.payload);

    case "send_item_to_first_position":
      return sendItemToTheFirstPosition(state, action.payload);

    case "send_item_to_last_position":
      return sendItemToTheLastPosition(state, action.payload);

    default:
      return state;
  }
}

export { quizReducer };
