import type { RouterOutputs } from "$lib/api";
import { learnosityItem } from "$server/api/schemas";
import { z } from "zod";

type LearnosityItem = z.infer<typeof learnosityItem>;

type IReducerState = {
  actionsStack: IActionsStack;
  pointer: number;
  direction: "undo" | "redo" | "new";
  referencesToVerify?: string[];
};

interface IAssessment {
  assessment_id: number;
  items: Array<IAssessmentQuestion | IAssessmentPool>;
  dt_published?: string;
}

interface IAssessmentPool {
  pool_id?: number;
  num_to_select: number;
  sort_order: number;
  weight: number;
  questions: Array<{
    id?: number;
    item_reference: string;
  }>;
}

interface IAssessmentQuestion {
  id?: number;
  item_reference: string;
  sort_order: number;
  weight: number;
}

interface IAssessmentItem {
  id: string;
  type: "pool" | "question";
  weight: number;
  index: number;
}

interface IAssessmentMeta {
  chapter: IUnit | null;
  course: IUnit;
  region: IUnit;
}

interface IPool {
  id: string;
  pool_id?: number;
  type: "pool";
  assessment_id: number;
  weight: number;
  percentage?: number;
  pick: number;
  questions: IQuestion[];
}

interface IQuiz {
  id: number;
  name: string;
  parent_id: number;
  link_active: boolean;
  meta: IAssessmentMeta;
  items: Array<IQuestion | IPool>;
  dt_updated?: string;
}

type IActionsStack = Array<IQuiz & { _ignoreEntry?: boolean }>;

export type IQuestion = {
  id: string;
  question_id?: number;
  weight: number;
  percentage?: number;
  type: "question";
  title: string;
  status: string;
  description: string | null;
  questions: {
    data: Array<{
      type: string;
      reference: string;
      widget_type: string;
      data: {
        type: string;
        stimulus?: string;
        options?: Array<{
          label: string;
          value: string | number;
        }>;
        validation?: {
          max_score?: number;
          valid_response?: {
            score: number;
            value: Array<string | number> | string;
          };
        };
      };
    }>;
  };
  deactivated?: boolean;
};

interface IUnit {
  id: number;
  name: string;
}

type IReducerAction =
  | { type: "undo" }
  | { type: "redo" }
  | {
      type: "verify_deactivated_questions";
      payload: RouterOutputs["question"]["findSelected"];
    }
  | { type: "add_item_to_quiz"; payload: IQuestion | IPool }
  | {
      type: "add_item_to_quiz_at_index";
      payload: { item: IQuestion | IPool; index: number };
    }
  | {
      type: "add_question_to_pool";
      payload: { pool_id: string; question: IQuestion };
    }
  | {
      type: "change_item_weight";
      payload: { itemId: string; weight: number };
    }
  | { type: "change_quiz_name"; payload: string }
  | { type: "drop_item_on_quiz"; payload: Array<IQuestion | IPool> }
  | { type: "merge_pools"; payload: { pools: IPool[]; insertionIndex: number } }
  | {
      type: "merge_questions_and_pools";
      payload: { ids: string[]; index: number };
    }
  | {
      type: "merge_questions";
      payload: { questions: IQuestion[]; index: number };
    }
  | { type: "remove_item_from_quiz"; payload: string[] }
  | {
      type: "delete_question_from_pool";
      payload: { poolId: string; questionIndex: number };
    }
  | { type: "change_pool_pick"; payload: { poolId: string; pick: number } }
  | {
      type: "move_question_out_of_pool";
      payload: { poolId: string; questionIndex: number };
    }
  | { type: "move_all_questions_out_of_pool"; payload: string }
  | { type: "send_item_to_first_position"; payload: string }
  | { type: "send_item_to_last_position"; payload: string }
  | { type: "send_item_up"; payload: string }
  | { type: "send_item_down"; payload: string }
  | {
      type: "set_item_percentage";
      payload: { itemId: string; percentage: number };
    }
  | { type: "close_opened_pool_details"; payload: string };

export type {
  IReducerState,
  IActionsStack,
  IAssessment,
  IAssessmentMeta,
  IAssessmentPool,
  IAssessmentQuestion,
  IPool,
  IQuiz,
  IReducerAction,
  LearnosityItem,
};
