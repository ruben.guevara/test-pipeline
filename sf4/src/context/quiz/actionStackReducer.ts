import { quizReducer } from "./quizReducer";
import {
  IActionsStack,
  IPool,
  IQuiz,
  IReducerAction,
  IReducerState,
} from "./types";
import { mapDeactivatedQuestions } from "./utils";

function actionStackReducer(
  state: IReducerState,
  action: IReducerAction
): IReducerState {
  switch (action.type) {
    case "undo":
      // Can't UNDO if there are no previous actions
      if (state.pointer === 0) {
        console.error("Unavailable UNDO action");
        return state;
      }

      // Save to local storage
      saveToLocalStorage(state.actionsStack[state.pointer - 1]);

      return {
        actionsStack: state.actionsStack,
        pointer: state.pointer - 1,
        direction: "undo",
      };

    case "redo":
      // Can't REDO if there are no further records
      if (state.pointer === state.actionsStack.length - 1) {
        console.error("Unavailable REDO action");
        return state;
      }

      // Save to local storage
      saveToLocalStorage(state.actionsStack[state.pointer + 1]);

      return {
        actionsStack: state.actionsStack,
        pointer: state.pointer + 1,
        direction: "redo",
      };

    case "close_opened_pool_details":
      // Verify if the pool has less than two questions
      // If so, remove the pool from the items, and clear any ignorable entries present on the stack
      const data = {
        ...state.actionsStack[state.pointer],
        _ignoreEntry: false,
      };
      const pool = data.items.find(
        (item) => item.id === action.payload
      ) as IPool;

      if (
        pool?.questions.length < 2 &&
        pool?.questions.findIndex((item) => !!item.deactivated) === -1
      ) {
        const newStack = quizReducer(data, {
          type: "move_all_questions_out_of_pool",
          payload: action.payload,
        });

        saveToLocalStorage(newStack);

        const updatedStack: IActionsStack = [
          ...state.actionsStack.slice(0, state.pointer + 1),
          newStack,
        ].filter((entry) => !entry._ignoreEntry);

        return {
          actionsStack: updatedStack,
          pointer: updatedStack.length - 1,
          direction: "new",
        };
      } else {
        // Make sure to filter ignored entries every time the pool details is closed
        return {
          actionsStack: state.actionsStack.filter(
            (entry) => !entry._ignoreEntry
          ),
          pointer: state.pointer,
          direction: "new",
        };
      }

    case "verify_deactivated_questions":
      return {
        ...state,
        actionsStack: [
          mapDeactivatedQuestions(state.actionsStack[0], action.payload),
        ],
        referencesToVerify: [],
      };

    default:
      const newStack: IActionsStack[number] = quizReducer(
        { ...state.actionsStack[state.pointer], _ignoreEntry: false },
        action
      );
      const updatedStack: IActionsStack = [
        ...state.actionsStack.slice(0, state.pointer + 1),
        newStack,
      ];

      if (!newStack._ignoreEntry) {
        saveToLocalStorage(newStack);
      }

      return {
        actionsStack: updatedStack,
        pointer: updatedStack.length - 1,
        direction: "new",
      };
  }
}

function saveToLocalStorage(value: IQuiz) {
  if (typeof window !== "undefined") {
    localStorage.setItem("@studyforge:quizbuilder-data", JSON.stringify(value));
  }
}

export { actionStackReducer };
