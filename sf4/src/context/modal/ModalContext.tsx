import {
  DialogProps,
  FlyoutProps,
  Modal,
  ModalProps,
  PopUpProps,
} from "$components/shared/Modal";
import { ReactNode, createContext, useContext, useState } from "react";
import { v4 as uuidv4 } from "uuid";

interface ProviderProps {
  children: ReactNode;
}

type IModal = DialogProps | FlyoutProps | PopUpProps;

type ModalContextProps = {
  flyout: (item: Omit<FlyoutProps, "type">) => void;
  dialog: (item: Omit<DialogProps, "type">) => void;
  popup: (item: Omit<PopUpProps, "type">) => void;
  remove: (id: string) => void;
};

const ModalContext = createContext({} as ModalContextProps);

const useModal = () => {
  return useContext(ModalContext);
};

function ModalContextProvider({ children }: ProviderProps) {
  const [modals, setModals] = useState<ModalProps[]>([]);

  function createFlyout(item: Omit<FlyoutProps, "type">) {
    const payload: IModal & { id: string } = {
      ...item,
      id: uuidv4(),
      type: "flyout",
    };
    setModals((current) => [...current, payload]);
  }

  function createDialog(item: Omit<DialogProps, "type">) {
    const payload: IModal & { id: string } = {
      ...item,
      id: uuidv4(),
      type: "dialog",
    };
    setModals((current) => [...current, payload]);
  }

  function createPopup(item: Omit<PopUpProps, "type">) {
    const payload: IModal & { id: string } = {
      ...item,
      id: uuidv4(),
      type: "container",
    };
    setModals((current) => [...current, payload]);
  }

  function remove(id: string) {
    setModals((current) => {
      const list = current.filter((item) => item.id !== id) || [];
      return list;
    });
  }

  return (
    <ModalContext.Provider
      value={{
        flyout: createFlyout,
        dialog: createDialog,
        popup: createPopup,
        remove,
      }}
    >
      {children}
      {!!modals.length && (
        <div className="w-screen h-screen fixed inset-0 z-50 bg-slate-900 opacity-80"></div>
      )}
      {modals.map((modal) => (
        <Modal key={modal.id} {...modal} />
      ))}
    </ModalContext.Provider>
  );
}

export { ModalContextProvider, useModal };
