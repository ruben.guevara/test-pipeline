export const config = {
  app: {
    publicAppUrl: new URL(process.env.NEXT_PUBLIC_APP_URL ?? 'localhost'),
  },
  api: {
    learnosity: {
      consumerKey: process.env.LEARNOSITY_CLIENT_ID ?? '',
      secret: process.env.LEARNOSITY_SECRET ?? '',
      version: process.env.LEARNOSITY_API_VERSION ?? 'v2023.1.LTS',
    }
  }
}