// turns a type into a compiled type that is better for inference
type Id<T> = T extends infer U ? { [K in keyof U]: U[K] } : never;

// gets all keys of a type
type AllKeys<T> = T extends unknown ? keyof T : never;

// intermediate step for Xor
type _ExclusifyUnion<T, K extends PropertyKey> = T extends unknown
  ? Id<T & Partial<Record<Exclude<K, keyof T>, never>>>
  : never;

// turns a union type into an intersection type
type Xor<T> = _ExclusifyUnion<T, AllKeys<T>>;

// if one of these in the union is present then they all must be
type AllOrNone<T> = T | { [K in keyof T]?: never };

// expands object types one level deep
type Expand<T> = T extends infer O ? { [K in keyof O]: O[K] } : never;

// expands object types recursively
type ExpandRecursively<T> = T extends object
  ? T extends infer O
    ? { [K in keyof O]: ExpandRecursively<O[K]> }
    : never
  : T;

// specify which properties of a type are required
type Require<T, K extends keyof T> = Omit<T, K> & Required<Pick<T, K>>;

// an object with an id property
type Entity<IdType extends string | number | Symbol = number> = {
  id: IdType;
  [key: string]: any;
};

type NotNull<T> = T extends null | undefined ? never : T;

type NestedNotNull<T> = T extends object
  ? { [K in keyof T]: NotNull<T[K]> }
  : T;

type ArrayElement<ArrayType> = ArrayType extends readonly (infer ElementType)[]
  ? ElementType
  : never;

// require an id of an entity but make all other properties optional
type RequireId<T extends { id: any }> = Require<Partial<T>, "id">;

// Excludes properties from a type that match a given type even if that property is only unioned with that type
type ExcludeMatchingProperties<T, V> = Pick<
  T,
  { [K in keyof T]-?: Extract<T[K], V> extends never ? K : never }[keyof T]
>;

// Used for Prisma generate "Select" or "Include" types to get only the properties that exist in that table
type OmitRelations<T> = ExcludeMatchingProperties<
  T,
  { select?: any; include?: any }
>;

// An Array with at least one element of type T
type ArrayAtLeastOne<T> = Readonly<[T] | [T, ...T[]]>;

type Optional<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;

type Resolved<T> = T extends Promise<infer R> ? R : never;

type ElementType<T extends ReadonlyArray<unknown>> = T extends ReadonlyArray<
  infer ElementType
>
  ? ElementType
  : never;

type KeyedBooleansFromArray<T extends ReadonlyArray<string | number>> = {
  [k in ElementType<T>]: boolean;
};

type AbsoluteUrl = `https://${string}` | `http://${string}`;
