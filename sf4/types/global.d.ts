interface Window {
  LearnosityAuthor?: {
    init: (config: any) => LernosityAuthorApp;
  };
}

type LernosityAuthorApp = {
  on: (event: string, cb: (...args: any) => void) => void;
  getItemList: () => Array<{
    note: string | null;
    dt_updated: string;
    item_id: string;
    dt_created: string;
    title: string;
    created_by: Array<any>;
    tags: Array<{
      name: string;
      type: string;
    }>;
    reference: string;
    authoring_workflow: {
      state: string;
    };
    status: "published" | "unpublished" | "archived" | false;
    organisation_id: number;
    content: string;
  }>;
  getLocation: () => {
    location: string;
    locationEncoded: string;
    route: string;
  };
  fetchItems: () => Promise<Array<any>>;
  destroy: () => void;
};
