import "@tanstack/react-table";

declare module "@tanstack/table-core" {
  interface ColumnMeta<TData extends RowData, TValue> {
    type?: "string" | "number" | "id" | "name" | "date" | "action";
    freeze?: boolean;
  }
}
