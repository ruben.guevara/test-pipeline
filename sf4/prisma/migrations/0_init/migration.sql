-- CreateTable
CREATE TABLE `sf_country` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,

    INDEX `IDX_939459B116FE72E1`(`updated_by`),
    INDEX `IDX_939459B1DE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_partner` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `region_id` INTEGER NULL,
    `description` VARCHAR(255) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,

    INDEX `IDX_F1CCAEC116FE72E1`(`updated_by`),
    INDEX `IDX_F1CCAEC198260155`(`region_id`),
    INDEX `IDX_F1CCAEC1DE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_region` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `country_id` INTEGER NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,

    INDEX `IDX_529E8B0916FE72E1`(`updated_by`),
    INDEX `IDX_529E8B09DE12AB56`(`created_by`),
    INDEX `IDX_529E8B09F92F3E70`(`country_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `af_api_token` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `access_token` VARCHAR(255) NOT NULL,
    `refresh_token` VARCHAR(255) NOT NULL,
    `expires_at` DATETIME(0) NOT NULL,
    `session_id` VARCHAR(255) NOT NULL,

    INDEX `IDX_A5F786F3A76ED395`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_author` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `context_id` INTEGER NULL,
    `region_id` INTEGER NULL,
    `user_id` INTEGER NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,
    `christian` BOOLEAN NOT NULL DEFAULT false,

    UNIQUE INDEX `UNIQ_E053A2B76B00C1CF`(`context_id`),
    INDEX `IDX_E053A2B716FE72E1`(`updated_by`),
    INDEX `IDX_E053A2B798260155`(`region_id`),
    INDEX `IDX_E053A2B7A76ED395`(`user_id`),
    INDEX `IDX_E053A2B7DE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_author_portion` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `author_id` INTEGER NULL,
    `portion` DECIMAL(10, 3) NOT NULL,
    `entity_type` VARCHAR(255) NOT NULL,
    `entity_id` INTEGER NULL,
    `item_reference` VARCHAR(255) NULL,

    INDEX `IDX_3A01BC2481257D5D`(`entity_id`),
    INDEX `IDX_3A01BC24F675F31B`(`author_id`),
    INDEX `item_ref_idx`(`item_reference`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_bible_version` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `abbreviation` VARCHAR(255) NOT NULL,
    `copyright` LONGTEXT NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,

    INDEX `IDX_ABFB797316FE72E1`(`updated_by`),
    INDEX `IDX_ABFB7973DE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_chapter` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `course_id` INTEGER NULL,
    `description` LONGTEXT NULL,
    `number` SMALLINT NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,
    `video_plate_file_id` INTEGER NULL,
    `lesson_page_css` LONGTEXT NULL,

    INDEX `IDX_396625F916FE72E1`(`updated_by`),
    INDEX `IDX_396625F9591CC992`(`course_id`),
    INDEX `IDX_396625F9690FD836`(`video_plate_file_id`),
    INDEX `IDX_396625F9DE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_chapter_progress` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `chapter_id` INTEGER NULL,
    `video_watched_percentage` DECIMAL(10, 3) NOT NULL,
    `question_green_percentage` DECIMAL(10, 3) NOT NULL,
    `question_red_percentage` DECIMAL(10, 3) NOT NULL,
    `question_yellow_percentage` DECIMAL(10, 3) NOT NULL,
    `date_first_viewed` DATETIME(0) NULL,
    `date_last_viewed` DATETIME(0) NULL,
    `date_last_updated` DATETIME(0) NULL,

    INDEX `IDX_4C11816F579F4768`(`chapter_id`),
    INDEX `IDX_4C11816FA76ED395`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_compiled_css` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `reading_id` INTEGER NULL,
    `interactive_id` INTEGER NULL,
    `assessment_id` INTEGER NULL,
    `css` LONGTEXT NOT NULL,
    `css_theme_id` INTEGER NULL,

    UNIQUE INDEX `UNIQ_7195B7A1527275CD`(`reading_id`),
    UNIQUE INDEX `UNIQ_7195B7A1329C9C7D`(`interactive_id`),
    UNIQUE INDEX `UNIQ_7195B7A1DD3DD5F1`(`assessment_id`),
    UNIQUE INDEX `UNIQ_7195B7A1CF6C57B7`(`css_theme_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_context` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `parent_id` INTEGER NULL,
    `lft` INTEGER NOT NULL,
    `rgt` INTEGER NOT NULL,
    `partner_id` INTEGER NULL,
    `institution_id` INTEGER NULL,
    `group_id` INTEGER NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `name` VARCHAR(255) NULL,

    UNIQUE INDEX `UNIQ_22BA15A99393F8FE`(`partner_id`),
    UNIQUE INDEX `UNIQ_22BA15A910405986`(`institution_id`),
    UNIQUE INDEX `UNIQ_22BA15A9FE54D947`(`group_id`),
    INDEX `IDX_22BA15A916FE72E1`(`updated_by`),
    INDEX `IDX_22BA15A9727ACA70`(`parent_id`),
    INDEX `IDX_22BA15A9DE12AB56`(`created_by`),
    INDEX `left_idx`(`lft`),
    INDEX `right_idx`(`rgt`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_course` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `region_id` INTEGER NULL,
    `header_image_id` INTEGER NULL,
    `code` VARCHAR(20) NOT NULL,
    `course_type` INTEGER NOT NULL,
    `description` LONGTEXT NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `subject_area` INTEGER NULL,
    `chapter_label` VARCHAR(255) NOT NULL DEFAULT 'Chapter',
    `credit_type_id` INTEGER NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `continuous_lesson_numbering` BOOLEAN NOT NULL DEFAULT false,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,
    `current` BOOLEAN NOT NULL DEFAULT false,
    `grade_level` INTEGER NOT NULL DEFAULT 0,
    `video_plate_file_id` INTEGER NULL,
    `lesson_page_css` LONGTEXT NULL,
    `prerequisite_id` INTEGER NULL,
    `course_stream_name` VARCHAR(255) NULL,
    `lft` INTEGER NULL,
    `rgt` INTEGER NULL,
    `license_amount` DECIMAL(10, 3) NOT NULL DEFAULT 1.000,

    UNIQUE INDEX `UNIQ_4B6215C68C782417`(`header_image_id`),
    INDEX `IDX_4B6215C616FE72E1`(`updated_by`),
    INDEX `IDX_4B6215C6276AF86B`(`prerequisite_id`),
    INDEX `IDX_4B6215C6690FD836`(`video_plate_file_id`),
    INDEX `IDX_4B6215C698260155`(`region_id`),
    INDEX `IDX_4B6215C6DE12AB56`(`created_by`),
    INDEX `IDX_4B6215C6FFC8EBBC`(`credit_type_id`),
    INDEX `left_idx`(`lft`),
    INDEX `right_idx`(`rgt`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_course_context` (
    `course_id` INTEGER NOT NULL,
    `context_id` INTEGER NOT NULL,

    INDEX `IDX_75255ED3591CC992`(`course_id`),
    INDEX `IDX_75255ED36B00C1CF`(`context_id`),
    PRIMARY KEY (`course_id`, `context_id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_course_editor` (
    `course_id` INTEGER NOT NULL,
    `user_id` INTEGER NOT NULL,

    INDEX `IDX_66F3D4B1591CC992`(`course_id`),
    INDEX `IDX_66F3D4B1A76ED395`(`user_id`),
    PRIMARY KEY (`course_id`, `user_id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_course_equivalent` (
    `equivalent_id` INTEGER NOT NULL,
    `course_id` INTEGER NOT NULL,

    INDEX `IDX_CE63CF35591CC992`(`course_id`),
    INDEX `IDX_CE63CF35D1487398`(`equivalent_id`),
    PRIMARY KEY (`equivalent_id`, `course_id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_course_progress` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `course_id` INTEGER NULL,
    `video_watched_percentage` DECIMAL(10, 3) NOT NULL,
    `question_green_percentage` DECIMAL(10, 3) NOT NULL,
    `question_red_percentage` DECIMAL(10, 3) NOT NULL,
    `question_yellow_percentage` DECIMAL(10, 3) NOT NULL,
    `date_first_viewed` DATETIME(0) NULL,
    `date_last_viewed` DATETIME(0) NULL,
    `date_last_updated` DATETIME(0) NULL,

    INDEX `IDX_8AF155C8591CC992`(`course_id`),
    INDEX `IDX_8AF155C8A76ED395`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_credit_transaction` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `institution_id` INTEGER NULL,
    `user_by` INTEGER NULL,
    `user_for` INTEGER NULL,
    `type` INTEGER NOT NULL,
    `price` DECIMAL(10, 2) NOT NULL,
    `credits` DECIMAL(10, 3) NOT NULL,
    `credits_left` DECIMAL(10, 3) NOT NULL,
    `balance` DECIMAL(10, 3) NOT NULL,
    `description` LONGTEXT NOT NULL,
    `date` DATETIME(0) NOT NULL,
    `credit_type_id` INTEGER NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,
    `group_id` INTEGER NULL,

    INDEX `IDX_9D233D2510405986`(`institution_id`),
    INDEX `IDX_9D233D25279C6687`(`user_by`),
    INDEX `IDX_9D233D254636873`(`user_for`),
    INDEX `IDX_9D233D25FE54D947`(`group_id`),
    INDEX `IDX_9D233D25FFC8EBBC`(`credit_type_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_credit_type` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,

    INDEX `IDX_47D7765F16FE72E1`(`updated_by`),
    INDEX `IDX_47D7765FDE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_css_theme` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `targets` LONGTEXT NOT NULL,
    `css` LONGTEXT NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `date_compiled` DATETIME(0) NULL,
    `basic` BOOLEAN NOT NULL DEFAULT false,

    INDEX `IDX_EE415F4616FE72E1`(`updated_by`),
    INDEX `IDX_EE415F46DE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_email_bounce` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `email` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,

    UNIQUE INDEX `email_unique`(`email`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_email_link` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `date_created` DATETIME(0) NOT NULL,
    `token` VARCHAR(255) NOT NULL,
    `path` LONGTEXT NOT NULL,
    `date_expired` DATETIME(0) NULL,
    `times_clicked` INTEGER NOT NULL DEFAULT 0,

    INDEX `IDX_6E7367C0A76ED395`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_email_message` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `to_user_id` INTEGER NULL,
    `from_user_id` INTEGER NULL,
    `date_sent` DATETIME(0) NOT NULL,
    `from_email` VARCHAR(255) NOT NULL,
    `from_name` VARCHAR(255) NOT NULL,
    `subject` VARCHAR(255) NOT NULL,
    `text_body` LONGTEXT NULL,
    `html_body` LONGTEXT NULL,
    `succeeded` BOOLEAN NULL,

    INDEX `IDX_F47BCF62130303A`(`from_user_id`),
    INDEX `IDX_F47BCF629F6EE60`(`to_user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_email_subscription` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `type` VARCHAR(255) NOT NULL,
    `level` INTEGER NOT NULL DEFAULT 0,

    INDEX `IDX_35EB5C44A76ED395`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_enrolment_period` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `credit_transaction_id` INTEGER NULL,
    `start` DATETIME(0) NOT NULL,
    `end` DATETIME(0) NOT NULL,
    `refunded` DATETIME(0) NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,
    `course_id` INTEGER NULL,
    `royalties_reported` BOOLEAN NOT NULL DEFAULT false,

    UNIQUE INDEX `UNIQ_36EFF610D834B30F`(`credit_transaction_id`),
    INDEX `IDX_36EFF610591CC992`(`course_id`),
    INDEX `IDX_36EFF610A76ED395`(`user_id`),
    INDEX `end_idx`(`end`),
    INDEX `start_idx`(`start`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_file` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `location` LONGTEXT NOT NULL,
    `mime_type` VARCHAR(255) NOT NULL,
    `file_size` INTEGER NOT NULL,
    `disk_name` VARCHAR(255) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `entity_type` VARCHAR(255) NULL,
    `original_name` LONGTEXT NULL,

    INDEX `IDX_89D9489916FE72E1`(`updated_by`),
    INDEX `IDX_89D94899DE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_group` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `institution_id` INTEGER NULL,
    `default_course_id` INTEGER NULL,
    `key` VARCHAR(191) NOT NULL,
    `label` VARCHAR(255) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `credit_type_id` INTEGER NULL,
    `setup` BOOLEAN NOT NULL DEFAULT true,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `lti_name` VARCHAR(255) NOT NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,
    `lti_context_id` VARCHAR(255) NULL,
    `join_code` VARCHAR(255) NULL,
    `course_id` INTEGER NULL,
    `registration_id` INTEGER NULL,
    `deployment_id_str` VARCHAR(255) NULL,
    `deployment_id` INTEGER NULL,

    UNIQUE INDEX `UNIQ_F9A1393F4E645A7E`(`key`),
    INDEX `IDX_F9A1393F10405986`(`institution_id`),
    INDEX `IDX_F9A1393F16FE72E1`(`updated_by`),
    INDEX `IDX_F9A1393F591CC992`(`course_id`),
    INDEX `IDX_F9A1393F833D8F43`(`registration_id`),
    INDEX `IDX_F9A1393F9DF4CE98`(`deployment_id`),
    INDEX `IDX_F9A1393FC87EE135`(`default_course_id`),
    INDEX `IDX_F9A1393FDBE9A713`(`lti_context_id`),
    INDEX `IDX_F9A1393FDE12AB56`(`created_by`),
    INDEX `IDX_F9A1393FFFC8EBBC`(`credit_type_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_infusionsoft_access_token` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `access_token` VARCHAR(255) NOT NULL,
    `refresh_token` VARCHAR(255) NOT NULL,
    `expires_at` DATETIME(0) NOT NULL,
    `extra_info` LONGTEXT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_infusionsoft_email_link` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `date_created` DATETIME(0) NOT NULL,
    `route` VARCHAR(255) NOT NULL,
    `email_code` VARCHAR(255) NOT NULL,

    INDEX `IDX_8FD65BC3A76ED395`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_institution` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `region_id` INTEGER NULL,
    `partner_id` INTEGER NULL,
    `key` VARCHAR(191) NOT NULL,
    `secret` VARCHAR(191) NOT NULL,
    `num_teachers` INTEGER NOT NULL DEFAULT 0,
    `enrolment_duration` INTEGER NOT NULL DEFAULT 365,
    `default_credit_price` DOUBLE NOT NULL DEFAULT 0,
    `contact_name` VARCHAR(255) NULL,
    `contact_email` VARCHAR(255) NULL,
    `grace_period_duration` INTEGER NOT NULL DEFAULT 20,
    `low_credit_notification` INTEGER NOT NULL DEFAULT 10,
    `auto_renew` BOOLEAN NOT NULL DEFAULT false,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `lti_version` VARCHAR(255) NULL,
    `timezone` VARCHAR(255) NULL,
    `christian` BOOLEAN NOT NULL DEFAULT false,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `outcome_service_url` LONGTEXT NULL,
    `lms` VARCHAR(255) NULL,
    `contact_user_id` INTEGER NULL,
    `lms_version` VARCHAR(255) NULL,
    `lms_version_approved` BOOLEAN NOT NULL DEFAULT true,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,
    `previous_approved_lms_version` VARCHAR(255) NULL,
    `lms_version_parameters` LONGTEXT NULL,
    `allow_observer_role` BOOLEAN NOT NULL DEFAULT false,
    `overdrive_website_id` INTEGER NULL,
    `exclude_from_reports` BOOLEAN NOT NULL DEFAULT false,
    `registration_token` INTEGER UNSIGNED NOT NULL,
    `key_disabled` BOOLEAN NOT NULL DEFAULT false,

    UNIQUE INDEX `UNIQ_F65406AE4E645A7E`(`key`),
    UNIQUE INDEX `UNIQ_F65406AE5CA2E8E5`(`secret`),
    UNIQUE INDEX `UNIQ_F65406AED09D01D3`(`registration_token`),
    INDEX `IDX_F65406AE16FE72E1`(`updated_by`),
    INDEX `IDX_F65406AE3D41F214`(`contact_user_id`),
    INDEX `IDX_F65406AE9393F8FE`(`partner_id`),
    INDEX `IDX_F65406AE98260155`(`region_id`),
    INDEX `IDX_F65406AEDE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_interactive` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `author_id` INTEGER NULL,
    `secular_alternative_id` INTEGER NULL,
    `screen_shot_file_id` INTEGER NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `description` LONGTEXT NULL,
    `christian` BOOLEAN NOT NULL DEFAULT false,
    `source` VARCHAR(255) NOT NULL,
    `url` VARCHAR(255) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `lti_external_tool_id` INTEGER NULL,
    `resource_link_url` TEXT NULL,
    `lti_consumer_key` VARCHAR(255) NULL,
    `lti_consumer_secret` VARCHAR(255) NULL,
    `lti_custom_parameters` TEXT NULL,
    `theme_id` INTEGER NULL,
    `css` LONGTEXT NOT NULL,
    `date_compiled` DATETIME(0) NULL,
    `instructions` LONGTEXT NULL,
    `review` LONGTEXT NULL,
    `iframe_height` INTEGER NULL,
    `force_fullscreen` BOOLEAN NOT NULL DEFAULT false,
    `iframe_aspect_ratio` VARCHAR(255) NULL,
    `fullscreen_only_instructions` LONGTEXT NULL,

    INDEX `IDX_F794137C16FE72E1`(`updated_by`),
    INDEX `IDX_F794137C4B7E5594`(`lti_external_tool_id`),
    INDEX `IDX_F794137C51899847`(`screen_shot_file_id`),
    INDEX `IDX_F794137C59027487`(`theme_id`),
    INDEX `IDX_F794137CDE12AB56`(`created_by`),
    INDEX `IDX_F794137CF28C831B`(`secular_alternative_id`),
    INDEX `IDX_F794137CF675F31B`(`author_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_interactive_status` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `interactive_id` INTEGER NULL,
    `date_last_viewed` DATETIME(0) NULL,
    `date_first_viewed` DATETIME(0) NULL,
    `times_viewed` INTEGER NOT NULL,
    `prepmagic_data` LONGTEXT NULL,
    `time_spent` INTEGER NOT NULL,
    `state` LONGTEXT NULL,
    `progress` LONGTEXT NULL,
    `percent_complete` DECIMAL(10, 3) NOT NULL,
    `viewed_instructions` BOOLEAN NOT NULL DEFAULT false,
    `viewed_review` BOOLEAN NOT NULL DEFAULT false,

    INDEX `IDX_138EE73B329C9C7D`(`interactive_id`),
    INDEX `IDX_138EE73BA76ED395`(`user_id`),
    UNIQUE INDEX `user_interactive_unique`(`user_id`, `interactive_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_internal_report` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` LONGTEXT NOT NULL,
    `required_parameters` LONGTEXT NOT NULL DEFAULT '{}',
    `prequery_statement` LONGTEXT NULL,
    `query` LONGTEXT NULL,
    `command_name` VARCHAR(255) NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,

    INDEX `IDX_6A6DFC8B16FE72E1`(`updated_by`),
    INDEX `IDX_6A6DFC8BDE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_internal_report_result` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `report_id` INTEGER NULL,
    `file_id` INTEGER NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `parameters` LONGTEXT NOT NULL DEFAULT '{}',
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,

    INDEX `IDX_3721E56116FE72E1`(`updated_by`),
    INDEX `IDX_3721E5614BD2A4C0`(`report_id`),
    INDEX `IDX_3721E56193CB796C`(`file_id`),
    INDEX `IDX_3721E561DE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_learnosity_activity` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `group_id` INTEGER NULL,
    `assessment_id` INTEGER NULL,
    `uuid` VARCHAR(255) NOT NULL,
    `grade_aggregation` VARCHAR(255) NULL,
    `config` LONGTEXT NULL,

    INDEX `IDX_85E82FEEDD3DD5F1`(`assessment_id`),
    INDEX `IDX_85E82FEEFE54D947`(`group_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_learnosity_assessment` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `reference` VARCHAR(255) NOT NULL,
    `render_type` INTEGER NOT NULL,
    `time_limit` INTEGER NULL,
    `max_number_of_attempts` INTEGER NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `grade_aggregation` VARCHAR(255) NOT NULL DEFAULT 'Natural',
    `config` LONGTEXT NULL,
    `theme_id` INTEGER NULL,
    `css` LONGTEXT NOT NULL,
    `date_compiled` DATETIME(0) NULL,
    `course_id` INTEGER NULL,
    `chapter_id` INTEGER NULL,
    `parent_id` INTEGER NULL,
    `sort_order` DOUBLE NULL,
    `number` INTEGER NULL,

    INDEX `IDX_A5F6FAAD16FE72E1`(`updated_by`),
    INDEX `IDX_A5F6FAAD579F4768`(`chapter_id`),
    INDEX `IDX_A5F6FAAD59027487`(`theme_id`),
    INDEX `IDX_A5F6FAAD591CC992`(`course_id`),
    INDEX `IDX_A5F6FAAD727ACA70`(`parent_id`),
    INDEX `IDX_A5F6FAADDE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_learnosity_assessment_configuration` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `assessment_id` INTEGER NOT NULL,
    `partner_id` INTEGER NULL,
    `institution_id` INTEGER NULL,
    `group_id` INTEGER NULL,
    `user_id` INTEGER NULL,
    `type` VARCHAR(100) NOT NULL,
    `config` LONGTEXT NULL,

    INDEX `IDX_F80460D310405986`(`institution_id`),
    INDEX `IDX_F80460D39393F8FE`(`partner_id`),
    INDEX `IDX_F80460D3A76ED395`(`user_id`),
    INDEX `IDX_F80460D3DD3DD5F1`(`assessment_id`),
    INDEX `IDX_F80460D3FE54D947`(`group_id`),
    UNIQUE INDEX `assessment_type_unique`(`assessment_id`, `partner_id`, `institution_id`, `group_id`, `user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_learnosity_grade` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `activity_id` INTEGER NOT NULL,
    `user_id` INTEGER NOT NULL,
    `aggregated_grade` DOUBLE NULL,
    `platform_grade` DOUBLE NULL,
    `feedback` LONGTEXT NULL,

    INDEX `IDX_9CA96ADB81C06096`(`activity_id`),
    INDEX `IDX_9CA96ADBA76ED395`(`user_id`),
    UNIQUE INDEX `UNIQ_9CA96ADB81C06096A76ED395`(`activity_id`, `user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_learnosity_session` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `activity_id` INTEGER NULL,
    `uuid` VARCHAR(255) NOT NULL,
    `num_attempted` INTEGER NULL,
    `num_questions` INTEGER NULL,
    `session_duration` INTEGER NULL,
    `status` VARCHAR(255) NULL,
    `date_created` DATETIME(0) NULL,
    `date_started` DATETIME(0) NULL,
    `date_completed` DATETIME(0) NULL,
    `items` LONGTEXT NOT NULL,
    `feedback_session_id` INTEGER NULL,
    `score` DOUBLE NULL,
    `item_max_scores` LONGTEXT NULL,
    `submission_results` LONGTEXT NULL,
    `feedback_results` LONGTEXT NULL,
    `override_results` LONGTEXT NULL,
    `is_feedback_session` BOOLEAN NOT NULL DEFAULT false,
    `date_graded` DATETIME(0) NULL,
    `auto_scored_items` LONGTEXT NULL,
    `config` LONGTEXT NULL,
    `intro_item` LONGTEXT NULL,
    `max_score` DOUBLE NULL,
    `attempt_number` INTEGER NULL,
    `date_saved` DATETIME(0) NULL,
    `date_resumed` DATETIME(0) NULL,
    `elapsedTime` INTEGER NOT NULL DEFAULT 0,
    `time_away` INTEGER NOT NULL DEFAULT 0,
    `time_added` INTEGER NOT NULL DEFAULT 0,

    UNIQUE INDEX `UNIQ_95ABF9AD3D32C285`(`feedback_session_id`),
    INDEX `IDX_95ABF9AD81C06096`(`activity_id`),
    INDEX `IDX_95ABF9ADA76ED395`(`user_id`),
    INDEX `date_completed_idx`(`date_completed`),
    INDEX `date_graded_idx`(`date_graded`),
    INDEX `status_idx`(`status`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_learnosity_session_item` (
    `session_id` INTEGER NOT NULL,
    `item_reference` VARCHAR(255) NOT NULL,

    INDEX `IDX_E8FC9BD1613FECDF`(`session_id`),
    INDEX `item_ref_idx`(`item_reference`),
    PRIMARY KEY (`session_id`, `item_reference`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_learnosity_user_config` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `activity_id` INTEGER NULL,
    `config` LONGTEXT NULL,

    INDEX `IDX_49E83EEF81C06096`(`activity_id`),
    INDEX `IDX_49E83EEFA76ED395`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_lesson` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `chapter_id` INTEGER NULL,
    `number` SMALLINT NOT NULL,
    `description` LONGTEXT NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `parent_id` INTEGER NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,
    `hideNumber` BOOLEAN NOT NULL DEFAULT false,
    `hideName` BOOLEAN NOT NULL DEFAULT false,
    `video_plate_file_id` INTEGER NULL,
    `lesson_page_css` LONGTEXT NULL,

    INDEX `IDX_A5880E8C16FE72E1`(`updated_by`),
    INDEX `IDX_A5880E8C579F4768`(`chapter_id`),
    INDEX `IDX_A5880E8C690FD836`(`video_plate_file_id`),
    INDEX `IDX_A5880E8C727ACA70`(`parent_id`),
    INDEX `IDX_A5880E8CDE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_lesson_learning_object_link` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `lesson_id` INTEGER NULL,
    `video_id` INTEGER NULL,
    `type` VARCHAR(255) NOT NULL,
    `number` INTEGER NOT NULL,
    `question_id` INTEGER NULL,
    `reading_id` INTEGER NULL,
    `group_number` INTEGER NOT NULL,
    `project_id` INTEGER NULL,
    `interactive_id` INTEGER NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,

    INDEX `IDX_AC8E303B166D1F9C`(`project_id`),
    INDEX `IDX_AC8E303B1E27F6BF`(`question_id`),
    INDEX `IDX_AC8E303B29C1004E`(`video_id`),
    INDEX `IDX_AC8E303B329C9C7D`(`interactive_id`),
    INDEX `IDX_AC8E303B527275CD`(`reading_id`),
    INDEX `IDX_AC8E303BCDF80196`(`lesson_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_lesson_progress` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `lesson_id` INTEGER NULL,
    `video_watched_percentage` DECIMAL(10, 3) NOT NULL,
    `question_green_percentage` DECIMAL(10, 3) NOT NULL,
    `question_red_percentage` DECIMAL(10, 3) NOT NULL,
    `question_yellow_percentage` DECIMAL(10, 3) NOT NULL,
    `date_first_viewed` DATETIME(0) NULL,
    `date_last_viewed` DATETIME(0) NULL,
    `date_last_updated` DATETIME(0) NULL,

    INDEX `IDX_6B027287A76ED395`(`user_id`),
    INDEX `IDX_6B027287CDF80196`(`lesson_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_lti_access_token` (
    `id` CHAR(36) NOT NULL,
    `registration_id` INTEGER NULL,
    `scopes` VARCHAR(255) NOT NULL,
    `expires_at` DATETIME(0) NOT NULL,
    `token` LONGTEXT NULL,

    INDEX `IDX_B7F73DD3833D8F43`(`registration_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_lti_deployment` (
    `registration_id` INTEGER NULL,
    `deployment_id` VARCHAR(255) NOT NULL,
    `institution_id` INTEGER NOT NULL,
    `enabled` BOOLEAN NOT NULL DEFAULT true,
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,

    INDEX `IDX_C440ED2310405986`(`institution_id`),
    INDEX `IDX_C440ED2316FE72E1`(`updated_by`),
    INDEX `IDX_C440ED23833D8F43`(`registration_id`),
    INDEX `IDX_C440ED23DE12AB56`(`created_by`),
    UNIQUE INDEX `reg_dep_idx`(`registration_id`, `deployment_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_lti_external_tool` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `lti_message_type` VARCHAR(255) NOT NULL DEFAULT 'basic-lti-launch-request',
    `lti_version` VARCHAR(255) NOT NULL DEFAULT 'LTI-1p0',
    `resource_link_title` VARCHAR(255) NOT NULL,
    `resource_link_url` TEXT NOT NULL,
    `lti_consumer_key` VARCHAR(255) NOT NULL,
    `lti_consumer_secret` VARCHAR(255) NOT NULL,
    `lti_custom_parameters` TEXT NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,

    INDEX `IDX_E95E611916FE72E1`(`updated_by`),
    INDEX `IDX_E95E6119DE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_lti_line_item` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `registration_id` INTEGER NULL,
    `activity_id` INTEGER NOT NULL,
    `resource_link_id` INTEGER NULL,
    `url` VARCHAR(255) NOT NULL,
    `deployment_id_str` VARCHAR(255) NULL,
    `lti_id` VARCHAR(255) NOT NULL,
    `score_maximum` DOUBLE NOT NULL,
    `label` VARCHAR(255) NOT NULL,
    `tag` VARCHAR(255) NULL,
    `start` DATETIME(0) NULL,
    `end` DATETIME(0) NULL,
    `submission_review_url` VARCHAR(255) NULL,
    `deployment_id` INTEGER NOT NULL,

    INDEX `IDX_A992CCFB81C06096`(`activity_id`),
    INDEX `IDX_A992CCFB833D8F43`(`registration_id`),
    INDEX `IDX_A992CCFB9DF4CE98`(`deployment_id`),
    INDEX `IDX_A992CCFBF004E599`(`resource_link_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_lti_lms_detail` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `institution_id` INTEGER NULL,
    `ext_lms` VARCHAR(255) NULL,
    `tool_consumer_info_product_family_code` VARCHAR(255) NULL,
    `tool_consumer_instance_guid` VARCHAR(255) NULL,
    `ext_d2l_tenantid` VARCHAR(255) NULL,
    `tool_consumer_info_version` VARCHAR(255) NULL,

    UNIQUE INDEX `UNIQ_C2A983FB10405986`(`institution_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_lti_outcome_result` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `activity_id` INTEGER NULL,
    `source_id` LONGTEXT NOT NULL,

    INDEX `IDX_87963D5C81C06096`(`activity_id`),
    INDEX `IDX_87963D5CA76ED395`(`user_id`),
    UNIQUE INDEX `user_activity_unique`(`user_id`, `activity_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_lti_registration` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `institution_id` INTEGER NULL,
    `auth_login_url` VARCHAR(255) NOT NULL,
    `auth_token_url` VARCHAR(255) NOT NULL,
    `key_set_url` VARCHAR(255) NULL,
    `client_id` VARCHAR(255) NOT NULL,
    `issuer` VARCHAR(255) NOT NULL,
    `enabled` BOOLEAN NOT NULL DEFAULT true,
    `name` VARCHAR(255) NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,

    INDEX `IDX_63FD471C16FE72E1`(`updated_by`),
    INDEX `IDX_63FD471CDE12AB56`(`created_by`),
    UNIQUE INDEX `UNIQ_63FD471CAD7A4F1519EB6921`(`issuer`, `client_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_lti_registration_institution` (
    `registration_id` INTEGER NOT NULL,
    `institution_id` INTEGER NOT NULL,

    INDEX `IDX_121F3C4210405986`(`institution_id`),
    INDEX `IDX_121F3C42833D8F43`(`registration_id`),
    PRIMARY KEY (`registration_id`, `institution_id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_lti_resource_link` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `registration_id` INTEGER NULL,
    `deployment_id_str` VARCHAR(255) NULL,
    `lti_context_id` VARCHAR(255) NOT NULL,
    `resource_link_id` VARCHAR(255) NOT NULL,
    `title` VARCHAR(255) NULL,
    `description` VARCHAR(255) NULL,
    `visits` INTEGER NOT NULL DEFAULT 0,
    `service_claims` LONGTEXT NULL,
    `target_uri` VARCHAR(255) NOT NULL,
    `last_access` DATETIME(0) NULL,
    `deployment_id` INTEGER NOT NULL,

    INDEX `IDX_653E06AF833D8F43`(`registration_id`),
    INDEX `IDX_653E06AF9DF4CE98`(`deployment_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_lti_user` (
    `registration_id` INTEGER NULL,
    `deployment_id_str` VARCHAR(255) NULL,
    `sub` VARCHAR(255) NOT NULL,
    `user_id` INTEGER NULL,
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `deployment_id` INTEGER NOT NULL,

    INDEX `IDX_DB59319F833D8F43`(`registration_id`),
    INDEX `IDX_DB59319F9DF4CE98`(`deployment_id`),
    INDEX `IDX_DB59319FA76ED395`(`user_id`),
    UNIQUE INDEX `reg_dep_use_idx`(`registration_id`, `deployment_id`, `sub`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_permission` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `description` VARCHAR(255) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,

    INDEX `IDX_27A04C1E16FE72E1`(`updated_by`),
    INDEX `IDX_27A04C1EDE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_permission_value` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `role_id` INTEGER NULL,
    `permission_id` INTEGER NULL,
    `value` VARCHAR(255) NOT NULL,

    INDEX `IDX_43F92B6BD60322AC`(`role_id`),
    INDEX `IDX_43F92B6BFED90CCA`(`permission_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_previous_password` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `password` VARCHAR(255) NOT NULL,
    `salt` VARCHAR(255) NULL,
    `date` DATETIME(0) NOT NULL,

    INDEX `IDX_18793B2CA76ED395`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_project` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `description` LONGTEXT NOT NULL,
    `color` INTEGER NOT NULL DEFAULT 1,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `course_id` INTEGER NULL,
    `chapter_id` INTEGER NULL,
    `number` INTEGER NULL,

    INDEX `IDX_EF54403916FE72E1`(`updated_by`),
    INDEX `IDX_EF544039579F4768`(`chapter_id`),
    INDEX `IDX_EF544039591CC992`(`course_id`),
    INDEX `IDX_EF544039DE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_project_map` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `course_id` INTEGER NULL,
    `chapter_id` INTEGER NULL,
    `original_project_id` INTEGER NULL,
    `mapped_project_id` INTEGER NULL,

    INDEX `IDX_CD1DEE13579F4768`(`chapter_id`),
    INDEX `IDX_CD1DEE13591CC992`(`course_id`),
    INDEX `IDX_CD1DEE137BDA6808`(`original_project_id`),
    INDEX `IDX_CD1DEE13F587106C`(`mapped_project_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_public_link` (
    `id` CHAR(36) NOT NULL,
    `user_id` INTEGER NULL,
    `url` VARCHAR(255) NOT NULL,
    `active` BOOLEAN NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,

    INDEX `IDX_813A456BA76ED395`(`user_id`),
    UNIQUE INDEX `user_url_uniquex`(`user_id`, `url`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_question` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `name` VARCHAR(255) NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `solution_video_id` INTEGER NULL,
    `linked_video_id` INTEGER NULL,
    `author_id` INTEGER NULL,
    `grade_level` VARCHAR(255) NOT NULL,
    `question_type_id` INTEGER NULL,
    `preview` VARCHAR(255) NOT NULL,
    `average_time_spent` INTEGER NULL,
    `date_last_calculated` DATETIME(0) NULL,
    `secular_alternative_id` INTEGER NULL,
    `christian` BOOLEAN NOT NULL DEFAULT false,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,

    INDEX `IDX_AE80E9A916FE72E1`(`updated_by`),
    INDEX `IDX_AE80E9A92D1F8EE`(`solution_video_id`),
    INDEX `IDX_AE80E9A947C14E91`(`linked_video_id`),
    INDEX `IDX_AE80E9A9CB90598E`(`question_type_id`),
    INDEX `IDX_AE80E9A9DE12AB56`(`created_by`),
    INDEX `IDX_AE80E9A9F28C831B`(`secular_alternative_id`),
    INDEX `IDX_AE80E9A9F675F31B`(`author_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_question_object` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `question_id` INTEGER NULL,
    `parent_id` INTEGER NULL,
    `file_id` INTEGER NULL,
    `video_id` INTEGER NULL,
    `type` VARCHAR(255) NOT NULL,
    `content` LONGTEXT NULL,
    `lft` INTEGER NULL,
    `rgt` INTEGER NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,

    INDEX `IDX_AEB44E3C1E27F6BF`(`question_id`),
    INDEX `IDX_AEB44E3C29C1004E`(`video_id`),
    INDEX `IDX_AEB44E3C727ACA70`(`parent_id`),
    INDEX `IDX_AEB44E3C93CB796C`(`file_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_question_status` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `question_id` INTEGER NULL,
    `date_last_viewed` DATETIME(0) NULL,
    `date_first_viewed` DATETIME(0) NULL,
    `times_viewed` INTEGER NOT NULL,
    `flagged` DATETIME(0) NULL,
    `ever_incorrect` BOOLEAN NOT NULL,
    `feedback` LONGTEXT NOT NULL,
    `percent_green` DECIMAL(10, 3) NOT NULL,
    `percent_red` DECIMAL(10, 3) NOT NULL,
    `percent_yellow` DECIMAL(10, 3) NOT NULL,
    `time_spent` INTEGER NOT NULL,
    `solution_steps_opened` VARCHAR(255) NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,

    INDEX `IDX_7D1980CC1E27F6BF`(`question_id`),
    INDEX `IDX_7D1980CCA76ED395`(`user_id`),
    UNIQUE INDEX `user_question_unique`(`user_id`, `question_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_question_type` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,

    INDEX `IDX_D60A5E8016FE72E1`(`updated_by`),
    INDEX `IDX_D60A5E80DE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_reading` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `author_id` INTEGER NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `css` LONGTEXT NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `singular` BOOLEAN NOT NULL DEFAULT false,
    `secular_alternative_id` INTEGER NULL,
    `christian` BOOLEAN NOT NULL DEFAULT false,
    `theme_id` INTEGER NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `class_name` VARCHAR(255) NULL,
    `borderless` BOOLEAN NOT NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,
    `date_compiled` DATETIME(0) NULL,
    `include_in_reports` BOOLEAN NOT NULL DEFAULT true,
    `advanced` BOOLEAN NOT NULL DEFAULT false,
    `has_notes` BOOLEAN NOT NULL DEFAULT false,
    `notes_theme_id` INTEGER NULL,
    `notes_html` LONGTEXT NULL,
    `notes_css` LONGTEXT NULL,
    `notes_theme_different` BOOLEAN NOT NULL DEFAULT false,

    INDEX `IDX_1FD6C9616FE72E1`(`updated_by`),
    INDEX `IDX_1FD6C9635043726`(`notes_theme_id`),
    INDEX `IDX_1FD6C9659027487`(`theme_id`),
    INDEX `IDX_1FD6C96DE12AB56`(`created_by`),
    INDEX `IDX_1FD6C96F28C831B`(`secular_alternative_id`),
    INDEX `IDX_1FD6C96F675F31B`(`author_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_reading_chunk` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `reading_id` INTEGER NULL,
    `number` INTEGER NOT NULL,
    `html` LONGTEXT NOT NULL,
    `css` LONGTEXT NOT NULL,
    `width` DOUBLE NOT NULL,
    `transition_id` INTEGER NULL,

    INDEX `IDX_FD0D56ED527275CD`(`reading_id`),
    INDEX `IDX_FD0D56ED8BF1A064`(`transition_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_reading_compiled_css` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `reading_id` INTEGER NULL,
    `css` LONGTEXT NOT NULL,

    UNIQUE INDEX `UNIQ_FE49BA42527275CD`(`reading_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_reading_status` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `reading_id` INTEGER NULL,
    `date_last_viewed` DATETIME(0) NULL,
    `date_first_viewed` DATETIME(0) NULL,
    `times_viewed` INTEGER NOT NULL,
    `percent_viewed` DECIMAL(10, 3) NOT NULL,
    `viewed_chunk_ids` LONGTEXT NOT NULL,
    `collapsed` BOOLEAN NOT NULL,
    `time_spent` INTEGER NOT NULL,

    INDEX `IDX_7905FD31527275CD`(`reading_id`),
    INDEX `IDX_7905FD31A76ED395`(`user_id`),
    UNIQUE INDEX `user_reading_unique`(`user_id`, `reading_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_reading_theme` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `css` LONGTEXT NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `compiled_css` LONGTEXT NULL,

    INDEX `IDX_FF7EC0CB16FE72E1`(`updated_by`),
    INDEX `IDX_FF7EC0CBDE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_reading_transition` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `css` LONGTEXT NOT NULL,
    `javascript` LONGTEXT NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_report_dashboard` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `description` VARCHAR(255) NULL,
    `template` BOOLEAN NOT NULL,
    `order` INTEGER NOT NULL,
    `parameters` LONGTEXT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `name` VARCHAR(255) NOT NULL,

    INDEX `IDX_FA298CCCA76ED395`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_report_dashboard_template` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `category` VARCHAR(255) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NOT NULL,
    `customization_settings` LONGTEXT NOT NULL,
    `benefits` LONGTEXT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_report_dashboard_template_widget` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `dashboard_template_id` INTEGER NULL,
    `widget_template_id` INTEGER NULL,
    `widget_settings` LONGTEXT NOT NULL,
    `order` INTEGER NOT NULL,
    `rows` INTEGER NOT NULL,
    `columns` INTEGER NOT NULL,

    INDEX `IDX_6EEAD97A3BC08534`(`widget_template_id`),
    INDEX `IDX_6EEAD97A5FFB2CF7`(`dashboard_template_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_report_widget` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `dashboard_id` INTEGER NULL,
    `template_id` INTEGER NULL,
    `user_id` INTEGER NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NULL,
    `widget_type` VARCHAR(255) NOT NULL,
    `sql` LONGTEXT NULL,
    `parameters` LONGTEXT NULL,
    `order` INTEGER NOT NULL,
    `columns` INTEGER NOT NULL,
    `rows` INTEGER NOT NULL,
    `settings` LONGTEXT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,

    INDEX `IDX_A8302F095DA0FB8`(`template_id`),
    INDEX `IDX_A8302F09A76ED395`(`user_id`),
    INDEX `IDX_A8302F09B9D04D2B`(`dashboard_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_report_widget_template` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NOT NULL,
    `settings` LONGTEXT NOT NULL,
    `customization_settings` LONGTEXT NOT NULL,
    `widget_type` VARCHAR(255) NOT NULL,
    `benefits` LONGTEXT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_role` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `description` VARCHAR(255) NOT NULL,
    `priority` INTEGER NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,

    INDEX `IDX_522FF4E316FE72E1`(`updated_by`),
    INDEX `IDX_522FF4E3DE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_role_assignment` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `context_id` INTEGER NULL,
    `user_id` INTEGER NULL,
    `role_id` INTEGER NULL,
    `deleted` BOOLEAN NOT NULL DEFAULT false,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,

    INDEX `IDX_BEB5E44E6B00C1CF`(`context_id`),
    INDEX `IDX_BEB5E44EA76ED395`(`user_id`),
    INDEX `IDX_BEB5E44ED60322AC`(`role_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_royalty_report_current` (
    `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `enrolment_period_id` INTEGER NOT NULL,
    `author_id` INTEGER NOT NULL,
    `institution_id` INTEGER NOT NULL,
    `views` INTEGER NOT NULL,
    `portions` DECIMAL(10, 3) NOT NULL,
    `sf2_views` INTEGER NOT NULL DEFAULT 0,
    `price` DECIMAL(10, 2) NOT NULL,
    `entity_type` VARCHAR(255) NOT NULL,

    UNIQUE INDEX `key_qualifier`(`author_id`, `institution_id`, `entity_type`, `enrolment_period_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_royalty_report_current_files` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `enrolment_period_id` INTEGER NOT NULL,
    `group_id` INTEGER NOT NULL,
    `file_id` INTEGER NOT NULL,
    `lti_request_uri` VARCHAR(255) NOT NULL,
    `views` INTEGER NOT NULL DEFAULT 0,

    INDEX `sf_royalty_report_current_files_sf_file_id_fk`(`file_id`),
    INDEX `sf_royalty_report_current_files_sf_group_id_fk`(`group_id`),
    INDEX `sf_royalty_report_current_files_sf_user_id_fk`(`user_id`),
    UNIQUE INDEX `ep_g_f_id_uindex`(`enrolment_period_id`, `group_id`, `file_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_royalty_report_current_interactive_status` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `interactive_id` INTEGER NULL,
    `date_last_viewed` DATETIME(0) NULL,
    `date_first_viewed` DATETIME(0) NULL,
    `times_viewed` INTEGER NOT NULL,
    `prepmagic_data` LONGTEXT NULL,
    `time_spent` INTEGER NOT NULL,
    `state` LONGTEXT NULL,
    `progress` LONGTEXT NULL,
    `percent_complete` DECIMAL(10, 3) NOT NULL,
    `enrolment_period_id` INTEGER NULL,

    INDEX `IDX_4C70DF85329C9C7D`(`interactive_id`),
    INDEX `IDX_4C70DF85A76ED395`(`user_id`),
    UNIQUE INDEX `user_interactive_unique`(`user_id`, `interactive_id`, `enrolment_period_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_royalty_report_current_learnosity_item` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `enrolment_period_id` INTEGER NOT NULL,
    `item_reference` VARCHAR(255) NOT NULL,
    `count` INTEGER NOT NULL DEFAULT 0,

    UNIQUE INDEX `enrolment_period_id_item_reference_uindex`(`enrolment_period_id`, `item_reference`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_royalty_report_current_question_status` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `question_id` INTEGER NULL,
    `date_last_viewed` DATETIME(0) NULL,
    `date_first_viewed` DATETIME(0) NULL,
    `times_viewed` INTEGER NOT NULL,
    `flagged` DATETIME(0) NULL,
    `ever_incorrect` BOOLEAN NOT NULL,
    `feedback` LONGTEXT NOT NULL,
    `percent_green` DECIMAL(10, 3) NOT NULL,
    `percent_red` DECIMAL(10, 3) NOT NULL,
    `percent_yellow` DECIMAL(10, 3) NOT NULL,
    `time_spent` INTEGER NOT NULL,
    `solution_steps_opened` LONGTEXT NULL,
    `enrolment_period_id` INTEGER NULL,

    INDEX `IDX_ECED1FD51E27F6BF`(`question_id`),
    INDEX `IDX_ECED1FD5A76ED395`(`user_id`),
    UNIQUE INDEX `user_question_unique`(`user_id`, `question_id`, `enrolment_period_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_royalty_report_current_reading_status` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `reading_id` INTEGER NULL,
    `date_last_viewed` DATETIME(0) NULL,
    `date_first_viewed` DATETIME(0) NULL,
    `times_viewed` INTEGER NOT NULL,
    `percent_viewed` DECIMAL(10, 3) NOT NULL,
    `viewed_chunk_ids` LONGTEXT NOT NULL,
    `collapsed` BOOLEAN NOT NULL,
    `time_spent` INTEGER NOT NULL,
    `enrolment_period_id` INTEGER NULL,

    INDEX `IDX_E9F6737F527275CD`(`reading_id`),
    INDEX `IDX_E9F6737FA76ED395`(`user_id`),
    UNIQUE INDEX `user_reading_unique`(`user_id`, `reading_id`, `enrolment_period_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_royalty_report_current_video_status` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `video_id` INTEGER NULL,
    `percent_watched` DECIMAL(10, 3) NOT NULL,
    `date_last_viewed` DATETIME(0) NULL,
    `date_first_viewed` DATETIME(0) NULL,
    `times_viewed` INTEGER NOT NULL,
    `checkpoints` LONGTEXT NOT NULL,
    `current_position` INTEGER NOT NULL,
    `time_spent` INTEGER NOT NULL,
    `enrolment_period_id` INTEGER NULL,

    INDEX `IDX_585AC77629C1004E`(`video_id`),
    INDEX `IDX_585AC776A76ED395`(`user_id`),
    UNIQUE INDEX `user_video_unique`(`user_id`, `video_id`, `enrolment_period_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_royalty_report_metadata` (
    `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `key` VARCHAR(255) NOT NULL,
    `value` VARCHAR(255) NULL,
    `qualifier` VARCHAR(255) NULL,
    `qualifier_value` VARCHAR(255) NULL,

    UNIQUE INDEX `key_qualifier`(`key`, `qualifier`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_royalty_view_count` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `enrolment_period_id` INTEGER NOT NULL,
    `entity_type` VARCHAR(255) NOT NULL,
    `entity_id` VARCHAR(255) NOT NULL,
    `multiplier` DOUBLE NOT NULL DEFAULT 1,

    INDEX `IDX_AE96B8C394FEE7D3`(`enrolment_period_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_skill` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_snapshot` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `project_id` INTEGER NULL,
    `created_by` INTEGER NULL,
    `session_id` INTEGER NULL,
    `user_id` INTEGER NULL,
    `name` VARCHAR(255) NULL,
    `date_created` DATETIME(0) NOT NULL,

    INDEX `IDX_343AB5D2166D1F9C`(`project_id`),
    INDEX `IDX_343AB5D2613FECDF`(`session_id`),
    INDEX `IDX_343AB5D2A76ED395`(`user_id`),
    INDEX `IDX_343AB5D2DE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_tag` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `object_type` VARCHAR(255) NOT NULL,
    `object_id` INTEGER NOT NULL,
    `tag_value` VARCHAR(255) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_tutorial` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `alternate_version_id` INTEGER NULL,
    `category` VARCHAR(255) NULL,
    `type` INTEGER NOT NULL DEFAULT 0,

    UNIQUE INDEX `UNIQ_DE1C5F0EE4EDEC69`(`alternate_version_id`),
    INDEX `IDX_DE1C5F0E16FE72E1`(`updated_by`),
    INDEX `IDX_DE1C5F0EDE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_tutorial_slide` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `tutorial_id` INTEGER NULL,
    `file_id` INTEGER NULL,
    `action_file_id` INTEGER NULL,
    `number` INTEGER NOT NULL,
    `type` VARCHAR(255) NOT NULL,
    `background_color` VARCHAR(255) NULL,
    `text` LONGTEXT NOT NULL,
    `rectangle` LONGTEXT NULL,
    `second_rectangle` LONGTEXT NULL,
    `action_text` LONGTEXT NULL,
    `number_of_strokes` INTEGER NULL,

    INDEX `IDX_E546CB894969F18D`(`action_file_id`),
    INDEX `IDX_E546CB8989366B7B`(`tutorial_id`),
    INDEX `IDX_E546CB8993CB796C`(`file_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_tutorial_status` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `tutorial_id` INTEGER NULL,
    `date_last_viewed` DATETIME(0) NULL,
    `date_first_viewed` DATETIME(0) NULL,
    `times_viewed` INTEGER NOT NULL,
    `percent_viewed` DECIMAL(10, 3) NOT NULL,
    `skipped` BOOLEAN NOT NULL,
    `viewed_slide_ids` LONGTEXT NOT NULL,

    INDEX `IDX_4C4FF7C989366B7B`(`tutorial_id`),
    INDEX `IDX_4C4FF7C9A76ED395`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_user` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `institution_id` INTEGER NULL,
    `default_course_id` INTEGER NULL,
    `auth` VARCHAR(20) NOT NULL DEFAULT 'manual',
    `username` VARCHAR(180) NOT NULL,
    `firstname` VARCHAR(255) NOT NULL,
    `lastname` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) NULL,
    `email_stop` BOOLEAN NOT NULL DEFAULT false,
    `password` VARCHAR(255) NOT NULL,
    `salt` VARCHAR(255) NULL,
    `first_access` DATETIME(0) NULL,
    `last_access` DATETIME(0) NULL,
    `last_login` DATETIME(0) NULL,
    `active` BOOLEAN NOT NULL,
    `last_ip` VARCHAR(45) NULL,
    `timezone` VARCHAR(255) NOT NULL,
    `show_next_video_popup` BOOLEAN NOT NULL DEFAULT true,
    `default_course_auto_set` BOOLEAN NOT NULL DEFAULT true,
    `username_canonical` VARCHAR(180) NOT NULL,
    `email_canonical` VARCHAR(255) NULL,
    `enabled` BOOLEAN NOT NULL,
    `confirmation_token` VARCHAR(180) NULL,
    `password_requested_at` DATETIME(0) NULL,
    `roles` LONGTEXT NOT NULL,
    `christian` BOOLEAN NULL,
    `lti_username` VARCHAR(255) NULL,
    `password_changed` BOOLEAN NOT NULL DEFAULT false,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,
    `lti_consumer_key` VARCHAR(255) NULL,
    `lti_user_id` VARCHAR(255) NULL,
    `region_id` INTEGER NULL,
    `school_name` VARCHAR(255) NULL,
    `requested_role` VARCHAR(255) NULL,
    `sign_up_reason` LONGTEXT NULL,
    `email_confirmed` BOOLEAN NOT NULL DEFAULT false,
    `acc_settings` LONGTEXT NULL,
    `force_password_change` BOOLEAN NOT NULL DEFAULT false,

    UNIQUE INDEX `UNIQ_88D5A8C092FC23A8`(`username_canonical`),
    UNIQUE INDEX `UNIQ_88D5A8C0C05FB297`(`confirmation_token`),
    INDEX `IDX_88D5A8C010405986`(`institution_id`),
    INDEX `IDX_88D5A8C098260155`(`region_id`),
    INDEX `IDX_88D5A8C0C87EE135`(`default_course_id`),
    INDEX `lti_consumer_key_user_idx`(`lti_consumer_key`, `lti_user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_video` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `video_file_id` INTEGER NULL,
    `notes_file_id` INTEGER NULL,
    `screen_shot_file_id` INTEGER NULL,
    `stripped_video_file_id` INTEGER NULL,
    `type` INTEGER NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` LONGTEXT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `length` INTEGER NOT NULL,
    `resolution_width` INTEGER NOT NULL,
    `resolution_height` INTEGER NOT NULL,
    `stripped_file_version` VARCHAR(255) NULL,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `date_created` DATETIME(0) NOT NULL,
    `author_id` INTEGER NULL,
    `subtitles` LONGTEXT NULL,
    `secular_alternative_id` INTEGER NULL,
    `christian` BOOLEAN NOT NULL DEFAULT false,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,
    `video_plate_file_id` INTEGER NULL,

    INDEX `IDX_E8A6A7D616FE72E1`(`updated_by`),
    INDEX `IDX_E8A6A7D651899847`(`screen_shot_file_id`),
    INDEX `IDX_E8A6A7D6690FD836`(`video_plate_file_id`),
    INDEX `IDX_E8A6A7D6762690C1`(`video_file_id`),
    INDEX `IDX_E8A6A7D6931D3538`(`notes_file_id`),
    INDEX `IDX_E8A6A7D6DE12AB56`(`created_by`),
    INDEX `IDX_E8A6A7D6DECB30CC`(`stripped_video_file_id`),
    INDEX `IDX_E8A6A7D6F28C831B`(`secular_alternative_id`),
    INDEX `IDX_E8A6A7D6F675F31B`(`author_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_video_interaction` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `video_id` INTEGER NULL,
    `file_id` INTEGER NULL,
    `type` VARCHAR(255) NOT NULL,
    `value` VARCHAR(255) NULL,
    `start_time` DOUBLE NOT NULL,
    `end_time` DOUBLE NULL,
    `pause_time` DOUBLE NULL,
    `continue_from_time` DOUBLE NULL,
    `fade_in_duration` DOUBLE NOT NULL DEFAULT 0.5,
    `fade_out_duration` DOUBLE NOT NULL DEFAULT 0.5,
    `x` DOUBLE NOT NULL,
    `y` DOUBLE NOT NULL,
    `width` DOUBLE NOT NULL,
    `height` DOUBLE NOT NULL,
    `theme_id` INTEGER NULL,
    `audio_file_id` INTEGER NULL,
    `url` VARCHAR(255) NULL,
    `target` VARCHAR(255) NULL,
    `audio_file_subtitles` LONGTEXT NULL,
    `learning_object_type` VARCHAR(255) NULL,
    `learning_object_id` INTEGER NULL,
    `instance_name` VARCHAR(255) NULL,
    `actions` LONGTEXT NULL,
    `show_icon` BOOLEAN NOT NULL DEFAULT true,

    INDEX `IDX_275F29FB29C1004E`(`video_id`),
    INDEX `IDX_275F29FB59027487`(`theme_id`),
    INDEX `IDX_275F29FB93CB796C`(`file_id`),
    INDEX `IDX_275F29FBAC7C70B0`(`audio_file_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_video_interaction_theme` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `created_by` INTEGER NULL,
    `updated_by` INTEGER NULL,
    `css` LONGTEXT NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME(0) NOT NULL,
    `date_last_updated` DATETIME(0) NOT NULL,
    `is_default` BOOLEAN NOT NULL DEFAULT false,
    `default_button_text` VARCHAR(255) NULL,

    INDEX `IDX_A5E0AA16FE72E1`(`updated_by`),
    INDEX `IDX_A5E0AADE12AB56`(`created_by`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `sf_video_status` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `video_id` INTEGER NULL,
    `percent_watched` DECIMAL(10, 3) NOT NULL,
    `date_last_viewed` DATETIME(0) NULL,
    `date_first_viewed` DATETIME(0) NULL,
    `times_viewed` INTEGER NOT NULL,
    `checkpoints` LONGTEXT NOT NULL,
    `current_position` INTEGER NOT NULL,
    `time_spent` INTEGER NOT NULL,
    `original_table_name` VARCHAR(255) NULL,
    `original_id` INTEGER NULL,
    `date_last_imported` DATETIME(0) NULL,
    `import_unlinked` BOOLEAN NOT NULL DEFAULT false,

    INDEX `IDX_A295965729C1004E`(`video_id`),
    INDEX `IDX_A2959657A76ED395`(`user_id`),
    UNIQUE INDEX `user_video_unique`(`user_id`, `video_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `sf_country` ADD CONSTRAINT `FK_939459B116FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_country` ADD CONSTRAINT `FK_939459B1DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_partner` ADD CONSTRAINT `FK_F1CCAEC116FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_partner` ADD CONSTRAINT `FK_F1CCAEC198260155` FOREIGN KEY (`region_id`) REFERENCES `sf_region`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_partner` ADD CONSTRAINT `FK_F1CCAEC1DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_region` ADD CONSTRAINT `FK_529E8B0916FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_region` ADD CONSTRAINT `FK_529E8B09DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_region` ADD CONSTRAINT `FK_529E8B09F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `sf_country`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `af_api_token` ADD CONSTRAINT `FK_A5F786F3A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_author` ADD CONSTRAINT `FK_E053A2B716FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_author` ADD CONSTRAINT `FK_E053A2B76B00C1CF` FOREIGN KEY (`context_id`) REFERENCES `sf_context`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_author` ADD CONSTRAINT `FK_E053A2B798260155` FOREIGN KEY (`region_id`) REFERENCES `sf_region`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_author` ADD CONSTRAINT `FK_E053A2B7A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_author` ADD CONSTRAINT `FK_E053A2B7DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_author_portion` ADD CONSTRAINT `FK_3A01BC24F675F31B` FOREIGN KEY (`author_id`) REFERENCES `sf_author`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_bible_version` ADD CONSTRAINT `FK_ABFB797316FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_bible_version` ADD CONSTRAINT `FK_ABFB7973DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_chapter` ADD CONSTRAINT `FK_396625F916FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_chapter` ADD CONSTRAINT `FK_396625F9591CC992` FOREIGN KEY (`course_id`) REFERENCES `sf_course`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_chapter` ADD CONSTRAINT `FK_396625F9690FD836` FOREIGN KEY (`video_plate_file_id`) REFERENCES `sf_file`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_chapter` ADD CONSTRAINT `FK_396625F9DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_chapter_progress` ADD CONSTRAINT `FK_4C11816F579F4768` FOREIGN KEY (`chapter_id`) REFERENCES `sf_chapter`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_chapter_progress` ADD CONSTRAINT `FK_4C11816FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_compiled_css` ADD CONSTRAINT `FK_7195B7A1329C9C7D` FOREIGN KEY (`interactive_id`) REFERENCES `sf_interactive`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_compiled_css` ADD CONSTRAINT `FK_7195B7A1527275CD` FOREIGN KEY (`reading_id`) REFERENCES `sf_reading`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_compiled_css` ADD CONSTRAINT `FK_7195B7A1CF6C57B7` FOREIGN KEY (`css_theme_id`) REFERENCES `sf_css_theme`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_compiled_css` ADD CONSTRAINT `FK_7195B7A1DD3DD5F1` FOREIGN KEY (`assessment_id`) REFERENCES `sf_learnosity_assessment`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_context` ADD CONSTRAINT `FK_22BA15A910405986` FOREIGN KEY (`institution_id`) REFERENCES `sf_institution`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_context` ADD CONSTRAINT `FK_22BA15A916FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_context` ADD CONSTRAINT `FK_22BA15A9727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `sf_context`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_context` ADD CONSTRAINT `FK_22BA15A99393F8FE` FOREIGN KEY (`partner_id`) REFERENCES `sf_partner`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_context` ADD CONSTRAINT `FK_22BA15A9DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_context` ADD CONSTRAINT `FK_22BA15A9FE54D947` FOREIGN KEY (`group_id`) REFERENCES `sf_group`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_course` ADD CONSTRAINT `FK_169E6FB98C782417` FOREIGN KEY (`header_image_id`) REFERENCES `sf_file`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_course` ADD CONSTRAINT `FK_4B6215C616FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_course` ADD CONSTRAINT `FK_4B6215C6DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_course` ADD CONSTRAINT `FK_4B6215C6276AF86B` FOREIGN KEY (`prerequisite_id`) REFERENCES `sf_course`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_course` ADD CONSTRAINT `FK_4B6215C6690FD836` FOREIGN KEY (`video_plate_file_id`) REFERENCES `sf_file`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_course` ADD CONSTRAINT `FK_4B6215C698260155` FOREIGN KEY (`region_id`) REFERENCES `sf_region`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_course` ADD CONSTRAINT `FK_4B6215C6FFC8EBBC` FOREIGN KEY (`credit_type_id`) REFERENCES `sf_credit_type`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_course_context` ADD CONSTRAINT `FK_75255ED3591CC992` FOREIGN KEY (`course_id`) REFERENCES `sf_course`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_course_context` ADD CONSTRAINT `FK_75255ED36B00C1CF` FOREIGN KEY (`context_id`) REFERENCES `sf_context`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_course_editor` ADD CONSTRAINT `FK_66F3D4B1591CC992` FOREIGN KEY (`course_id`) REFERENCES `sf_course`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_course_editor` ADD CONSTRAINT `FK_66F3D4B1A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_course_equivalent` ADD CONSTRAINT `FK_CE63CF35591CC992` FOREIGN KEY (`course_id`) REFERENCES `sf_course`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_course_equivalent` ADD CONSTRAINT `FK_CE63CF35D1487398` FOREIGN KEY (`equivalent_id`) REFERENCES `sf_course`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_course_progress` ADD CONSTRAINT `FK_8AF155C8591CC992` FOREIGN KEY (`course_id`) REFERENCES `sf_course`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_course_progress` ADD CONSTRAINT `FK_8AF155C8A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_credit_transaction` ADD CONSTRAINT `FK_9D233D2510405986` FOREIGN KEY (`institution_id`) REFERENCES `sf_institution`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_credit_transaction` ADD CONSTRAINT `FK_9D233D25279C6687` FOREIGN KEY (`user_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_credit_transaction` ADD CONSTRAINT `FK_9D233D254636873` FOREIGN KEY (`user_for`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_credit_transaction` ADD CONSTRAINT `FK_9D233D25FE54D947` FOREIGN KEY (`group_id`) REFERENCES `sf_group`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_credit_transaction` ADD CONSTRAINT `FK_9D233D25FFC8EBBC` FOREIGN KEY (`credit_type_id`) REFERENCES `sf_credit_type`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_credit_type` ADD CONSTRAINT `FK_47D7765F16FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_credit_type` ADD CONSTRAINT `FK_47D7765FDE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_css_theme` ADD CONSTRAINT `FK_EE415F4616FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_css_theme` ADD CONSTRAINT `FK_EE415F46DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_email_link` ADD CONSTRAINT `FK_6E7367C0A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_email_message` ADD CONSTRAINT `FK_F47BCF62130303A` FOREIGN KEY (`from_user_id`) REFERENCES `sf_user`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_email_message` ADD CONSTRAINT `FK_F47BCF629F6EE60` FOREIGN KEY (`to_user_id`) REFERENCES `sf_user`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_email_subscription` ADD CONSTRAINT `FK_35EB5C44A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_enrolment_period` ADD CONSTRAINT `FK_36EFF610591CC992` FOREIGN KEY (`course_id`) REFERENCES `sf_course`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_enrolment_period` ADD CONSTRAINT `FK_36EFF610A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_enrolment_period` ADD CONSTRAINT `FK_36EFF610D834B30F` FOREIGN KEY (`credit_transaction_id`) REFERENCES `sf_credit_transaction`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_file` ADD CONSTRAINT `FK_89D9489916FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_file` ADD CONSTRAINT `FK_89D94899DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_group` ADD CONSTRAINT `FK_F9A1393F10405986` FOREIGN KEY (`institution_id`) REFERENCES `sf_institution`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_group` ADD CONSTRAINT `FK_F9A1393FDE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_group` ADD CONSTRAINT `FK_F9A1393F16FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_group` ADD CONSTRAINT `FK_F9A1393F591CC992` FOREIGN KEY (`course_id`) REFERENCES `sf_course`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_group` ADD CONSTRAINT `FK_F9A1393F833D8F43` FOREIGN KEY (`registration_id`) REFERENCES `sf_lti_registration`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_group` ADD CONSTRAINT `FK_F9A1393F9DF4CE98` FOREIGN KEY (`deployment_id`) REFERENCES `sf_lti_deployment`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_group` ADD CONSTRAINT `FK_F9A1393FC87EE135` FOREIGN KEY (`default_course_id`) REFERENCES `sf_course`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_group` ADD CONSTRAINT `FK_F9A1393FFFC8EBBC` FOREIGN KEY (`credit_type_id`) REFERENCES `sf_credit_type`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_infusionsoft_email_link` ADD CONSTRAINT `FK_8FD65BC3A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_institution` ADD CONSTRAINT `FK_F65406AEDE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_institution` ADD CONSTRAINT `FK_F65406AE16FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_institution` ADD CONSTRAINT `FK_F65406AE3D41F214` FOREIGN KEY (`contact_user_id`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_institution` ADD CONSTRAINT `FK_F65406AE9393F8FE` FOREIGN KEY (`partner_id`) REFERENCES `sf_partner`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_institution` ADD CONSTRAINT `FK_F65406AE98260155` FOREIGN KEY (`region_id`) REFERENCES `sf_region`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_interactive` ADD CONSTRAINT `FK_F794137CDE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_interactive` ADD CONSTRAINT `FK_F794137C16FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_interactive` ADD CONSTRAINT `FK_F794137C4B7E5594` FOREIGN KEY (`lti_external_tool_id`) REFERENCES `sf_lti_external_tool`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_interactive` ADD CONSTRAINT `FK_F794137C51899847` FOREIGN KEY (`screen_shot_file_id`) REFERENCES `sf_file`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_interactive` ADD CONSTRAINT `FK_F794137C59027487` FOREIGN KEY (`theme_id`) REFERENCES `sf_css_theme`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_interactive` ADD CONSTRAINT `FK_F794137CF28C831B` FOREIGN KEY (`secular_alternative_id`) REFERENCES `sf_interactive`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_interactive` ADD CONSTRAINT `FK_F794137CF675F31B` FOREIGN KEY (`author_id`) REFERENCES `sf_author`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_interactive_status` ADD CONSTRAINT `FK_138EE73B329C9C7D` FOREIGN KEY (`interactive_id`) REFERENCES `sf_interactive`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_interactive_status` ADD CONSTRAINT `FK_138EE73BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_internal_report` ADD CONSTRAINT `FK_6A6DFC8B16FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_internal_report` ADD CONSTRAINT `FK_6A6DFC8BDE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_internal_report_result` ADD CONSTRAINT `FK_3721E561DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_internal_report_result` ADD CONSTRAINT `FK_3721E56116FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_internal_report_result` ADD CONSTRAINT `FK_3721E5614BD2A4C0` FOREIGN KEY (`report_id`) REFERENCES `sf_internal_report`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_internal_report_result` ADD CONSTRAINT `FK_3721E56193CB796C` FOREIGN KEY (`file_id`) REFERENCES `sf_file`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_activity` ADD CONSTRAINT `FK_85E82FEEDD3DD5F1` FOREIGN KEY (`assessment_id`) REFERENCES `sf_learnosity_assessment`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_activity` ADD CONSTRAINT `FK_85E82FEEFE54D947` FOREIGN KEY (`group_id`) REFERENCES `sf_group`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_assessment` ADD CONSTRAINT `FK_A5F6FAADDE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_assessment` ADD CONSTRAINT `FK_A5F6FAAD16FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_assessment` ADD CONSTRAINT `FK_A5F6FAAD579F4768` FOREIGN KEY (`chapter_id`) REFERENCES `sf_chapter`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_assessment` ADD CONSTRAINT `FK_A5F6FAAD59027487` FOREIGN KEY (`theme_id`) REFERENCES `sf_css_theme`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_assessment` ADD CONSTRAINT `FK_A5F6FAAD591CC992` FOREIGN KEY (`course_id`) REFERENCES `sf_course`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_assessment` ADD CONSTRAINT `FK_A5F6FAAD727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `sf_learnosity_assessment`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_assessment_configuration` ADD CONSTRAINT `FK_F80460D310405986` FOREIGN KEY (`institution_id`) REFERENCES `sf_institution`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_assessment_configuration` ADD CONSTRAINT `FK_F80460D39393F8FE` FOREIGN KEY (`partner_id`) REFERENCES `sf_partner`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_assessment_configuration` ADD CONSTRAINT `FK_F80460D3A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_assessment_configuration` ADD CONSTRAINT `FK_F80460D3DD3DD5F1` FOREIGN KEY (`assessment_id`) REFERENCES `sf_learnosity_assessment`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_assessment_configuration` ADD CONSTRAINT `FK_F80460D3FE54D947` FOREIGN KEY (`group_id`) REFERENCES `sf_group`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_grade` ADD CONSTRAINT `FK_9CA96ADB81C06096` FOREIGN KEY (`activity_id`) REFERENCES `sf_learnosity_activity`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_grade` ADD CONSTRAINT `FK_9CA96ADBA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_session` ADD CONSTRAINT `FK_95ABF9AD3D32C285` FOREIGN KEY (`feedback_session_id`) REFERENCES `sf_learnosity_session`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_session` ADD CONSTRAINT `FK_95ABF9AD81C06096` FOREIGN KEY (`activity_id`) REFERENCES `sf_learnosity_activity`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_session` ADD CONSTRAINT `FK_95ABF9ADA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_session_item` ADD CONSTRAINT `FK_E8FC9BD1613FECDF` FOREIGN KEY (`session_id`) REFERENCES `sf_learnosity_session`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_user_config` ADD CONSTRAINT `FK_49E83EEF81C06096` FOREIGN KEY (`activity_id`) REFERENCES `sf_learnosity_activity`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_user_config` ADD CONSTRAINT `FK_49E83EEFA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lesson` ADD CONSTRAINT `FK_A5880E8CDE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lesson` ADD CONSTRAINT `FK_A5880E8C16FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lesson` ADD CONSTRAINT `FK_A5880E8C579F4768` FOREIGN KEY (`chapter_id`) REFERENCES `sf_chapter`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lesson` ADD CONSTRAINT `FK_A5880E8C690FD836` FOREIGN KEY (`video_plate_file_id`) REFERENCES `sf_file`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lesson` ADD CONSTRAINT `FK_A5880E8C727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `sf_lesson`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lesson_learning_object_link` ADD CONSTRAINT `FK_AC8E303B166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `sf_project`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lesson_learning_object_link` ADD CONSTRAINT `FK_AC8E303B1E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `sf_question`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lesson_learning_object_link` ADD CONSTRAINT `FK_AC8E303B29C1004E` FOREIGN KEY (`video_id`) REFERENCES `sf_video`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lesson_learning_object_link` ADD CONSTRAINT `FK_AC8E303B329C9C7D` FOREIGN KEY (`interactive_id`) REFERENCES `sf_interactive`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lesson_learning_object_link` ADD CONSTRAINT `FK_AC8E303B527275CD` FOREIGN KEY (`reading_id`) REFERENCES `sf_reading`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lesson_learning_object_link` ADD CONSTRAINT `FK_AC8E303BCDF80196` FOREIGN KEY (`lesson_id`) REFERENCES `sf_lesson`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lesson_progress` ADD CONSTRAINT `FK_6B027287A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lesson_progress` ADD CONSTRAINT `FK_6B027287CDF80196` FOREIGN KEY (`lesson_id`) REFERENCES `sf_lesson`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_access_token` ADD CONSTRAINT `FK_B7F73DD3833D8F43` FOREIGN KEY (`registration_id`) REFERENCES `sf_lti_registration`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_deployment` ADD CONSTRAINT `FK_C440ED2310405986` FOREIGN KEY (`institution_id`) REFERENCES `sf_institution`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_deployment` ADD CONSTRAINT `FK_C440ED23DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_deployment` ADD CONSTRAINT `FK_C440ED2316FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_deployment` ADD CONSTRAINT `FK_C440ED23833D8F43` FOREIGN KEY (`registration_id`) REFERENCES `sf_lti_registration`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_external_tool` ADD CONSTRAINT `FK_E95E611916FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_external_tool` ADD CONSTRAINT `FK_E95E6119DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_line_item` ADD CONSTRAINT `FK_A992CCFB81C06096` FOREIGN KEY (`activity_id`) REFERENCES `sf_learnosity_activity`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_line_item` ADD CONSTRAINT `FK_A992CCFB833D8F43` FOREIGN KEY (`registration_id`) REFERENCES `sf_lti_registration`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_line_item` ADD CONSTRAINT `FK_A992CCFB9DF4CE98` FOREIGN KEY (`deployment_id`) REFERENCES `sf_lti_deployment`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_line_item` ADD CONSTRAINT `FK_A992CCFBF004E599` FOREIGN KEY (`resource_link_id`) REFERENCES `sf_lti_resource_link`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_lms_detail` ADD CONSTRAINT `FK_C2A983FB10405986` FOREIGN KEY (`institution_id`) REFERENCES `sf_institution`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_outcome_result` ADD CONSTRAINT `FK_87963D5C81C06096` FOREIGN KEY (`activity_id`) REFERENCES `sf_learnosity_activity`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_outcome_result` ADD CONSTRAINT `FK_87963D5CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_registration` ADD CONSTRAINT `FK_63FD471C16FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_registration` ADD CONSTRAINT `FK_63FD471CDE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_registration_institution` ADD CONSTRAINT `FK_121F3C4210405986` FOREIGN KEY (`institution_id`) REFERENCES `sf_institution`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_registration_institution` ADD CONSTRAINT `FK_121F3C42833D8F43` FOREIGN KEY (`registration_id`) REFERENCES `sf_lti_registration`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_resource_link` ADD CONSTRAINT `FK_653E06AF833D8F43` FOREIGN KEY (`registration_id`) REFERENCES `sf_lti_registration`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_resource_link` ADD CONSTRAINT `FK_653E06AF9DF4CE98` FOREIGN KEY (`deployment_id`) REFERENCES `sf_lti_deployment`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_user` ADD CONSTRAINT `FK_DB59319F833D8F43` FOREIGN KEY (`registration_id`) REFERENCES `sf_lti_registration`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_user` ADD CONSTRAINT `FK_DB59319F9DF4CE98` FOREIGN KEY (`deployment_id`) REFERENCES `sf_lti_deployment`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_lti_user` ADD CONSTRAINT `FK_DB59319FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_permission` ADD CONSTRAINT `FK_27A04C1E16FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_permission` ADD CONSTRAINT `FK_27A04C1EDE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_permission_value` ADD CONSTRAINT `FK_43F92B6BD60322AC` FOREIGN KEY (`role_id`) REFERENCES `sf_role`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_permission_value` ADD CONSTRAINT `FK_43F92B6BFED90CCA` FOREIGN KEY (`permission_id`) REFERENCES `sf_permission`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_previous_password` ADD CONSTRAINT `FK_18793B2CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_project` ADD CONSTRAINT `FK_EF544039DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_project` ADD CONSTRAINT `FK_EF54403916FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_project` ADD CONSTRAINT `FK_EF544039579F4768` FOREIGN KEY (`chapter_id`) REFERENCES `sf_chapter`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_project` ADD CONSTRAINT `FK_EF544039591CC992` FOREIGN KEY (`course_id`) REFERENCES `sf_course`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_project_map` ADD CONSTRAINT `FK_CD1DEE13579F4768` FOREIGN KEY (`chapter_id`) REFERENCES `sf_chapter`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_project_map` ADD CONSTRAINT `FK_CD1DEE13591CC992` FOREIGN KEY (`course_id`) REFERENCES `sf_course`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_project_map` ADD CONSTRAINT `FK_CD1DEE137BDA6808` FOREIGN KEY (`original_project_id`) REFERENCES `sf_project`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_project_map` ADD CONSTRAINT `FK_CD1DEE13F587106C` FOREIGN KEY (`mapped_project_id`) REFERENCES `sf_project`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_public_link` ADD CONSTRAINT `FK_813A456BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_question` ADD CONSTRAINT `FK_AE80E9A9DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_question` ADD CONSTRAINT `FK_AE80E9A916FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_question` ADD CONSTRAINT `FK_AE80E9A92D1F8EE` FOREIGN KEY (`solution_video_id`) REFERENCES `sf_video`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_question` ADD CONSTRAINT `FK_AE80E9A947C14E91` FOREIGN KEY (`linked_video_id`) REFERENCES `sf_video`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_question` ADD CONSTRAINT `FK_AE80E9A9CB90598E` FOREIGN KEY (`question_type_id`) REFERENCES `sf_question_type`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_question` ADD CONSTRAINT `FK_AE80E9A9F28C831B` FOREIGN KEY (`secular_alternative_id`) REFERENCES `sf_question`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_question` ADD CONSTRAINT `FK_AE80E9A9F675F31B` FOREIGN KEY (`author_id`) REFERENCES `sf_author`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_question_object` ADD CONSTRAINT `FK_AEB44E3C1E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `sf_question`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_question_object` ADD CONSTRAINT `FK_AEB44E3C29C1004E` FOREIGN KEY (`video_id`) REFERENCES `sf_video`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_question_object` ADD CONSTRAINT `FK_AEB44E3C727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `sf_question_object`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_question_object` ADD CONSTRAINT `FK_AEB44E3C93CB796C` FOREIGN KEY (`file_id`) REFERENCES `sf_file`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_question_status` ADD CONSTRAINT `FK_7D1980CC1E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `sf_question`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_question_status` ADD CONSTRAINT `FK_7D1980CCA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_question_type` ADD CONSTRAINT `FK_D60A5E8016FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_question_type` ADD CONSTRAINT `FK_D60A5E80DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_reading` ADD CONSTRAINT `FK_1FD6C96DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_reading` ADD CONSTRAINT `FK_1FD6C9616FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_reading` ADD CONSTRAINT `FK_1FD6C9635043726` FOREIGN KEY (`notes_theme_id`) REFERENCES `sf_css_theme`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_reading` ADD CONSTRAINT `FK_1FD6C9659027487` FOREIGN KEY (`theme_id`) REFERENCES `sf_css_theme`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_reading` ADD CONSTRAINT `FK_1FD6C96F28C831B` FOREIGN KEY (`secular_alternative_id`) REFERENCES `sf_reading`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_reading` ADD CONSTRAINT `FK_1FD6C96F675F31B` FOREIGN KEY (`author_id`) REFERENCES `sf_author`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_reading_chunk` ADD CONSTRAINT `FK_FD0D56ED527275CD` FOREIGN KEY (`reading_id`) REFERENCES `sf_reading`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_reading_chunk` ADD CONSTRAINT `FK_FD0D56ED8BF1A064` FOREIGN KEY (`transition_id`) REFERENCES `sf_reading_transition`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_reading_compiled_css` ADD CONSTRAINT `FK_FE49BA42527275CD` FOREIGN KEY (`reading_id`) REFERENCES `sf_reading`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_reading_status` ADD CONSTRAINT `FK_7905FD31527275CD` FOREIGN KEY (`reading_id`) REFERENCES `sf_reading`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_reading_status` ADD CONSTRAINT `FK_7905FD31A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_reading_theme` ADD CONSTRAINT `FK_FF7EC0CB16FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_reading_theme` ADD CONSTRAINT `FK_FF7EC0CBDE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_report_dashboard` ADD CONSTRAINT `FK_FA298CCCA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_report_dashboard_template_widget` ADD CONSTRAINT `FK_6EEAD97A3BC08534` FOREIGN KEY (`widget_template_id`) REFERENCES `sf_report_widget_template`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_report_dashboard_template_widget` ADD CONSTRAINT `FK_6EEAD97A5FFB2CF7` FOREIGN KEY (`dashboard_template_id`) REFERENCES `sf_report_dashboard_template`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_report_widget` ADD CONSTRAINT `FK_A8302F095DA0FB8` FOREIGN KEY (`template_id`) REFERENCES `sf_report_widget_template`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_report_widget` ADD CONSTRAINT `FK_A8302F09A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_report_widget` ADD CONSTRAINT `FK_A8302F09B9D04D2B` FOREIGN KEY (`dashboard_id`) REFERENCES `sf_report_dashboard`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_role` ADD CONSTRAINT `FK_522FF4E316FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_role` ADD CONSTRAINT `FK_522FF4E3DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_role_assignment` ADD CONSTRAINT `FK_BEB5E44E6B00C1CF` FOREIGN KEY (`context_id`) REFERENCES `sf_context`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_role_assignment` ADD CONSTRAINT `FK_BEB5E44EA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_role_assignment` ADD CONSTRAINT `FK_BEB5E44ED60322AC` FOREIGN KEY (`role_id`) REFERENCES `sf_role`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_royalty_report_current_files` ADD CONSTRAINT `sf_royalty_report_current_files_sf_enrolment_period_id_fk` FOREIGN KEY (`enrolment_period_id`) REFERENCES `sf_enrolment_period`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_royalty_report_current_files` ADD CONSTRAINT `sf_royalty_report_current_files_sf_file_id_fk` FOREIGN KEY (`file_id`) REFERENCES `sf_file`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_royalty_report_current_files` ADD CONSTRAINT `sf_royalty_report_current_files_sf_group_id_fk` FOREIGN KEY (`group_id`) REFERENCES `sf_group`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_royalty_report_current_files` ADD CONSTRAINT `sf_royalty_report_current_files_sf_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_royalty_report_current_interactive_status` ADD CONSTRAINT `FK_4C70DF85329C9C7D` FOREIGN KEY (`interactive_id`) REFERENCES `sf_interactive`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_royalty_report_current_interactive_status` ADD CONSTRAINT `FK_4C70DF85A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_royalty_report_current_learnosity_item` ADD CONSTRAINT `enrolment_period_id_fk` FOREIGN KEY (`enrolment_period_id`) REFERENCES `sf_enrolment_period`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_royalty_report_current_question_status` ADD CONSTRAINT `FK_ECED1FD51E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `sf_question`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_royalty_report_current_question_status` ADD CONSTRAINT `FK_ECED1FD5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_royalty_report_current_reading_status` ADD CONSTRAINT `FK_E9F6737F527275CD` FOREIGN KEY (`reading_id`) REFERENCES `sf_reading`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_royalty_report_current_reading_status` ADD CONSTRAINT `FK_E9F6737FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_royalty_report_current_video_status` ADD CONSTRAINT `FK_585AC77629C1004E` FOREIGN KEY (`video_id`) REFERENCES `sf_video`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_royalty_report_current_video_status` ADD CONSTRAINT `FK_585AC776A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_royalty_view_count` ADD CONSTRAINT `FK_AE96B8C394FEE7D3` FOREIGN KEY (`enrolment_period_id`) REFERENCES `sf_enrolment_period`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_snapshot` ADD CONSTRAINT `FK_343AB5D2166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `sf_project`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_snapshot` ADD CONSTRAINT `FK_343AB5D2613FECDF` FOREIGN KEY (`session_id`) REFERENCES `sf_learnosity_session`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_snapshot` ADD CONSTRAINT `FK_343AB5D2A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_snapshot` ADD CONSTRAINT `FK_343AB5D2DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_tutorial` ADD CONSTRAINT `FK_DE1C5F0E16FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_tutorial` ADD CONSTRAINT `FK_DE1C5F0EDE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_tutorial` ADD CONSTRAINT `FK_DE1C5F0EE4EDEC69` FOREIGN KEY (`alternate_version_id`) REFERENCES `sf_tutorial`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_tutorial_slide` ADD CONSTRAINT `FK_E546CB894969F18D` FOREIGN KEY (`action_file_id`) REFERENCES `sf_file`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_tutorial_slide` ADD CONSTRAINT `FK_E546CB8989366B7B` FOREIGN KEY (`tutorial_id`) REFERENCES `sf_tutorial`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_tutorial_slide` ADD CONSTRAINT `FK_E546CB8993CB796C` FOREIGN KEY (`file_id`) REFERENCES `sf_file`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_tutorial_status` ADD CONSTRAINT `FK_4C4FF7C989366B7B` FOREIGN KEY (`tutorial_id`) REFERENCES `sf_tutorial`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_tutorial_status` ADD CONSTRAINT `FK_4C4FF7C9A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_user` ADD CONSTRAINT `FK_88D5A8C010405986` FOREIGN KEY (`institution_id`) REFERENCES `sf_institution`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_user` ADD CONSTRAINT `FK_88D5A8C098260155` FOREIGN KEY (`region_id`) REFERENCES `sf_region`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_user` ADD CONSTRAINT `FK_88D5A8C0C87EE135` FOREIGN KEY (`default_course_id`) REFERENCES `sf_course`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video` ADD CONSTRAINT `FK_E8A6A7D6DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video` ADD CONSTRAINT `FK_E8A6A7D616FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video` ADD CONSTRAINT `FK_E8A6A7D651899847` FOREIGN KEY (`screen_shot_file_id`) REFERENCES `sf_file`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video` ADD CONSTRAINT `FK_E8A6A7D6690FD836` FOREIGN KEY (`video_plate_file_id`) REFERENCES `sf_file`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video` ADD CONSTRAINT `FK_E8A6A7D6762690C1` FOREIGN KEY (`video_file_id`) REFERENCES `sf_file`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video` ADD CONSTRAINT `FK_E8A6A7D6931D3538` FOREIGN KEY (`notes_file_id`) REFERENCES `sf_file`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video` ADD CONSTRAINT `FK_E8A6A7D6DECB30CC` FOREIGN KEY (`stripped_video_file_id`) REFERENCES `sf_file`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video` ADD CONSTRAINT `FK_E8A6A7D6F28C831B` FOREIGN KEY (`secular_alternative_id`) REFERENCES `sf_video`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video` ADD CONSTRAINT `FK_E8A6A7D6F675F31B` FOREIGN KEY (`author_id`) REFERENCES `sf_author`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video_interaction` ADD CONSTRAINT `FK_275F29FB29C1004E` FOREIGN KEY (`video_id`) REFERENCES `sf_video`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video_interaction` ADD CONSTRAINT `FK_275F29FB59027487` FOREIGN KEY (`theme_id`) REFERENCES `sf_video_interaction_theme`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video_interaction` ADD CONSTRAINT `FK_275F29FB93CB796C` FOREIGN KEY (`file_id`) REFERENCES `sf_file`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video_interaction` ADD CONSTRAINT `FK_275F29FBAC7C70B0` FOREIGN KEY (`audio_file_id`) REFERENCES `sf_file`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video_interaction_theme` ADD CONSTRAINT `FK_A5E0AA16FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video_interaction_theme` ADD CONSTRAINT `FK_A5E0AADE12AB56` FOREIGN KEY (`created_by`) REFERENCES `sf_user`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video_status` ADD CONSTRAINT `FK_A295965729C1004E` FOREIGN KEY (`video_id`) REFERENCES `sf_video`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_video_status` ADD CONSTRAINT `FK_A2959657A76ED395` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

