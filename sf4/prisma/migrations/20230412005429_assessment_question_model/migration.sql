-- CreateTable
CREATE TABLE `sf_learnosity_assessment_question` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `assessment_id` INTEGER NULL,
    `item_reference` VARCHAR(255) NULL,
    `sort_order` INTEGER NOT NULL,
    `weight` DOUBLE NOT NULL DEFAULT 0,

    INDEX `sf_learnosity_assessment_question_assessment_id_idx`(`assessment_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `sf_learnosity_assessment_question` ADD CONSTRAINT `sf_learnosity_assessment_question_assessment_id_fkey` FOREIGN KEY (`assessment_id`) REFERENCES `sf_learnosity_assessment`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;
