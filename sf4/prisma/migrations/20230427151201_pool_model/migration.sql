-- AlterTable
ALTER TABLE `sf_learnosity_assessment_question` ADD COLUMN `pool_id` INTEGER NULL;

-- CreateTable
CREATE TABLE `sf_learnosity_pool` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `num_to_select` INTEGER NULL,
    `weight` DOUBLE NOT NULL DEFAULT 0,
    `created_at` DATETIME(0) NOT NULL,
    `updated_at` DATETIME(0) NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateIndex
CREATE INDEX `sf_learnosity_assessment_question_pool_id_idx` ON `sf_learnosity_assessment_question`(`pool_id`);

-- AddForeignKey
ALTER TABLE `sf_learnosity_assessment_question` ADD CONSTRAINT `sf_learnosity_assessment_question_pool_id_fkey` FOREIGN KEY (`pool_id`) REFERENCES `sf_learnosity_pool`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;
