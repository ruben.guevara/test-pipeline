-- CreateTable
CREATE TABLE `sf_learnosity_assessment_version` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `assessment_id` INTEGER NOT NULL,
    `user_id` INTEGER NOT NULL,
    `version_data` LONGTEXT NOT NULL,
    `timestamp` INTEGER NOT NULL,

    INDEX `sf_learnosity_assessment_version_assessment_id_idx`(`assessment_id`),
    INDEX `sf_learnosity_assessment_version_user_id_idx`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `sf_learnosity_assessment_version` ADD CONSTRAINT `sf_learnosity_assessment_version_assessment_id_fkey` FOREIGN KEY (`assessment_id`) REFERENCES `sf_learnosity_assessment`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

-- AddForeignKey
ALTER TABLE `sf_learnosity_assessment_version` ADD CONSTRAINT `sf_learnosity_assessment_version_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `sf_user`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;
