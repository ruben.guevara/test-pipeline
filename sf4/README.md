# StudyForge 4

**"Better Features, Faster"**

# Getting Started

See the root level readme for how to setup your local development environment with Docker.

# Overview

## Background

StudyForge 3 has accumulated a lot of technical debt. There are a lot of pieces of the app that were done in non-standard ways where we chose to reinvent the wheel rather than use the open source libraries and frameworks. So StudyForge 3 has a component-like pattern, but not using a framework like React or Vue. So it has a lot of the same things, but made in a custom way with less performance, more bugs, and harder to maintain. The goal with StudyForge 4 is to modernize our tech stack so that we can make use of all the great libraries and the latest developments in web dev frameworks. This will eventually speed up our development process because we won’t have to build what everyone else has already built.

## Language and Framework

StudyForge 4 is a Next.js app running on Node hosted on our own on-prem servers. We use Typescript for everything that we can and do not compromise on type safety. It is **always** better to get a compile time error instead of shipping a bug that we don’t know about until reported. The StudyForge 4 app takes a lot of inspiration from the [T3 Stack](https://create.t3.gg).

We've chosen these technologies for ease of use and DevX (relatively to php and symfony there is no contest), and also for their large community and support. React has been the standard way to make new web apps for a long time. Every major library has support for React as a default and only sometimes adds support for other frameworks. And NextJS is the standard metaframework for React, pioneering serverside React since its beginning, maintained by a thriving company, Vercel, and still pushing the envelope with new ways to make web apps and web dev tooling. There may be other frameworks that are newer and shinier (Svelte is amazing!), but we've chosen to Next as our anchor because it is too easy to be blown around in the storm of javascript frameworks constantly being released.

## Transition Plan

The plan is to slowly replace StudyForge 3 feature by feature with StudyForge 4 versions of those features. The two systems will run in parallel sharing the same database and cache.

Pages from StudyForge 4 will be accessible behind the `/v4/` path prefix. For example the sf4 version of the `/manage/regions` will be available at `/v4/manage/regions`. The nginx load balancer sends requests to the php server or node server based on this path pattern. Once a page is moved to sf4 the sf3 php route will simply return a 304 redirect to the v4 route. Eventually when we no longer need the php sf3 servers the load balancer will send all requests to the sf4 servers and the v4 routes will remain available but respond with 304 redirects to the urls without the path.

Components from StudyForge 4 will be available to be rendered client side in StudyForge 3 using module federation. For example a new nav bar will need to be written for sf4 pages, but we dont want to maintain two versions of the nav bar, so sf3 pages will render the sf4 version that it loads at runtime from the sf4 servers via module federation.

## Network architecture diagram

<!--TODO-->

## Systems architecture diagram

<!--TODO-->

## List of technologies

| Category      | Technology Choice                                                  |
| ------------- | ------------------------------------------------------------------ |
| Language      | [Typescript](https://www.typescriptlang.org/)                      |
| Framework     | [NextJS](https://nextjs.org/) & [React](https://beta.reactjs.org/) |
| DB Layer      | [Prisma](https://www.prisma.io/)                                   |
| API Layer     | [tRPC](https://trpc.io/)                                           |
| CSS Framework | [Tailwind](https://tailwindcss.com/)                               |
| Forms         | [React Hook Form](https://react-hook-form.com/)                    |
| Tables        | [Tanstack Table](https://tanstack.com/table/v8)                    |

The goal of these tools is to (a) remain type safe in everything we do and (b) not reinvent the wheel. We should, when we can, use libraries that allow us to focus on StudyForge specific UI and logic, rather than spending all our time on a solution that has been solved in a better ways that we ever could by the larger OSS community. Libraries written with typescript first are always preferred because maintaining someone else's types is a nightmare. Headless UI libraries are always preferred because they allow us to pcik and chose what we want in terms of features and stay consistent with how UI is written in sf4 and how it looks our users. Basically anything from [Tanstack](https://tanstack.com/).

### Why Tailwind?

Asking a developer to make new UI without a full hifi design document makes them grind to a hault. They will either make it ugly and inconsistent, or spend way to long tweaking every little bit of spacing and shade of color. But that is often required of StudyForge developers as our design team is shared with the whole Society. So Tailwind allows us basically write css, and gives us the freedom to make things look exactly how we want them to, while giving us so many shortcuts with its standardized design system so that we dont have sweat the little stuff when it comes to styling.

### Why tRPC?

RESTful APIs are a pain to maintain and document. There are so many opinions about how to do REST that you always have to check "the house rules" before actually using it. You also have to cast your responses to types in the client side typescript which can result in errors not being found at compile time. GraphQL APIs are much better for type safety but they still require you to maintain and document a middle layer between your server and client (the GraphQL schema). tRPC gives you full type safety without having to maintain any middle layers. You can easily refactor code on the backend from the frontend or vice versa. It brings the backend and frontend closer together. Seriously, tRPC is maybe the best thing about StudyForge 4.

## Directory structure

<!--TODO
- explain the $ aliases
-->

# Pages & Routes

<!--TODO-->

# UI Component Library

<!--TODO
- structure: shared, features, pages
- tailwind
-->

# Forms

<!--TODO-->

# API

<!--TODO-->

# Auth

<!--TODO-->

# Database Models

<!--TODO
- prisma, schema, migrations, config
- custom models
-->

# Tests

<!--TODO
- integration tests of services
- unit tests of ui components
-->

# Environments

<!--TODO-->

# Deployment

<!--TODO-->

# VS Code Setup

<!--TODO
- prettier
- vitest
- tailwind
- eslint
- vs-code-twoslash-queries
- copilot
- docker
-->

# Other Bits

<!--TODO
- explain the relatively unused places: styles, lib, resources
- utility types
-->
