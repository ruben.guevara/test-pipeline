module.exports = {
  extends: ["next", "next/core-web-vitals"],
  rules: {
    "import/no-anonymous-default-export": 1,
  },
  parserOptions: {
    babelOptions: {
      presets: [require.resolve("next/babel")],
    },
  },
};
