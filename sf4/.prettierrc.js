module.exports = {
  plugins: [
    "prettier-plugin-tailwindcss",
    require.resolve("@trivago/prettier-plugin-sort-imports"),
  ],
  printWidth: 80,
  importOrder: [
    "^$server/(.*)$",
    "^$lib/(.*)$",
    "^@features/(.*)$",
    "^$components/(.*)$",
    "^$styles/(.*)$",
    "^$resources/(.*)$",
    "^$customTypes/(.*)$",
    "^$tests/(.*)$",
    "^[./]",
  ],
  importOrderSeparation: false,
  importOrderSortSpecifiers: true,
  tabSize: 2
};
