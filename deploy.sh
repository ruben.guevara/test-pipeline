#! /usr/bin/bash

set -e
trap 'catch $? $LINENO' EXIT
trap 'cleanup; exit 1' INT TERM;

catch() {
  if [[ "$1" != "0" ]]; then
    echo -e "${GREEN}[$CMD]: Error $1 occured on line $2, cleaning up${NC}"
    cleanup;
    exit $1;
  fi
}

cleanup() {
    if test -d "$NEW_DIR"; then
        exe rm -rf $NEW_DIR;
    fi
}

exe() {
    echo "\$ $@"
    "$@"
}

GREEN='\033[0;32m'
NC='\033[0m' # No Colo

CMD=$(basename $0);
BRANCH=$1;
if [[ -z $BRANCH ]]; then
    echo "Usage: $CMD <branch>";
fi

echo -e "${GREEN}[$CMD]: Checking previous version${NC}"
exe mkdir /studyforge;
# exe cd /studyforge;
# exe git fetch --depth 1;
# status=$(exe git status -sb);
# if ! echo "$status" | grep -q 'behind'; then
#     echo -e "${GREEN}[$CMD]: Up to date, exiting${NC}"
#     exit 0;
# fi

echo -e "${GREEN}[$CMD]: Creating new directory${NC}"
NEW_DIR="/studyforge-$(date +%Y-%m-%d-%H-%M-%S)";
exe mkdir $NEW_DIR;

###################################
echo $NEW_DIR
###################################